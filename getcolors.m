function [col,varargout]=getcolors(colorscheme,varargin);
% Returns a color or a sequence of colors.
% The sequence can be accessed through a variable amount of output arguments
% (each one containing a character representing a color), or through a string.
%
% Syntax: (* = optional)
%
% [color, color2, color3, ...] = getcolors(colorscheme);
% colorseq = getcolors(colorscheme, n);
%
% In arguments:
%
% 1. colorscheme
%	An integer that is used for calculating the color sequence. Different values
%	give different sequences.
% 2. n
%	Amount of colors in the resulting sequence of colors (the 'colorseq' output argument)
%
% Out arguments:
%
% 1. color, color2, color3, ...
%	A character representing a color. Matlab standards are used, so for instance
%	'b' means blue and 'g' means green (see the documentation of the plot command
%	for a list of colors).
% 1. colorseq
%	A string containing a sequence of colors (characters). 

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%colors='brmkgcy';
%colors='mrgkycb';
colors='mrgbyck';

n=length(colors);
j=colorscheme;
if nargin>1
	for i=1:varargin{1}
		col(i)=colors(rem(j,n)+1);
		j=j+4;
	end
else
	col=colors(rem(j,n)+1);
	if nargout>=2
		for i=1:nargout-1
			j=j+4;
			varargout{i}=colors(rem(j,n)+1);
		end;
	end
end
