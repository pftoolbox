% Just a testscript

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

T=1;
f=[1 0 T 0; 0 1 0 T; 0 0 1 0; 0 0 0 1];
gw=[0.5*T^2 0; 0 0.5*T^2; T 0; 0 T];
%h={'sqrt(x1.^2+x2.^2)', 'atan(x2./x1)', 'x4'};
h={'x1','x2','x4'};

model=danl(f,gw,[],h,[],[0 0 0 0]',[1 1],[0.25 0.2 0.1], [], 1, {'x1','x2','x3','x4'});

disp('Initializing filter objects');
simobj=simulator(model);
sirobj=sir(model);
ekfobj=ekf(model);

animate(simobj,{sirobj,ekfobj},100, [1 2], [1 1]);
