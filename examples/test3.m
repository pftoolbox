% Just a testscript

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

T=1;
A=[1 0 T 0; 0 1 0 T; 0 0 1 0; 0 0 0 1];
B=[0.5*T^2 0; 0 0.5*T^2; T 0; 0 T];
h={'sqrt(x1.^2+x2.^2)', 'atan2(x2,x1)'};

gradx={'1/(x1^2+x2^2+eps)^(1/2)*x1', '1/(x1^2+x2^2+eps)^(1/2)*x2', '0', '0'; '-x2/(x1^2+eps)/(1+x2^2/(x1^2+eps))', '1/(x1+eps)/((1+x2^2+eps)/(x1^2+eps))', '0', '0'};

model=danl(A,B,[],h,[],[0 0 0 0],[1 1],[1 1], [1 1 1 1]');
model=set(model,'hgradx',gradx);

%disp('Our new gradient:');
%get(get(model,'h'),'gradx')

simobj=simulator(model);
sirobj=sir(model);
ekfobj=ekf(model);

rmse(simobj,{ekfobj,sirobj},10);
