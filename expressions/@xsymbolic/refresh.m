function obj=refresh(obj);
% No documentation available

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

expression=obj.expression;
if ~isempty(expression)

	% Calculate the amount of subexpressions
	if isa(expression,'cell')
		n=length(expression);
	else
		% A single object was supplied
		n=1;
		expression={expression};
	end;

	xvars=obj.xvars;
	uvars=obj.uvars;
	wvars=obj.wvars;

	% Convert all subexpressions to char
	charexpr=convert2char(expression);

	% If *vars is empty, calculate default values.
	if isempty(xvars); xvars=getvars(charexpr,'x'); end;
	if isempty(wvars); wvars=getvars(charexpr,obj.wchar); end;
	if isempty(uvars); uvars=getvars(charexpr,'u'); end;

	% Generate the internal expression, for use with the feval command.
	intexpr=char2oneline(ext2int(charexpr,xvars,'x',wvars,'w',uvars,'u')');

%	obj.expression=expression;
	obj.intexpr=intexpr;
	obj.str=expr2str(expression);
	obj.exprsize=[n,0];
	obj.xvars=xvars;
	obj.uvars=uvars;
	obj.wvars=wvars;
end
