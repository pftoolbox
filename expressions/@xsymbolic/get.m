function v = get(obj,varargin)
% Accesses the properties of an object.
%
% value = get(obj, 'propertyname') returns the value of the specified property of the
% object 'obj'.
%
% An equivalent syntax is value = obj.propertyname.
%   
% get(obj) displays all properties of obj and their values.  
%
% Type 'props(obj)' for more details on the properties of the object 'obj'.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin>1
	error(nargchk(1,2,nargin));	
	prop_name=varargin{1};
	switch prop_name

	% <custom>
	% Here we handle properties that need custom treatment (or need to be 
	% processed really fast)


	% </custom>

	otherwise
		[props,rprops]=pnames;
		if findcstr(props,prop_name)||findcstr(rprops,prop_name)
			eval(strcat('v=obj.',prop_name,';'));
		else
			error([prop_name,' is not a valid property'])
		end
	end
else
	displayprops(obj);
end;
