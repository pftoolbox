function obj=initgradx(obj)
% Intializes the gradient with respect to x. Used by, for instance, the EKF filter.
% The result is found in the 'gradx' property. If the gradw property is non-empty,
% a new gradient will NOT be calculated. This way, user-defined gradients will survive
% the creation of filters that use the 'initgrad' methods.
%
% Syntax: (* = optional)
%
% obj = initgradx(obj);
%
% In arguments:
%
% 1. obj
%	Data object to be differentiated.
% Out arguments:
%
% 1. obj
%	Resulting object

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Create a new gradient only if no gradient is present. This is because we want the user
% to be able to create her own gradients without them beeing lost each time 
% (for instance) an EKF filter is created.
if isempty(obj.gradx)
	% Calculate the gradient with respect to x
	str=initgrad(obj.expression,obj.xvars);
	% Convert the result to the internal format, and store it.
	obj.gradx=char2oneline(ext2int(str,obj.xvars,'x',obj.wvars,'w',obj.uvars,'u'));
end
