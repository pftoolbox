function str=removedots(str)
%REMOVEDOTS	Input arguments:
%		1) str: An string expression
%
% Removes the dots in all '.^', '.*' and './' substrings.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

m=length(str);
for j=1:m;
	str{j}=strrep(str{j},'.^','^');
	str{j}=strrep(str{j},'.*','*');
	str{j}=strrep(str{j},'./','/');
end
