function s=getvars(cstr,var)
%GETVARS 	Input arguments:
%		1) cstr: A cell (char) array containing expressions
%		2) var: The base character.
%
% Finds the highest numbered variable on the form <var><number> in the
% cstr cell array (using the maxvar method).
% A new cell (char) array is constructed, containing the following elements:
% {'<var>1', '<var>2', ..., <var><highest number>'}
%
% This method is called whenever a supplied xvars/wvars/uvars cell array
% is empty, or when any of them isn't specified. The result of this method
% will function as the new xvars/wvars/uvars cell array.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if isa(cstr,'cell')
	n=length(cstr);
else
	%A single string was sent to the function
	n=1;
	cstr={cstr};
end;

m=0;
for i=1:n
	m=max(m,maxvar(cstr{i},var));
end

s=cell(1,m);
for i=1:m
	s{i}=strcat(var,num2str(i));
end
