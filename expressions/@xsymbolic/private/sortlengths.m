function [carray, order]=sortlengths(carray);
%EXT2INT	Input arguments:
%		1) cell (char) array
%
% Sorts the cell array with respect to element length.
% Returns the sorted array and the corresponding index mapping.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

len=length(carray);
order=cumsum(ones(1,len));

% Prepare for BUBBLESORT
for i=1:len-1
	for j=1:len-i
		if length(carray{j})>length(carray{j+1})
			temp1=carray{j};
			carray{j}=carray{j+1};
			carray{j+1}=temp1;
			temp2=order(j);
			order(j)=order(j+1);
			order(j+1)=temp2;
		end
	end
end
