function c=sym2char(symexpr)
%SYM2CHAR	Input arguments:
%		1) symexpr: A symbolic array
%
% Converts the symbolic array to a cell (char) array.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

n=size(symexpr,2);	%columns
m=size(symexpr,1);	%rows

%Convert to cell array
c=cell(m,n);
for j=1:m
	for i=1:n
		c{j,i}=char(symexpr(j,i));
	end;
end;
