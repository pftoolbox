function v=maxvar(str, var);
%MAXVAR		Input arguments:
%		1) str: A string containing an expression
%		2) var: The base character.
%
% Returns the highest number of the variables on the form <var><number> in the
% str string.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

dbl=double(str);
varidx=findstr(str,var);
numidx=find(dbl>=double('0')&dbl<=double('9'));
v=0;
for i=1:length(varidx)
	currval=0;
	j=varidx(i)+1;
	while(length(find(numidx==j))~=0)
		currval=currval*10;
		currval=currval+dbl(j)-double('0');
		j=j+1;
	end
	v=max(v,currval);
end
