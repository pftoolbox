function expr=char2oneline(c,varargin);
%CHAR2ONELINE	Input arguments:
%		1) expr: An cell array containing string expressions
%
% Converts the cell elements to a large inline function.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

n=size(c,2);	%columns
m=size(c,1);	%rows

%Convert to inline representation
%Instead of using a cell array, we use a 2 dimensional inline
%expression since that's a lot faster and speed is our friend...
str='[';
for j=1:m
	for i=1:n
		str=strcat(str,c{j,i},',');
	end
	str=strcat(str(1:end-1),';');
end
str=strcat(str(1:end-1),']');

expr=inline(str,'x','t','w','u');
