function expr=char2inline(expr);
%CHAR2INLINE	Input arguments:
%		1) expr: An cell array containing string expressions
%
% Converts the cell elements to inline functions.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

m=size(expr,1);		%rows
n=size(expr,2);		%columns

for j=1:m
	for i=1:n
		expr{j,i}=inline(expr{j,i},'x','t','w','u');
	end
end
