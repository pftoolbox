function j=initgrad(expr,vars)
%INITGRAD	Input arguments:
%		1) expr: A cell (char) array containing expressions
%		2) vars: A cell (char) array containing variables
%
% Calculates the jacobian of expr with respect to vars.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

expression=char2sym(removedots(convert2char(expr)));
vars=char2sym(vars);
j=sym2char(jacobian(expression,vars));
