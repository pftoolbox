function expr=ext2int(expr,varargin);
%EXT2INT	Input arguments:
%		1) cell (char) array
%		2) variable cell array 1
%		3) base variable 1
%		2n) variable cell array n
%		2n+1) base variable array n		
%
% Converts a given state expression to internal representation using vector indices instead
% of the variables given in the variable arrays.
% Returns the new representation as a cell (char) array.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% These are used instead of internal variables (x(t), w(t), u(t)) to avoid collisions
% between them and the given xvars/wvars/uvars.
intcode={'_"_','_#_','_!_'};

m=size(expr,1);		%rows
n=size(expr,2);		%columns

nvars=(nargin-1)/2;

k=1;
nvarelements=0;
intsymbol=cell(1,nvars);
vars=cell(1,nvars);
for i=1:2:nargin-1
	intsymbol{k}=varargin{i+1};
	vars{k}=varargin{i};
	nvarelements=nvarelements+length(vars{k});
	k=k+1;
end;

allvars=cell(1,nvarelements);
allvarids=zeros(1,nvarelements);
allindices=zeros(1,nvarelements);
k=1;
for i=1:nvars
	v=vars{i};
	for j=1:length(v)
		allvars{k}=v{j};
		allvarids(k)=i;
		allindices(k)=j;
		k=k+1;
	end
end

[sortedvars order]=sortlengths(allvars);
sortedvarids=allvarids(order);
sortedindices=allindices(order);

for l=1:m
	for j=1:n
		for i=length(sortedvars):-1:1;
			x=strcat(intcode{sortedvarids(i)},'(');
			xi=strcat(x, num2str(sortedindices(i)), ',:)');
			expr{l,j}=strrep(expr{l,j},sortedvars{i},xi);			
		end
	end

end

for l=1:m
	for j=1:n
		for i=1:nvars
			expr{l,j}=strrep(expr{l,j},intcode{i},intsymbol{i});
		end
	end
end

