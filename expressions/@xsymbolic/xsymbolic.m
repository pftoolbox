function obj=xsymbolic(varargin)
% Holds an inline object, a symbolic expression or just a string.
% Will be treated as inlines internally.
%
% Syntax: (* = optional)
%
% obj = xsymbolic(expression, xvars*, uvars*, wvars*);
%
% In arguments:
%
% 1. expression
%	An inline object, a symbolic expression or a string.
% 2* xvars
%	A cell array containing the names of the states.
% 2* []
%	'xvars' is set to {'x1', 'x2', 'x3', ...}
% 3* uvars
%	A cell array containing the name of the elements forming u(t).
% 3* []
%	'uvars' is set to {'u1', 'u2', 'u3', ...}
% 4* wvars
%	A cell array containing the name of the elements forming w(t).
% 4* []
%	'wvars' is set to {'w1', 'w2', 'w3', ...}
%
% Out arguments:
%
% 1. obj
%	The resulting data object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin==0
	% Empty constructor
	expression={};
elseif isa(varargin{1},'xsymbolic')
	% Copy constructor
	obj = varargin{1};	%Copy
	return;			%Exit
else
	expression=varargin{1};
end

if isempty(expression)
	% Either the empty constructor was called, or the user supplied an
	% empty inargument;
	obj.expression={};
	obj.intexpr=[];
	obj.str='';
	obj.exprsize=[0,0];
	obj.gradx=[];
	obj.gradw=[];
	obj.xvars={};
	obj.uvars={};
	obj.wvars={};
	obj.islinear=false;
	obj.wchar='w';
	obj.description='Symbolic expression';
	obj.wvars_auto=true;	% PRIVATE
	obj=class(obj,'xsymbolic');
else
	% Ok, the expression is not empty. Let's move on!

	wchar='w';		% For use in the future

	% Calculate the amount of subexpressions
	if isa(expression,'cell')
		n=length(expression);
	else
		% A single object was supplied
		n=1;
		expression={expression};
	end;
	xvars={};
	uvars={};
	wvars={};

	if nargin>=2; xvars=varargin{2}; end;
	if nargin>=3; uvars=varargin{3}; end;
	if nargin>=4; wvars=varargin{4}; end;

	% Convert all subexpressions to char
	charexpr=convert2char(expression);

	% If *vars is empty, calculate default values.
	if isempty(xvars); xvars=getvars(charexpr,'x'); end;
	if isempty(wvars);
		wvars=getvars(charexpr,'w');
		wvars_auto=true;
	else
		wvars_auto=false;
	end
	if isempty(uvars); uvars=getvars(charexpr,'u'); end;

	% Generate the internal expression, for use with the feval command.
	intexpr=char2oneline(ext2int(charexpr,xvars,'x',wvars,'w',uvars,'u')');

	obj.expression=expression;
	obj.intexpr=intexpr;
	obj.str=expr2str(expression);
	obj.exprsize=[n,0];
	obj.gradx=[];
	obj.gradw=[];
	obj.xvars=xvars;
	obj.uvars=uvars;
	obj.wvars=wvars;
	obj.islinear=false;
	obj.wchar='w';
	obj.description='Symbolic expression';
	obj.wvars_auto=wvars_auto;	% PRIVATE
	obj=class(obj,'xsymbolic');
end
