function obj=xhandle(varargin)
% Holds a function handle.
%
% Syntax: (* = optional)
%
% obj = xhandle(expression, gradx*, gradw*);
%
% In arguments:
%
% 1. expression
%	Function handle to the expression. The in arguments are x, t, u, w.
% 2* gradx
%	Function handle to the gradient with respect to x. The in arguments are x, t, u, w.
% 2* []
%	'gradx' will be set to [], hence the EKF filter can not be used before
%	setting this property with the set command.
% 3* gradw
%	Function handle to the gradient with respect to w. The in arguments are x, t, u, w.
% 3* []
%	'gradw' will be set to [], hence the EKF filter can not be used before
%	setting this property with the set command.
%
% Out arguments:
%
% 1. obj
%	The resulting data object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
if nargin==0
	% Empty constructor
	expression=[];
elseif isa(varargin{1},'xhandle')
	% Copy constructor
	obj = varargin{1};
	return;			%Exit
else
	expression=varargin{1};
end;


% Declare the arguments
gradx=[];
gradw=[];

% Fetch arguments, if they exist
if nargin>=2; gradx=varargin{2}; end;
if nargin>=3; gradw=varargin{3}; end;

obj.expression=expression;
obj.str=func2str2(expression);
obj.exprsize=zeros(1,2);
obj.gradx=gradx;
obj.gradw=gradw;
obj.xvars={};
obj.uvars={};
obj.wvars={};
obj.islinear=false;
obj.wchar='w';
obj.description='Function handle expression';
obj=class(obj,'xhandle');
