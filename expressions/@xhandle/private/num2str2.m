function str=num2str2(A);
% Converts a double to a single-line string.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

m=size(A,1);
n=size(A,2);

if isempty(A)
	% Empty double
	str='[]';
elseif m==1&&n==1
	% Scalar. Don't use '[' and ']'
	str=num2str(A);
else
	str='[';
	for j=1:m
		str=[str, num2str(A(j,:)), '; '];
	end;
	str=str(1:end-2);
	str=[str,']'];

	% Remove consequtive spaces
	temp=[];
	while ~strcmp(str,temp)
		temp=str;
		str=strrep(str,'  ',' ');
	end

end
