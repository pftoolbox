function v=eval(obj,varargin)
% Method for evaluating an expression contained in a xhandle object.
%
% Syntax: (* = optional)
%
% v = eval(model, x*, t*, u*, w*);
%
% In arguments:
%
% 1. model
%	The model object that will be used.
% 2* x
%	A column vector or a scalar containing x(t).
% 3* t
%	A scalar containing the time t.
% 4* u
%	A column vector or scalar containing u(t).
% 5* w
%	A column vector or scalar containing w(t)
%
% Out arguments:
%
% 1. v
%	The result of the operation.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

expr=obj.expression;
if isempty(expr)
	error('Can not evaluate an empty expression!');
else
	v=feval(expr,varargin{:});
end
