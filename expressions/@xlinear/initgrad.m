function grad=initgrad(obj,diffvar)
% Generates a gradient with respect to the variable represented by the integer given in 'diffvar'
%
% Syntax: (* = optional)
%
% grad = initgrad(obj, diffvar);
%
% In arguments:
%
% 1. model
%	Non-empty xlinear data object
% 2. diffvar
%	Integer representing the variable to differentiate with respect to. 
%	The numbering is defined by the 'evalvar' property.
% Out arguments:
%
% 1. grad
%	The resulting gradient matrix

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

evalvar=obj.evalvar;
expression=obj.expression;

% Is diffvar in evalvar?
pos=find(diffvar==evalvar);

if ~isempty(pos)
	if length(evalvar)==1
		% evalvar==diffvar
		% Just return the expression matrix
		grad=expression;
	else
		% Multiple evaluation variables
		% We need to find the right columns to copy from the expression matrix.
		varsize=obj.varsize;
		% Start with an empty matrix
		grad=zeros(size(expression,1),varsize(pos));
		% Find the start position
		i=sum(varsize(evalvar(1:pos-1)));
		% Copy the columns
		grad(:,1:varsize(evalvar(pos)))=expression(:,1+i:i+varsize(evalvar(pos)));
	end;
else
	% Differentiation with respect to a variable not in 'evalvar'.
	% Return an empty matrix
	grad=[];
end
