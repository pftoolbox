function obj=xlinear(varargin)
% Holds a matrix which forms a linear expression.
%
% Syntax: (* = optional)
%
% obj = xlinear(expression, evalvar*, varsize*);
%
% In arguments:
%
% 1. expression
%	Matrix that represents a linear expression.
% 2* evalvar
%	Some data objects, such as xlinear that use matrix multiplication, don't support
%	xvars, uvars and wvars. These objects need to know what variable(s) to use in the
%	evaluation. 'evalvar' contains this information. The variables x, t, w and u
%	are represented by the numbers 1, 2, 3 and 4 respectively.
%	evalvar=1 means that x will be used in the evaluation.
%	evalvar=[1 3] means that [x; u] will be used (x and u are, like always,
%	row vectors or scalars). evalvar=[] means that no variable is used.
%	For arguments that uses xvars, uvars and wvars, this argument is redundant.
%	Example - the eval command of xlinear ('expression' is a matrix):
%	evalvar=[] returns expression
%	evalvar=1 returns expression*x
%	evalvar=[1 4 3] returns expression*[x; w; u]
%	When differentiating with respect to a variable not in the evalvar vector,
%	an empty matrix will be returned.
% 2* []
%	'evalvar' will be set to [], ie no variable will be used when evaluating.
% 3* varsize
%	Only needed when evaluating multiple variables, ie when 'evalvar' is a vector.
%	When differentiating an expression based on multiple variables, we need to know
%	the size of each variable (column vector) in order to extract the right columns of the
%	matrix. 'varsize' must be a vector of 4 elements representing the size of
%	[x t u w]. Not that if a particular variable isn't evaluated (ie its index is
%	not present in the evalvar vector), its size does not matter and can be set to 0.
%	Example: x=[1 2 3]', w=[1 2]', u=3 and evalvar=[1 4 3], the 'varsize' argument
%	must be set to [3 x 2 1], where x can be anything (preferably 1, since t is always
%	a scalar).
%	The gradient with respect to w will be a 6x2 matrix where column 4 and 5 will be
%	extracted from the expression matrix, and the remaining columns will contain zeros.
% 3* []
%	All columns in the expression matrix will be treated equally when differentiating.
%
% Out arguments:
%
% 1. obj
%	The resulting data object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
if nargin==0
	% Empty constructor
	expression=[];
elseif isa(varargin{1},'xlinear')
	% Copy constructor
	obj = varargin{1};
	return;			%Exit
else
	expression=varargin{1};
end;

intexpr=expression;

% Declare the arguments
evalvar=[];
varsize=[];

% Fetch arguments, if they exist
if nargin>=2; evalvar=varargin{2}; end;
if nargin>=3; varsize=varargin{3}; end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(evalvar)
	evalvar=[];	% evalvar default: evaluate no variable
end;

if isempty(varsize)
	varsize=[0 0 0 0];	% x t u w
end;

if size(varsize,1)~=1||size(varsize,2)~=4
	error('''varsize'' must be a row vector of size 4.');
end

obj.expression=expression;
obj.str=expr2str(expression);
obj.exprsize=size(expression);
obj.gradx=[];
obj.gradw=[];
obj.xvars={};
obj.uvars={};
obj.wvars={};
obj.evalvar=evalvar;
obj.varsize=varsize;
obj.islinear=true;    
obj.wchar='w';
obj.description='Linear time-invariant (LTI) expression';
obj=class(obj,'xlinear');
