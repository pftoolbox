function [idxstart,idxlength]=initgrad(obj,diffvar)
% Generates a gradient with respect to the variable represented by the integer given in 'diffvar'
%
% Syntax: (* = optional)
%
% grad = initgrad(obj, diffvar);
%
% In arguments:
%
% 1. model
%	Non-empty xltv data object
% 2. diffvar
%	Integer representing the variable to differentiate with respect to. 
%	The numbering is defined by the 'evalvar' property.
% Out arguments:
%
% 1. grad
%	The resulting gradient matrix

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

evalvar=obj.evalvar;

% Is diffvar in evalvar?
pos=find(diffvar==evalvar);

if ~isempty(pos)
	if length(evalvar)==1
		% evalvar==diffvar
		% 0=entire matrix!
		idxstart=0;
		idxlength=0;
	else
		% Multiple evaluation variables
		% We need to find the right columns to copy from the intexpr matrix.
		varsize=obj.varsize;

		% Find the start position
		i=sum(varsize(evalvar(1:pos-1)));

		idxstart=i;
		idxlength=varsize(evalvar(pos));
	end;
else
	idxstart=[];
	idxlength=[];
end
