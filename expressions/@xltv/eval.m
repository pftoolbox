function v=eval(obj,varargin)
% Method for evaluating an expression contained in a xltv object.
%
% Syntax: (* = optional)
%
% v = eval(model, x*, t*, u*, w*);
%
% In arguments:
%
% 1. model
%	The model object that will be used.
% 2* x
%	A column vector or a scalar containing x(t).
% 3* t
%	A scalar containing the time t.
% 4* u
%	A column vector or scalar containing u(t).
% 5* w
%	A column vector or scalar containing w(t)
%
% Out arguments:
%
% 1. v
%	The result of the operation.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

t=varargin{2};

if obj.interpolate
	tvec=find(obj.Ts>=t);

	if isempty(tvec)
		% t>Ts(max)
		Ts=obj.Ts;
		array=obj.array;
		idxup=length(Ts);
		idxlow=idxup-1;
		tup=Ts(idxup);
		tlow=Ts(idxlow); % An error here means that Ts contains too few elements for interpolation
		vlow=array(:,:,idxlow);
		vup=array(:,:,idxup);
		v=vup+(vup-vlow)./(tup-tlow).*(t-tup);			
		return;		
	end

	idxup=tvec(1);
	Ts=obj.Ts;
	tup=Ts(idxup);
	if tup==t
		% Right on target. No interpolation needed.
		v=obj.array(:,:,idxup);
	elseif idxup>1
		% t is somewhere in Ts
		array=obj.array;
		idxlow=idxup-1;
		tlow=Ts(idxlow);
		vlow=array(:,:,idxlow);
		vup=array(:,:,idxup);
		v=vlow+(vup-vlow)./(tup-tlow).*(t-tlow);
	else
		% idxup==1 and t~=tup, so t<Ts(min)
		array=obj.array;
		v=array(:,:,1)+(array(:,:,2)-array(:,:,1))./(Ts(2)-tup).*(t-tup);			
	end
else
	tvec=find(obj.Ts>=t);
	if isempty(tvec)
		v=obj.array(:,:,end);
	else
		v=obj.array(:,:,tvec(1));
	end
end

if obj.evalvar
	% 'evalvar' is non-zero and non-empty
	% Variables will be used in the evaluation.
	evalvar=obj.evalvar;
	if length(evalvar)>1
		% Evaluate using multiple variables.
		evalvar=obj.evalvar;
		vec=varargin{evalvar(1)};
		for i=2:length(evalvar);
			vec=[vec; varargin{evalvar(i)}];
		end; 
		v=v*vec;
	else
		% Evalate using one single variable.
		% The code is optimized for this case, hence the if statement.
		% For nicer code, let multiple variables block above handle this
		% case (it will do that automatically, just remove the if statement including
		% the evaluation below)
		v=v*varargin{evalvar};
	end
else
	% 'evalvar' is zero or empty.
	% No variables will be used.
	v=[];
end;
