function obj=xltv(varargin)
% Holds an 3d array 2d matrices which forms linear expressions.
%
% Syntax: (* = optional)
%
% obj = xltv(expression, Ts*, evalvar*, varsize*, interpolate*);
%
% In arguments:
%
% 1. array
%	Data array containing expressions.
% 2* Ts
%	Sample point vector
% 2* []
%	'Ts' is set to [0:size(array,3)-1]
% 3* evalvar
%	Some data objects, such as xltv that use matrix multiplication, don't support
%	xvars, uvars and wvars. These objects need to know what variable(s) to use in the
%	evaluation. 'evalvar' contains this information. The variables x, t, w and u
%	are represented by the numbers 1, 2, 3 and 4 respectively.
%	evalvar=1 means that x will be used in the evaluation.
%	evalvar=[1 3] means that [x; u] will be used (x and u are, like always,
%	row vectors or scalars).
%	Example - the eval command of xltv ('expression' is a matrix):
%	evalvar=1 returns expression*x
%	evalvar=[1 4 3] returns expression*[x; w; u]
%	Note that xltv doesn't support empty evalvar vectors. For that
%	kind of functionallity, see xtable.
% 3* []
%	'evalvar' will be set to 1, ie x will be multiplied to the right when evaluating.
% 4* varsize
%	Only needed when evaluating multiple variables, ie when 'evalvar' is a vector.
%	When differentiating an expression based on multiple variables, we need to know
%	the size of each variable (column vector) in order to extract the right columns of the
%	matrix. 'varsize' must be a vector of 4 elements representing the size of
%	[x t u w]. Not that if a particular variable isn't evaluated (ie its index is
%	not present in the evalvar vector), its size does not matter and can be set to 0.
%	Example: x=[1 2 3]', w=[1 2]', u=3 and evalvar=[1 4 3], the 'varsize' argument
%	must be set to [3 x 2 1], where x can be anything (preferably 1, since t is always
%	a scalar).
%	The gradient with respect to w will be a 6x6 matrix where column 4 and 5 will be
%	extracted from the expression matrix, and the remaining columns will contain zeros.
% 4* []
%	All columns in the expression matrix will be treated equally when differentiating.
% 5* interpolate
%	Sets linear interpolation on=true or off=false.
% 5* []
%	'interpolate' is set to false, ie interpolation is turned off.
%
% Out arguments:
%
% 1. obj
%	The resulting data object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
if nargin==0
	% Empty constructor
	array=[];
elseif isa(varargin{1},'xltv')
	% Copy constructor
	obj = varargin{1};
	return;			%Exit
else
	array=varargin{1};
end;

% Declare the arguments
Ts=[];
evalvar=[];
varsize=[];
interpolate=[];

% Fetch arguments, if they exist
if nargin>=2; Ts=varargin{2}; end;
if nargin>=3; evalvar=varargin{3}; end;
if nargin>=4; varsize=varargin{4}; end;
if nargin>=5; interpolate=varargin{5}; end;

if isempty(Ts)
	if length(array)
		Ts=[0:size(array,3)-1];		% Ts default
	end
end

if isempty(evalvar)
	evalvar=1;	% evalvar default = use x as multiplicator
end;

if isempty(varsize)
	varsize=[0 0 0 0];	% x t u w
end;

if isempty(interpolate)
	interpolate=false;	% interpolate default
end;

exprsize=[size(array,1), size(array,2)];

if length(array)
	if size(Ts)~=size(array,3)
		error('''Ts'' and ''array'' do not match');
	end
end


if size(varsize,1)~=1||size(varsize,2)~=4
	error('''varsize'' must be a row vector of size 4.');
end


obj.array=array;
obj.str='No string representation';
obj.exprsize=exprsize;
obj.Ts=Ts;
obj.gradx=[];
obj.gradw=[];
obj.xvars={};
obj.uvars={};
obj.wvars={};
obj.islinear=true;    
obj.evalvar=evalvar;
obj.varsize=varsize;
obj.wchar='w';
obj.interpolate=interpolate;
obj.description='Linear time-variant (LTV) expression';
obj.gradx_idxstart=[];		% PRIVATE
obj.gradx_idxlength=[];		% PRIVATE!
obj.gradw_idxstart=[];		% PRIVATE
obj.gradw_idxlength=[];		% PRIVATE!
obj=class(obj,'xltv');
