function obj=initgradx(obj)
% Intializes the gradient with respect to x. Used by, for instance, the EKF filter.
% The result is found in the 'gradx' property.
%
% Syntax: (* = optional)
%
% obj = initgradx(obj);
%
% In arguments:
%
% 1. obj
%	Data object to be differentiated.
% Out arguments:
%
% 1. obj
%	Resulting object
%

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Empty for this object
