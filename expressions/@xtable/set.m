function objout = set(obj,varargin)
% Sets the properties of an object.
%
% set(obj, 'propertyname', value) sets the property 'propertyname' of the object 'obj'
% to the value 'value'. 
%
% An equivalent syntax is obj.propertyname = value.
%
% set(obj, 'property1', value1, 'property2', value2, ...) sets multiple property values.
% set(obj, 'property') displays legitimate values for the specified property of 'obj'.
% set(obj) displays all properties of 'obj' and their admissible values.
%
% If an output argument is specified, the modified object will be assigned to this and
% no modifications will be done to the input object.
%
% Type 'props(obj)' for more details on the properties of the object 'obj'.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin==1
	% Only 'obj' is supplied. Display the list of properties.
	displayprops(obj);
elseif nargin==2;
	% A parameter was given, but no value. Display information about the property.
	prop_name = varargin{1};
	[props,rprops]=pnames;
	if findcstr(props,prop_name)||findcstr(rprops,prop_name)
		disp(pformat(get(obj,prop_name)));
	else
		error('Unknown property');
	end
else
	if isempty(inputname(1))&&nargout<1
		error('The first argument must be a named variable if no output argument is given.')
	elseif rem(nargin-1,2)~=0,
		error('Property/value pairs must come in even number.')
	end

	property_argin = varargin;
	while length(property_argin) >= 2,
		prop_name = property_argin{1};
		v = property_argin{2};
		property_argin = property_argin(3:end);
		switch prop_name

		% <custom>
		% Here we handle properties that need custom treatment (or need to be 
		% processed really fast)

		case {'array'}
			exprsize=[size(v,1), size(v,2)];
			obj.array=v;
			obj.exprsize=exprsize;
		case {'str'}

		% </custom>

		otherwise
			[props,rprops]=pnames;
			if findcstr(props,prop_name)
				eval(strcat('obj.',prop_name,'=v;'));
			elseif findcstr(rprops,prop_name)
				error([prop_name,' is a read-only property'])
			else
				error([prop_name,' is not a valid property'])
			end
		end
	end

	if nargout==0
		% No output variable was specified by the user.
		% We'll do the assignment manually in the caller's workspace.
		objname = inputname(1);
		assignin('caller',objname,obj)
	else
		objout=obj;
	end;
end
