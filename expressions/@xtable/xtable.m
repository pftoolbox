function obj=xtable(varargin)
% Holds an 3d array 2d matrices which forms expressions.
%
% Syntax: (* = optional)
%
% obj = xtable(array, Ts, interpolate);
%
% In arguments:
%
% 1. array
%	Data array containing expressions.
% 2. Ts
%	Sample point vector
% 3* interpolate
%	true or false
% 3* []
%	'interpolate' is set to false.
%
% Out arguments:
%
% 1. obj
%	The resulting data object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
if nargin==0
	% Empty constructor
	array=[];
	Ts=[];
elseif isa(varargin{1},'xtable')
	% Copy constructor
	obj = varargin{1};
	return;			%Exit
else
	array=varargin{1};
	Ts=varargin{2};
end;

if length(array)
	if size(Ts)~=size(array,3)
		error('''Ts'' and ''array'' do not match');
	end
end

% Declare the arguments
interpolate=[];

% Fetch arguments, if they exist
if nargin>=3; interpolate=varargin{3}; end;


if isempty(interpolate)
	interpolate=false;	% No interpolation. Nearest upper neighbor is used.
end;

exprsize=[size(array,1), size(array,2)];

obj.array=array;
obj.str='No string representation';
obj.exprsize=exprsize;
obj.gradx=[];
obj.gradw=[];
obj.xvars={};
obj.uvars={};
obj.wvars={};
obj.islinear=false;
obj.wchar='w';
obj.interpolate=interpolate;
obj.Ts=Ts;
obj.description='Data array';
obj=class(obj,'xtable');
