function pfpath(varargin)
% Adds the necessary subdirectories to the Matlab path.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin>=1
	action=varargin{1};
else
	action=[];
end
disp(' ');
disp('This script adds the necessary subdirectories to your Matlab path.');
if action
	addpath expressions filters GUI models noise resampling simulators;
        disp('Operation completed sucessfully. You will not have to do this agian.');
else
	disp('Usage: pfpath(true)');
	disp(' ');
	disp('No modifiactions were done.');
end;
disp(' ');
