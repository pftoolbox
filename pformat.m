function str=pformat(property)
% pformat v0.1 (2005-06-09 14:18)
%
% Formats the text output of a property value
% Used by the get/set functions, and will become private after the final
% version is created.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

pclass=class(property);
m=size(property,1);
n=size(property,2);

switch pclass
case 'double'
	if m==0&&n==0	% For instance, m=0, n=1 is not empty for us, but for the isempty
			% function, and that's why we don't use it here.
			% Try a 'get(ss)' to see the problem, and we want to follow standards.
		str='[]';
	elseif m==1&n==1
		str=num2str(property);
	else
		str=sprintf('[%dx%d double]',m,n); 
	end;
case 'cell'
	if m==0&&n==0	% See comment above
		str='{}';
	else
		str=sprintf('{%dx%d cell}',m,n); 
	end
case 'logical'
	if m==1&&n==1	% See comment above
		if property==true
			str='[1x1 logical: true]';
		else
			str='[1x1 logical: false]';
		end
	else
		str=sprintf('[%dx%d logical]',m,n); 
	end
otherwise
	str=pclass;
end
