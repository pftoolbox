function plot(obj,varargin);
% Plots the data contained in the object.
%
% Syntax: (* = optional)
%
% plot(simobj, state*, colorscheme*);
% plot(simobj, states*, colorscheme*);
%
% In arguments:
%
% 1. simobj
%	Simulator object containing the data to be plotted
% 2* state
%	The state x(state) will be plotted, in one-dimension (using the plot1d method).
% 2* states
%	A pair of integers [sx, sy].
%	The x and y axes will represent x(sx) and x(sy) respectively.
%	This will result in a two-dimensional plot (using the plot2d method).
% 2* []
%	If simobj.xhat contains multiple states, 'states' is set to [1 2] and
%	the plot will be two-dimensional. If not, 'state' is set to 1 and a
%	one-dimensional plot will be done.
% 3* colorscheme
%	An integer that decides what colors will be used in the figure.
%	Two colors will be picked using getcolors(colorstate) 
% 3* []
%	colorscheme is set to 1

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if isempty(obj.x)
	error('Object contains no data');
end;

% Declare the arguments
states=[];
colorscheme=[];

% Fetch arguments, if they exist
if nargin>=2; states=varargin{1}; end
if nargin>=3; colorscheme=varargin{2}; end

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(states)
	if size(obj.x,1)>1
		states=[1 2];	% Default state indices for multi dimensional states
	else
		states=1;	% Default state to visualize in the one dimensional case
	end
end

if isempty(colorscheme)
	colorscheme=1; 		% colorscheme default
end

color1=getcolors(colorscheme);

x=obj.x(:,end);

if length(states)>1
	% plot2d
	plot(x(states(1)), x(states(2)), strcat(color1,'x'));
else
	% plot1d
	plot(x(states(1)), 0, strcat(color1,'x'));
end
