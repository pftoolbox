function resultbuf=append(prevbuf, nextbuf, resultsize)
% append(prevbuf, nextbuf, resultsize)
%
% Appends the columns of matrix nextbuf, to the columns of matric prevbuf and returns
% the last resultsize columns of the concatenated result.
% If resultsize is negative or zero, the resulting vector will be returned without any
% truncation (i.e. there is no size limit).
%
% Both matrices must be two-dimensional.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if length(prevbuf)	% _slightly_ faster version of ~isempty(prevbuf) :)
	% prevbuf is not empty. append nextbuf to it.
	nextlen=size(nextbuf,2);
	prevlen=size(prevbuf,2);
	if (nextlen+prevlen>resultsize)&&(resultsize>0)
		% The result needs to be truncated to fit the size specified by resultsize
		if nextlen>=resultsize
			% If nextbuf is really big, we don't need to care about prevbuf
			% (all of it will be lost after the truncation anyway)
			% Truncate nextbuf to make it fit the buffer!
			resultbuf=nextbuf(:,end-resultsize+1:end);
		else
			% Append, with truncation of prevbuf!
			resultbuf=zeros(size(nextbuf,1), resultsize);
			resultbuf(:,1:resultsize-nextlen)=prevbuf(:,end-resultsize+nextlen+1:end);
			resultbuf(:,resultsize-nextlen+1:resultsize)=nextbuf;
		end
	else
		% No truncation is necessary. Just append!
		resultbuf=zeros(size(nextbuf,1), prevlen+nextlen);
		resultbuf(:,prevlen+1:prevlen+nextlen)=nextbuf;
		resultbuf(:,1:prevlen)=prevbuf;
	end
else
	% prevbuf was empty so no appending is necessary
	resultbuf=nextbuf;
end
