function obj=simulator(varargin)
% Constructor for the simulator class
%
% Syntax: (* = optional)
%
% simobj = simulator(model, t*, steps*);
%
% In arguments:
%
% 1. model
%	The model object that will be used for the simulation.
% 2* t
% 	Current time.
% 	Will be used as the time of the next simulation step, if nothing else is
% 	specified in the call to the simulate method. See 'help simulator/simulate'
% 	for more information.
% 2* []
%	t=0 will be used
% 3* steps
%	If this argument is specified, an initial simulation will be made. The amount
%	of simulation steps will be equal to the value of the argument.
% 3* []
%	No initial simulation will be carried out.
%
% Out arguments:
%
% 1. simobj
%	Simulator object, ready to be used for filtering.
%
% For more help, type 'props(simulator)'

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin==0
	% Empty constructor
	model=[];
	x0=[];
elseif isa(varargin{1},'simulator')
	% Copy constructor
	obj = varargin{1};	%Copy
	return;			%Exit
else
	model=varargin{1};
	x0=calc_x0(model,1);
end

% Declare the arguments
t=[];
steps=[];

% Fetch arguments, if they exist
if nargin>=2; t=varargin{2}; end;
if nargin>=3; steps=varargin{3}; end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(t)
	t=0;			% t default
end;

% Initialize property structure
obj.model=model;
obj.t=t;
obj.Ts=[];
obj.x=[];
obj.y=[];
obj.u=[];
obj.x0=x0;
obj.historysize=0;
obj.x_history=[];
obj.y_history=[];
obj.u_history=[];
obj.Ts_history=[];
obj=class(obj,'simulator');

if ~isempty(steps);
	% Do an initial simulation
	simulate(obj,steps);
	% To follow the Matlab OO standard, use the row below instead:
	% obj=simulate(obj,steps);
end
