function displayprops(obj)
% displayprops v0.1 (2005-06-09 14:12)

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

[props,rprops]=pnames;

% Determine the maximum character length of the property names
proplength=0;
for i=1:length(props); proplength=max(length(props{i}),proplength); end;
for i=1:length(rprops); proplength=max(length(rprops{i}),proplength); end;

fprintf('\nProperties:\n\n');

fstring=strcat('%+',num2str(proplength),'s : %s\n');
for i=1:length(props)
	fprintf(fstring,props{i},pformat(get(obj,props{i})));
end;

% If read-only properties exist, display them.
if length(rprops)
	fprintf('\nRead-only properties:\n\n');
	for i=1:length(rprops)
		fprintf(fstring,rprops{i},pformat(get(obj,rprops{i})));
	end;
end
