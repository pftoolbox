function x = random(OBJ, t, varargin)
% RANDOM Generate IID samples from Gaussian distribution
%   R=RANDOM(W, T, M, N, ...) or R=RANDOM(W, T, [M, N, ...]) returns a
%   MxNx... matrix with IID samples. (T is ignored.)

% Copyright (C) 2005  Gustaf Hendeby, Jakob Ros�n
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Syntax should be identical with standard RANDOM from Stat Toolbox. 
  
  % FIXME: Improve input check
  error(nargchk(3, inf, nargin));
  N = cell2mat(varargin);

  if OBJ.n==1
    x = OBJ.mu + sqrt(OBJ.R) * randn(N);
  else
    % FIXME:  Consider the treatment of this case.
    if N(1) ~= 1
      error('Cannot produce random number arrays for N-dim Gaussians.');
    end
    
    x = OBJ.Rsqrtm*randn([OBJ.n, N(2)]);
    for i = 1:OBJ.n			% Probably faster than a repmat
      x(i, :) = x(i, :) + OBJ.mu(i);
    end
  end