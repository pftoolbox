function OBJ = gauss(varargin)
% GAUSS Gaussian noise object
%   GAUSS() creates a N(0, 1) noise
%   
%   GAUSS(W) where W is GAUSS object creates a copy of W
%   
%   GAUSS(R) where R is a symmetric positive semi-definite matrix,
%   generates the noise, N(0, R).
%   
%   GAUSS(MU, R) where MU is a vector and R as above, yields N(MU, R). 

% Copyright (C) 2005  Gustaf Hendeby, Jakob Ros�n
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
  
error(nargchk(0, 2, nargin));
nin = nargin;
if nin == 0				% Empty constructor
	mu = 0; R = 1; n = 1;
elseif nin == 1 && isa(varargin{1}, 'gauss')% Copy constructor
	OBJ = varargin{1};
	return;
elseif nin == 1
	R = varargin{1};
	n = length(R);
	mu = zeros(n, 1);
else
	mu = varargin{1};
	R = varargin{2};
	n = length(R);
	if size(mu,2)>1
		% A row vector was supplied
		mu=mu';
	end;
end

if size(mu,2)>1
	error('Mu must be a one dimensional vector');
end;

if ~isequal(R,R')
	error('R must be symmetric');
end;

if length(mu)~=n
	error('The dimensions of R and Mu must agree')
end;
 
% This is an expensive calculation, so we'd better do it only once
Rsqrtm=sqrtm(R);	% Private property, not included in pnames.m

OBJ = class(struct('mu', mu, 'R', R, 'R_str', expr2str(R), 'n', n, 'Rsqrtm', Rsqrtm, 'description', 'Gaussian noise (gauss object)'), 'gauss');
