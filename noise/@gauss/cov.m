function R = cov(obj, t)
% Returns the covariance matrix of the noise object for a given time t.
%
% Syntax: (* = optional)
%
% R = cov(obj, t);
%
% In arguments:
%
% 1. obj
%	The noise object
% 2. t
%	The actual time
%
% Out arguments:
%
% 1. R
%	The covariance at time t

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Just return the covariance matrix R. Gaussian noise does not depend on time.
R=obj.R;
