function v=h_general(obj,x,t,u,varargin)
% Calculates: h(x,t,u) + e(t)
%
% Syntax: (* = optional)
%
% y = h_general(model, x, t, u, e*);
%
% In arguments:
%
% 1. model
%	Model object
% 2. x
%	Column vector or scalar containing x(t)
% 3. t
%	Scalar containing the time of the operation.
% 4. u
%	Column vector or scalar containing deterministic data for this particular step.
%	Redundand argument for this model.
% 5* e
%	Column vector or scalar containing e(t) for this particular step.
% 5* []
%	e(t) is drawn from model.e
%
% Out arguments:
%
% 1. y
%	The result of the operation, y(t)

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Evaluate h(x,t,u)
v=eval(obj.h,x,t,u,[]);

% Handle the measurement noise
e=[];

% Fetch the e parameter
if nargin>=5
	e=varargin{1};
end;

if isempty(e)
	% No e was given. Draw from model.e
	e=random(obj.e, t, 1, size(v,2));
	v=v+e;	% An error message here means that e has wrong dimensions
else
	% e(t) was supplied. Use that value
	v=v+e;	% An error message here means that the supplied e has wrong dimensions
end

