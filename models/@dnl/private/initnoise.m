function newexpr=initnoise(noise);
% Used to initialize noise object. If an object is supplied, it will be returned without
% modifications. If a double is supplied, a new 'gauss' object will be generated.
%
% Syntax: (* = optional)
%
% obj = initnoise(covariance);
% obj = initnoise(variance);
% obj = initnoise(noiseobj);
%
% In arguments:
%
% 1. covariance
%	A covariance matrix, that will be sent to the 'gauss' constructor, to create a new
%	normally distributed noise object.
% 1. variance
%	A vector containing the diagonal of a covariance matrix, that will be sent to the
%	'gauss' constructor, to create a new normally distributed noise object.
% 1. noiseobj
%	Will be returned without modifications.
%
% Out arguments:
%
% 1. obj
%	The resulting noise object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

switch class(noise)
case 'double';
	if size(noise,1)==size(noise,2)
		% A covariance matrix was supplied
		newexpr=gauss(noise);
	else
		if min(size(noise,1),size(noise,2))>1
			error('Invalid variance vector');
		end;
		newexpr=gauss(diag(noise));
	end;
otherwise
	newexpr=noise;
end;
