function newexpr=initdata(expr, xvars, uvars, wvars, evalvar);
% Assigns the expression to the appropriate data object. If an unknown expression type
% (such as a custom data object) is given, it will be returned without modifications.
%
% Syntax: (* = optional)
%
% dataobj = initdata(expr, xvars, wvars, uvars, evalvar);
%
% In arguments:
%
% 1. expr
%	An expression
% 2. xvars
%	A cell (char) array containing the state variables
% 3. uvars
%	A cell (char) array containing the u(t) variables
% 4. wvars
% 	A cell (char) array containing the w(t) variables
% 5. evalvar
%	Some data objects, such as xlinear that use matrix multiplication, don't support
%	xvars, uvars and wvars. These objects need to know what variable(s) to use in the
%	evaluation. 'evalvar' contains this information. evalvar=1 means that x will be used in
%	the evaluation. evalvar=[1 3] means that [x; u] will be used (x and u are, like always,
%	row vectors or scalars). evalvar=0 means that no variable is used.
%	For arguments that uses xvars, uvars and wvars, this argument is redundant.
%	Example - the eval command of xlinear ('expression' is a matrix):
%	evalvar=0 returns expression
%	evalvar=1 returns expression*x
%	evalvar=[1 3 2] returns expression*[x; u; w]
%
% Out arguments:
%
% 1. dataobj
%	The resulting data object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

switch class(expr)
case 'char';
	newexpr=xsymbolic(expr,xvars,uvars,wvars);
case 'cell';
	newexpr=xsymbolic(expr,xvars,uvars,wvars);
case 'inline';
	newexpr=xsymbolic(expr,xvars,uvars,wvars);
case 'sym';
	newexpr=xsymbolic(expr,xvars,uvars,wvars);
case 'double';
	newexpr=xlinear(expr,evalvar);
otherwise
	newexpr=expr;
end
