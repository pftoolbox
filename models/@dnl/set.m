function objout = set(obj,varargin)
% Sets the properties of an object.
%
% set(obj, 'propertyname', value) sets the property 'propertyname' of the object 'obj'
% to the value 'value'. 
%
% An equivalent syntax is obj.propertyname = value.
%
% set(obj, 'property1', value1, 'property2', value2, ...) sets multiple property values.
% set(obj, 'property') displays legitimate values for the specified property of 'obj'.
% set(obj) displays all properties of 'obj' and their admissible values.
%
% If an output argument is specified, the modified object will be assigned to this and
% no modifications will be done to the input object.
%
% Type 'props(obj)' for more details on the properties of the object 'obj'.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin==1
	% Only 'obj' is supplied. Display the list of properties.
	displayprops(obj);
elseif nargin==2;
	% A parameter was given, but no value. Display information about the property.
	prop_name = varargin{1};
	[props,rprops]=pnames;
	if findcstr(props,prop_name)||findcstr(rprops,prop_name)
		disp(pformat(get(obj,prop_name)));
	else
		error('Unknown property');
	end
else
	if isempty(inputname(1))&&nargout<1
		error('The first argument must be a named variable if no output argument is given.')
	elseif rem(nargin-1,2)~=0,
		error('Property/value pairs must come in even number.')
	end

	property_argin = varargin;
	while length(property_argin) >= 2,
		prop_name = property_argin{1};
		v = property_argin{2};
		property_argin = property_argin(3:end);
		switch prop_name

		% <custom>
		% Here we handle properties that need custom treatment (or need to be 
		% processed really fast)

		case 'x0'
			if size(v,2)>1
				% A row vector! Transpone it
				obj.x0=v';
			else
				obj.x0=v;
			end
		case 'w'
			obj.w = initnoise(v);
		case 'e'
			obj.e = initnoise(v);
		case 'p0'
			obj.p0 = initnoise(v);
		case 'fgradx'
			f=obj.f;
			obj.f = set(f,'gradx',v);
		case 'fgradw'
			f=obj.f;
			obj.f = set(f,'gradw',v);
		case 'hgradx'
			h=obj.h;
			h = set(h,'gradx',v);
			obj.h=h;
		case 'hgrade'
			h=obj.h;
			obj.h = set(h,'gradw',v);
		case {'f','gw','gu','h','hu'}
			if strcmp(prop_name,'f')||strcmp(prop_name,'h')
				% f(x,t) and h(x,t) depends on x by default
				evalvar=1;
			else
				% the rest are independent of x (by default)
				evalvar=0;
			end
			% Get the previous model.
			exprobj=obj.(prop_name);
			xvars=obj.xvars;
			if isempty(xvars)
				% The xvars property of the model is empty.
				% Use the xvars of the previous object.
				xvars=get(exprobj,'xvars');
			end
			% Initialize new data object.
			exprobj=initdata(v,xvars,{},{},evalvar);
			% Store the new object within the model.
			obj.(prop_name)=exprobj;
			
		% </custom>

		otherwise
			[props,rprops]=pnames;
			if findcstr(props,prop_name)
				eval(strcat('obj.',prop_name,'=v;'));
			elseif findcstr(rprops,prop_name)
				error([prop_name,' is a read-only property'])
			else
				error([prop_name,' is not a valid property'])
			end
		end
	end

	if nargout==0
		% No output variable was specified by the user.
		% We'll do the assignment manually in the caller's workspace.
		objname = inputname(1);
		assignin('caller',objname,obj)
	else
		objout=obj;
	end;
end
