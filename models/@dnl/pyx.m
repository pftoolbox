function pr=pyx(obj,y,x,t,u);
% Calculates p(y|x), ie the probability of y given x.
%
% Syntax: (* = optional)
%
% pr = pyx(model, y, x, t, u);
%
% In arguments:
%
% 1. model
%	Model object.
% 2. y
%	Column vector or scalar containing y
% 3. x
%	Column vector or scalar containing x
% 4. t
%	Scalar containing the time of the operation.
% 5. u
%	Column vector or scalar containing deterministic data.
%
% Out arguments:
%
% 1. pr
%	The resulting probability.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

yrep=repmat(y,1,size(x,2));
ypred=eval(obj.h,x,t,u,0);
pr=pdf(obj.e,t,yrep-ypred);
