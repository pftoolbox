function v=hgrade_general(obj,x,t,u,varargin)
% Calculates: grad_e[h(x,t) + hu(x,t)*u(t) + e(t)]
%
% Syntax: (* = optional)
%
% grad_e = hgrade_general(model, x, t, u, e*);
%
% In arguments:
%
% 1. model
%	Model object.
% 2. x
%	Column vector or scalar containing x
%	Redundant argument for this model, because x is elimiated by the differentiation.
% 3. t
%	Scalar containing the time of the operation.
%	Redundant argument for this model, because t is elimiated by the differentiation.
% 4. u
%	Column vector or scalar containing deterministic data for this particular step.
%	Redundant argument for this model, because u(t) is eliminated by the differentiation.
% 5* e
%	Column vector or scalar containing e(t) for this particular step.
%	Redundant argument for this model, since this is eliminated by the differentiation.
%
% Out arguments:
%
% 1. grad_w = 1
%	The result of the operation: the gradient with respect to e.
%	grad_w always equals one for this model.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% This should be property or something for the sake of optimization
%v=eye(size(get(model,'R')));

% A scalar will PROBABLY be enough, since the "real" gradient will be the identity matrix
% of the same size as R. Since V*R*V' in the EKF filter produces the same result with
% a scalar, with much fast execution time, this little optimization will probably work.
% if not, just uncomment the above code...
v=1;						% (... and comment this line!)
