function obj=initgradients(obj)
% Initializes the gradients of the data objects f and h.
%
% Syntax: (* = optional)
%
% model = initgradients(model);
%
% In arguments:
%
% 1. model
%	Model object.
%
% Out arguments:
%
% 1. model
%	Model object with gradients.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

obj.f=initgradx(obj.f);
obj.f=initgradw(obj.f);
obj.h=initgradx(obj.h);
