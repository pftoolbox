function obj = pfsys(varargin)
% Constructor for the DNL (Discrete Non-Linear) model
%
% x(t+T) = f(x,u,w,t)
%   y(t) = h(x,u,t) + e(t)
%
% Syntax: (* = optional)
%
% model = dnl(f, h, x0, w, e, p0*, T*, xvars*, uvars*, wvars*);
%
% In arguments:
%
% 1. f
%	Data object containing f.
%	If a double (matrix), or a cell array of strings (or inlines) is given, it will be
%	used to create an apropriate data object.
% 2. h
%	Data object containing h
%	If a double (matrix), or a cell array of strings (or inlines) is given, it will be
%	used to create an apropriate data object.
% 3. x0
%	A column vector containing the expectation of the initial state estimate.
%	Each row represents a state.
% 4. w
%	Noise object containing w.
%	It can also be a covariance matrix or a vector containing the diagonal of a
%	diagonal covariance matrix. This will be used to create a gaussian noise object
%	for use as w.
% 5. e
%	Noise object containing e
%	It can also be a covariance matrix or a vector containing the diagonal of a
%	diagonal covariance matrix. This will be used to create a gaussian noise object
%	for use as e.
% 6* p0
%	Noise object containing p0, that has to generate tuples of the same size as x0.
%	It can also be a covariance matrix or a vector containing the diagonal of a
%	diagonal covariance matrix. This will be used to create a gaussian noise object
%	for use as p0.
% 6* []
%	'p0' is set to independent gaussian noise with variance 1.
% 7* T
%	Sample time.
% 7* []
%	'T' is set to 1.
% 8* xvars
%	A cell array containing the names of the states.
% 8* []
%	'xvars' is set to {'x1', 'x2', 'x3', ...}
% 9* uvars
%	A cell array containing the names of the u(t) components.
% 9* []
%	'uvars' is set to {'u1', 'u2', '3', ...}
% 10* wvars
%	A cell array containing the names of the noise components.
% 10* []
%	'wvars' is set to {'w1', 'w2', 'w3', ...}
%
% Out arguments:
%
% 1. model
%	The resulting dnl model object.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin==0
	% Empty constructor
	% Will create a non-sense object, to satisfy Matlab standards
	obj.f=[];
	obj.h=[];
	obj.x0=[];
	obj.w=[];
	obj.e=[];
	obj.p0=[];
	obj.T=1;
	obj.xvars={};
	obj.uvars={};
	obj.wvars={};
	obj.description='Discrete Non-Linear model (DNL)';
	obj=class(obj,'dnl');
	return;				%Exit
elseif nargin==1
	argin=varargin{1};
	if isa(argin,'dnl')
		% Copy constructor
		obj = argin;		%Copy
		return;			%Exit
	end
end;

error(nargchk(5, 10, nargin));

% Fetch x0
x0=varargin{3};
if (size(x0,1)==1)&&(size(x0,2)>1);
	x0=x0';		% A column vector was supplied. Transpose it!
end;

% Declare the arguments
p0temp=[];
T=[];
xvars={};
uvars={};
wvars={};

% Fetch arguments, if they exist
if nargin>=6; p0temp=varargin{6}; end;
if nargin>=7; T=varargin{7}; end;
if nargin>=8; xvars=varargin{8};  end;
if nargin>=9; uvars=varargin{9};  end;
if nargin>=10; wvars=varargin{10};  end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(p0temp)
	p0temp=eye(length(x0));	% p0 default, ind. gaussian noise with variance 1
end;

if isempty(T)
	T=1;			% T default
end;

if isempty(xvars)
	% Generate a xvar vector
	xvars=generatevars('x',length(x0));
elseif length(xvars)~=length(x0)
	error('The number of elements in ''xvars'' does not match the number of states');
end


% Initialize the noise (supplied as an argument or generated above)
p0=initnoise(p0temp);
w=initnoise(varargin{4});
e=initnoise(varargin{5});

if isempty(wvars)
	% Generate a wvar vector
	wvars=generatevars('w',get(w,'n'));
elseif length(wvars)~=get(w,'n')
	error('The number of elements in ''wvars'' does not match the number of noise elements');
end

% Initialize data.
f=initdata(varargin{1},xvars,uvars,wvars,1);
h=initdata(varargin{2},xvars,uvars,wvars,1);

if isempty(uvars)
	% Generate a uvar vector
	uvars=joinvars(get(f,'uvars'),get(h,'uvars'));
end

%Initialize object data
obj.f=f;
obj.h=h;
obj.x0=x0;
obj.w=w;
obj.e=e;
obj.p0=p0;
obj.T=T;
obj.xvars=xvars;
obj.uvars=uvars;
obj.wvars=wvars;
obj.description='Discrete Non-Linear model (DNL)';
obj=class(obj,'dnl');
