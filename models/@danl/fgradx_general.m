function v=fgradx_general(obj,x,t,u,varargin)
% Calculates: grad_x[f(x,t) + gu(x,t)*u(t) + gw(x,t)*w(t)]
%
% Syntax: (* = optional)
%
% grad_x = fgradx_general(model, x, t, u, w*);
%
% In arguments:
%
% 1. model
%	Model object.
% 2. x
%	Column vector or scalar containing x
% 3. t
%	Scalar containing the time of the operation.
% 4. u
%	Column vector or scalar containing deterministic data for this particular step.
% 5* w
%	Column vector or scalar containing w(t) for this particular step.
% 5* []
%	w(t) is drawn from model.w
%
% Out arguments:
%
% 1. grad_x
%	The result of the operation: the gradient with respect to x.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Evaluate grad_x(f(x,t))
v=gradx(obj.f,x,t,[],[]);

% If u is defined, we have to differentiate gu(x,t)*u(t) too (with respect to x)
if length(u)
	gu=obj.gu;
	if ~isempty(gu)
		% ...but only if gu is defined.
		v=v+gradx(gu,x,t,[],[])*u;
	end
end;

% If w is defined, we have to differentiate gw(x,t)*u(t) too (with respect to x)
w=[];		% Default: draw noise from object
if nargin>=5 
	w=varargin{1};		% w was supplied as an argument
end;
if isempty(w)
	% No w was given as an argument. Use model.w to draw random data
	% if size(v,2)>1, we are dealing with a particle swarm and must draw a matrix
	w=random(obj.w, t, 1, size(v,2));
	v=v+gradx(obj.gw,x,t,[],[])*w;		% grad_x( g(x,t)*w )
elseif w~=0	% use it if it's non-zero!
		v=v+gradx(obj.gw,x,t,[],[])*w;	% grad_x( g(x,t)*w )
end	% if w is zero, don't do anything, the gradient is zero

