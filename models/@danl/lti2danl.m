function obj=lti2danl(ltiobj);
% Converts a LTI subclass object (ss, tf or zpk) to a DANL object
%
% Syntax: (* = optional)
%
% danlobj = lti2danl(ltiobj);
%
% In arguments:
%
% 1. ltiobj
%	LTI subclass object (ss, tf or zpk)
%
% Out arguments:
%
% 1. danlobj
%	The resulting DANL object

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%FIX: will probably not work for systems including a deterministic variable u(t)

ssobj=ss(ltiobj);
	
if ssobj.Ts==0
	error('Conversion from a continous-time model directly to DANL is not supported.');
elseif ssobj.Ts==-1
	error('LTI functions with undetermined sample time is not supported');
end
	
f=xlinear(ssobj.a,{},{},{},1);
g=initdata(ssobj.b,{},{},{},0);
h=initdata(ssobj.c,{},{},{},1);
gu=initdata([], {}, {}, {}, 0);
hu=initdata([], {}, {}, {}, 0);
T=ssobj.Ts;

w=initnoise(ones(size(ssobj.b,2),1);
e=initnoise(ssobj.d);
p0=initnoise(eye(size(ssobj.a)));
x0=zeros(length(ssobj.a));

obj.T=T;
obj.x0=x0;
obj.f=f;
obj.gw=gw;
obj.gu=gu;
obj.h=h;
obj.hu=hu;
obj.w=w;
obj.e=e;
obj.p0=p0;
obj.description='Discrete Additive Non-Linear model (DANL)';
obj=class(obj,'danl');
