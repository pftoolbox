Help on properties of the Discrete Additive Non-Linear (DANL) model.

'f'
Data object representing f(x,t).

'gw'
Data object representing gw(x,t).

'gu'
Data object representing gu(x,t).

'h'
Data object representing h(x,t).

'hu'
Data object representing hu(x,t).

'x0'
A row vector or scalar containing the initial state estimate. x0(i) represents the initial
estimate of state i.

'w'
Noise object representing the process noise w(t).

'e'
Noise object representing measurement noise e(t).

'p0'
Noise object representing the initial process noise, p0.

'T'
Sample time.

'xvars'
A cell array containing the names of the states.

'description'
A simple description of the object.

'R' (READ-ONLY)
The covariance matrix R, representing the covariance of the process noise w(t).

'Q' (READ-ONLY)
The covariance matrix Q, representing the covariance of measurement noise e(t).

'P0' (READ-ONLY)
The covariance matrix P0, representing the covariance of p0.
