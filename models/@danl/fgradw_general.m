function v=fgradw_general(obj,x,t,u,varargin)
% Calculates: grad_w[f(x,t) + gu(x,t)*u(t) + gw(x,t)*w(t)]
%
% Syntax: (* = optional)
%
% grad_w = fgradw_general(model, x, t, u, w*);
%
% In arguments:
%
% 1. model
%	Model object.
% 2. x
%	Column vector or scalar containing x
% 3. t
%	Scalar containing the time of the operation.
% 4. u
%	Column vector or scalar containing deterministic data for this particular step.
% 5* w
%	Column vector or scalar containing w(t) for this particular step.
%	Redundant argument for this model, because w(t) is elimiated by the differentiation.
%
% Out arguments:
%
% 1. grad_w
%	The result of the operation: the gradient with respect to w.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Everything except for gw(x,t) is eliminated by the differentiation.
% Hence, we just need to evaluate gw(x,t)
v=eval(obj.gw,x,t,[],[]);
