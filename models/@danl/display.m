function display(obj)
% No documentation available

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

props={'T','x0','f','gu','gw','h','hu','w','e','p0'};
propvals={num2str(obj.T),num2str(obj.x0')};
n=length(props);
for i=3:n
%	eval(strcat('propvals{i}=get(obj.',props{i},',''description'');'));
	eval(strcat('str=get(obj.',props{i},',''str'');'), strcat('str=get(obj.',props{i},',''R_str'');'));
	propvals{i}=str(1:min(length(str),80));
end;

% each property gets assigned a value in this cellarray.
% 1 means that a warning message will be printed next to the [] string, if
% the property is empty.
warnifempty={1,1,1,0,1,1,0,1,1,1};

disp(' ');
disp([inputname(1),' = '])
disp(' ');

disp('Discrete Additive Non-Linear state space model');
disp('----------------------------------------------');
disp('x(t+T) = f(x,t) + gu(x,t)*u(t) + gw(x,t)*w(t)');
disp('  y(t) = h(x,t) + e(t)');
disp(' ');

for i=1:n
	if isempty(propvals{i})
		propstr='[]';
		if warnifempty{i}
			propstr=strcat(propstr,' (Warning! Must be defined before use!)');
		end
	else
		propstr=propvals{i};
	end

	fprintf('%+2s: %s\n',props{i},propstr);
end

disp(' ');
