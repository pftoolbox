function v=hgrade_general(obj,x,t,u,varargin)
% Calculates: grad_e[h(x,t,u,e)]
%
% Syntax: (* = optional)
%
% grad_e = hgrade_general(model, x, t, u, e*);
%
% In arguments:
%
% 1. model
%	Model object.
% 2. x
%	Column vector or scalar containing x
%	Redundant argument for this model, because x is elimiated by the differentiation.
% 3. t
%	Scalar containing the time of the operation.
%	Redundant argument for this model, because t is elimiated by the differentiation.
% 4. u
%	Column vector or scalar containing deterministic data for this particular step.
%	Redundant argument for this model, because u(t) is eliminated by the differentiation.
% 5* e
%	Column vector or scalar containing e(t) for this particular step.
%	Redundant argument for this model, since this is eliminated by the differentiation.
%
% Out arguments:
%
% 1. grad_w = 1
%	The result of the operation: the gradient with respect to e.
%	grad_w always equals one for this model.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

e=[];		% Default: draw noise from object
if nargin>=5 
	e=varargin{1};		% w was supplied as an argument
end;
if isempty(e)
	% No w was given as an argument. Use model.w to draw random data
	% if size(x,2)>1, we are dealing with a particle swarm and must draw a matrix
	e=random(obj.e, t, 1, size(x,2));
else
	% We assume that w=0 and no noise is going to be drawn.
	% However, the zero matrix needs to have the same length as x,
	% because we're dealing with a particle swarm.
	e=zeros(get(obj.e,'n'), size(x,2));
end

v=gradw(obj.h,x,t,u,e);
