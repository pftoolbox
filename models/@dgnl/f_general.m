function v=f_general(obj,x,t,u,varargin)
% Calculates: f(x,t,u,w)
%
% Syntax: (* = optional)
%
% x_new = f_general(model, x, t, u, w*);
%
% In arguments:
%
% 1. model
%	Model object
% 2. x
%	Column vector or scalar containing x(t)
% 3. t
%	Scalar containing the time of the operation.
% 4. u
%	Column vector or scalar containing deterministic data for this particular step.
% 5* w
%	Column vector or scalar containing w(t) for this particular step.
% 5* []
%	w(t) is drawn from model.w
%
% Out arguments:
%
% 1. x_new
%	The result of the operation, x(t+T)

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Handle the process noise
w=[];		% Default: draw noise from object
if nargin>=5 
	w=varargin{1};		% w was supplied as an argument
end;
if isempty(w)
	% No w was given as an argument. Use model.w to draw random data
	% if size(x,2)>1, we are dealing with a particle swarm and must draw a matrix
	w=random(obj.w, t, 1, size(x,2));
%elseif size(x,2)>1
else
	% We assume that w=0 and no noise is going to be drawn.
	% However, the zero matrix needs to have the same length as x,
	% because we're dealing with a particle swarm.
	w=zeros(get(obj.w,'n'), size(x,2));
end	% If size(x,2)==1, use the supplied w value (probably 0)

% Evaluate f(x,t,u,w)
v=eval(obj.f,x,t,u,w);
