function index=findcstr(carray,str)
% Searches for a given string in a cell array of strings.
%
% Syntax: (* = optional)
%
% index = findcstr(carray,str);
%
% In arguments:
%
% 1. carray
%	A cell array of strings
% 2. str
%	The string to search for.
%
% Out arguments:
%
% 1. index
%	The element number (in the cell array) of the first occurance.
%	index=0 means that the given string was not found.
%
% Used by the get/set functions, and will become private after the final
% version is created.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

for i=1:length(carray)
	if strcmp(carray{i},str)
		index=i;
		return;
	end
end
index=0;
