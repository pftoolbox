function plot1d(obj,varargin);
% Plots the data contained in the EKF object, in one dimension.
%
% Syntax: (* = optional)
%
% plot1d(ekfobj, state*, colorscheme*);
%
% In arguments:
%
% 1. ekfobj
%	ekf object containing the data to be plotted
% 2* state
%	The state x(state) will be plotted.
% 2* []
%	'state' is set to 1, hence x(1) will be plotted..
% 3* colorscheme
%	An integer that decides what colors will be used in the figure.
%	Two colors will be picked using getcolors(colorstate). 
% 3* []
%	'colorscheme' is set to 3.
%
% Used by plot.m

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Declare the arguments
state=[];
colorscheme=[];

% Fetch arguments, if they exist
if nargin>=2; state=varargin{1}; end;
if nargin>=3; colorscheme=varargin{2}; end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(state)
	state=1;		% state default
end

if isempty(colorscheme)
	colorscheme=3; 		% colorscheme default
end

% Plot the data
[color1,color2]=getcolors(colorscheme);
xhat=obj.xhat(state,end);
P=obj.Phat(:,:,end);
plot([xhat(state)-P(state,state) xhat(state)+P(state,state)],[0 0],color1)
hold on
plot(xhat(state),0,strcat(color2,'o'))
hold off
