function [N,Nth,resampler,t]=extractparams(varargin)
% No documentation available

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Declare the arguments
N=[];
Nth=[];
resampler=[];
t=[];

% Fetch arguments, if they exist
if nargin>=2; N=varargin{2}; end;
if nargin>=3; Nth=varargin{3}; end;
if nargin>=4; resampler=varargin{4}; end;
if nargin>=5; t=varargin{5}; end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(N)
	N=1000;				% N default
end

if isempty(Nth)
	Nth=N;				% N default
end

if isempty(resampler)
	resampler=@rs_simple;		% Resampler default
end

if isempty(t)
	t=0;				% t default
end

if Nth<0
	error('Nth must be positive!');
elseif Nth>N
	error('Nth must not be greater than N');
end
