function objout=reset(obj,varargin)
% Resets the SIS particle filter.
% When this function is called, the filter forgets all previous information about
% the states and the importance weights.
%
% Syntax: (* = optional)
%
% obj* = reset(obj, t*);
%
% In arguments:
%
% 1. obj
%	The filter object to reset.
% 2* t
%	Sets the time property t to the given value.
% 2* []
%	If the object contains sample point data Ts, t will be set to Ts(1). If not, the
%	stored t property will not be altered by the reset.
%
% Out arguments:
%
% 1* obj
%	The reset filter object.
%       If no output argument is specified, the input object in the caller's workspace
%	will be modified.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if nargin>1
	obj.t=varargin{1};
elseif length(obj.Ts)
        obj.t=obj.Ts(1);
end
obj.xpred_particles=calc_x0(obj.model,obj.N);
N=obj.N;
obj.q=repmat(1/N,1,N);		% Initialize importance weights

if nargout==0
	% No output variable was specified by the user.
	% We'll do the assignment manually in the caller's workspace.
	objname = inputname(1);
	assignin('caller',objname,obj)
else
	objout=obj;
end;
