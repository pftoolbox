function plot1d(obj,varargin);
% Plots the data contained in the SIS object, in one dimension.
% The particles are visualized as gauss functions.
%
% Syntax: (* = optional)
%
% plot1d(ekfobj, state*, colorscheme*, sigma*, gridpoints*);
%
% In arguments:
%
% 1. ekfobj
%	ekf object containing the data to be plotted
% 2* state
%	The state x(state) will be plotted.
% 2* []
%	'state' is set to 1, hence x(1) will be plotted.
% 3* colorscheme
%	An integer that decides what colors will be used in the figure.
%	Two colors will be picked using getcolors(colorstate) 
% 3* []
%	'colorscheme' is set to 2.
% 4* sigma
%	Scaling parameter of the gauss function.
% 4* []
%	'sigma' is set to 10.
% 5* gridpoints
%	Horizontal resolution of the gauss function.
% 5* []
%	'gridpoints' is set to 100.
%
% Used by plot.m

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Declare the arguments
state=[];
colorscheme=[];
sigma=[];
gridpoints=[];

% Fetch arguments, if they exist
if nargin>=2; state=varargin{1}; end;
if nargin>=3; colorscheme=varargin{2}; end;
if nargin>=4; sigma=varargin{3}; end;
if nargin>=5; gridpoints=varargin{4}; end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(state)
	state=1;		% state default
end

if isempty(colorscheme)
	colorscheme=2; 		% colorscheme default
end

if isempty(sigma)
	sigma=10;		% sigma default
end

if isempty(gridpoints)
	gridpoints=100;		% gridpoints default
end

% Plot the data
color1=getcolors(colorscheme);
x=obj.xhat_particles(state,:);
xmin=min(x);
xmax=max(x);

% This improvised algorithm for window sizing seems to work ok.
xdiff=xmax-xmin;
xmin=xmin-sigma*xdiff;
xmax=xmax+sigma*xdiff;

xg=xmin:(xmax-xmin)/gridpoints:xmax;
ng=length(xg);
m=exp(-(repmat(x',1,ng)-repmat(xg,obj.N,1)).^2)/(2*sigma^2);
m=sum(m,1);
plot(xg,m,color1);
