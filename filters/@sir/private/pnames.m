function [props,rprops]=pnames
% The properties are divided into two groups:
% General properties (or just 'properties') that can be both read and written, and
% read-only properties that can be read but not written.
%
% props: a cell array containing the names of the general properties
% rprops: a cell array containing the names of the read-only properties

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

props={'model','t','Ts','xhat','xpred','u','y','N','resampler','xpred_particles','xhat_particles','historysize','xhat_history','xpred_history','u_history','y_history','Ts_history','description'};
rprops={};
