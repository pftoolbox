function plot2d(obj,varargin);
% Plots the data contained in the object, in two dimensions.
%
% Syntax: (* = optional)
%
% plot2d(sirobj, states*, colorscheme*, N*);
%
% In arguments:
%
% 1. sirobj
%	sir object containing the data to be plotted
% 2* states
%	A pair of integers [sx, sy].
%	The x and y axes will represent x(sx) and x(sy) respectively.
% 2* []
%	'states' is set to [1 2]
% 3* colorscheme
%	An integer that decides what colors will be used in the figure.
%	Two colors will be picked using getcolors(colorstate) 
% 3* []
%	'colorscheme' is set to 2
% 4* N
%	Number of particles visualized in the plot.
% 4* []
%	All particles will be plotted.
%
% Used by plot.m

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Declare the arguments
states=[];
colorscheme=[];
N=[];

% Fetch arguments, if they exist
if nargin>=2; states=varargin{1}; end;
if nargin>=3; colorscheme=varargin{2}; end;
if nargin>=4; N=varargin{3}; end;

% If an empty argument, or no argument at all, was supplied - use its default value!
if isempty(states)
	states=[1 2];		% states default
end

if isempty(colorscheme)
	colorscheme=2; 		% states default
end

if isempty(N)
	%N=min(100,obj.N);	
	N=obj.N;		% N default
end

% Plot the data           
[color1,color2]=getcolors(colorscheme);
xhat=obj.xhat(:,end);
x=obj.xhat_particles;
plot(x(states(1),1:N),x(states(2),1:N),strcat(color1,'.'));
hold on
plot(xhat(states(1)),xhat(states(2)),strcat(color2,'o'))
hold off
