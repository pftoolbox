function model_edit_wvars(action,varargin);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% replace 'wvars', 'w'

myfont='default';

if strcmp(action,'init')
	callback=varargin{2};
	parentfig=varargin{1};
	parenthandles=guidata(parentfig);
	
	wvars=parenthandles.wvars;

	hf=figure('Name','w(t) component editor');

	uicontrol('Style','Pushbutton','String','Save','Units','Normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['model_edit_wvars(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','Normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

	uicontrol(hf,'Style','Text','String','Element','Units','normalized','Position',[0.0 0.9 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Text','String','Name','Units','normalized','Position',[0.2 0.9 0.3 0.05],'HorizontalAlign','Left','Fontname',myfont);

	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.88 1 0.003]);


	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.1 1 0.003]);
	uicontrol(hf,'Style','Text','String','Elements:','Units','normalized','Position',[0.0 0.02 0.20 0.05],'HorizontalAlign','Left','Fontname',myfont);
	handles=guihandles(hf);
	handles.edit_statenumber=uicontrol(hf,'Style','Edit','String',num2str(length(wvars)),'Units','normalized','Position',[0.2 0.02 0.1 0.05],'callback',['model_edit_wvars(''changenumber'',' ,num2str(hf), ')'],'Fontname',myfont);

	handles.wvars=wvars;
	handles.states=length(wvars);
	handles.parentfig=parentfig;
	handles.callback=callback;
	guidata(hf,handles);	

	model_edit_wvars('update',hf);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	wvars=cell(1,handles.states);
	
	for i=1:handles.states
		name_str=get(handles.(['name_',num2str(i)]),'String');
		
		if isempty(name_str)
			msgbox('Error! A field is empty');
			return;
		end
		
		wvars{i}=name_str;
	end
	
	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.wvars=wvars;

	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

elseif strcmp(action,'changenumber')


	hf=varargin{1};
	handles=guidata(hf);	
	states=str2num(get(handles.edit_statenumber,'String'));
	states_old=length(handles.wvars);
	sdiff=states-states_old;

	wvars=handles.wvars;

	if sdiff>0
		% Add more states
		for i=states_old+1:states
			wvars{i}=['w',num2str(i)];
		end;
	elseif sdiff<0
		% Delete some states
		wvars=wvars(1:states);
	else
		% Nothing has changed. Do nothing.
		return;
	end
	
	handles.wvars=wvars;
	handles.states=states;
	guidata(hf,handles);


	model_edit_wvars('update',hf);

elseif strcmp(action,'update')

	hf=varargin{1};
	handles=guidata(hf);	

	wvars=handles.wvars;

	% Remove old controls
	i=1;
	while isfield(handles,['number_',num2str(i)])
		delete(handles.(['number_',num2str(i)]));
		delete(handles.(['name_',num2str(i)]));
		handles=rmfield(handles,['number_',num2str(i)]);
		handles=rmfield(handles,['name_',num2str(i)]);
		i=i+1;
	end;	

	postop=0.8;
	space=0.08;
	pos=0;

	% Add new controls
	for i=1:length(wvars)
		str=wvars{i};

		handles.(['number_',num2str(i)])=uicontrol(hf,'Style','Text','String',num2str(i),'Units','normalized','Position',[0.0 postop-space*pos 0.1 0.05],'Fontname',myfont);
		handles.(['name_',num2str(i)])=uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.2 postop-space*pos 0.6 0.05],'Fontname',myfont);
		pos=pos+1;
	end

	guidata(hf,handles);
end
