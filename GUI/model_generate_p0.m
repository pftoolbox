function danl_generate_p0(hObject);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hObject);

if get(handles.check_p0,'Value')
	p0=gauss(eye(handles.states));
	set(handles.p0_select,'value',2);	% 2=gauss. Make dynamic!
	set(handles.p0_edit,'String',get(p0,'R_str'));	% 2=gauss. Make dynamic!
	handles.p0=p0;
end

guidata(hObject,handles);
