function danl_edit(obj, hObject, varargin)
% NOTE: Model edit windows do not use the "action" input argument,
% as all other edit windows do.
% This will be fixed as soon as possible. In the meantime, just exclude the first input
% argument when programming own model edit windows.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Sorry, I didn't have enough time to write nice GUI code.

%myfont='FixedWidth';
myfont='default';

%hf=figure('Name','DANL Edit','NumberTitle','off','Units','Centimeters','Position',[1 3 15 10],'Units','Normalized','PaperUnits','Centimeters','PaperPosition',[0.25 3 18 12]);
hf=figure('Name','DANL Edit');

datatypes=pfgui_config('expression_types');

object_str='Undefined';
object_str2='Undefined (Optional)';
for i=1:length(datatypes)
	object_str=[object_str,'|',eval(['get(',datatypes{i},',''description'')'])];
	object_str2=[object_str2,'|',eval(['get(',datatypes{i},',''description'')'])];
end;

dnames={'f','gw','gu','h','hu'};
postop=0.45;
space=0.06;

pos=0;
for i=1:length(dnames);
	currobj=dnames{i};

	if ~isempty(obj.(currobj));
		str=get(obj.(currobj),'str');
	else;
		str=[];
	end;

	index=0;
	for j=1:length(datatypes)
		if isa(obj.(currobj),datatypes{j})
			index=j;
			break;
		end;
	end;

	if strcmp(currobj,'gu')||strcmp(currobj,'hu')
		popup_str=object_str2;		% Optional objects
	else
		popup_str=object_str;
	end

	uicontrol(hf,'Style','Text','String',currobj,'Units','normalized','Position',[0.0 postop-space*pos 0.055 0.05],'HorizontalAlign','Left','TooltipString','The actual object','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.04 postop-space*pos 0.38 0.05],'Tag',[currobj,'_edit'],'callback',['danl_action(',num2str(hf),',''edit_data'',''',currobj,''')'],'TooltipString','A string representation of the object','Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','E','Units','normalized','Position',[0.425 postop-space*pos 0.03 0.05],'callback',['danl_action(',num2str(hf),',''objedit'',''',currobj,''')'],'Tag',[currobj,'_objedit'],'TooltipString','Edit the object','Fontname',myfont);
	uicontrol(hf,'Style','Popup','String',popup_str,'Value',index+1,'Units','normalized','Position',[0.51 postop-space*pos 0.49 0.05],'callback',['danl_action(',num2str(hf),',''select'',''',currobj,''')'],'Tag',[currobj,'_select'],'TooltipString','Object type','Fontname',myfont);

	pos=pos+1;
end;

%-------------- rnd stuff

rndtypes=pfgui_config('noise_types');
rndselect_str='Undefined';
for i=1:length(rndtypes)
	rndselect_str=[rndselect_str,'|',eval(['get(',rndtypes{i},',''description'')'])];
end;

rnames={'w','e','p0'};

postop=0.13;
space=0.06;

pos=0;
for i=1:length(rnames);
	currobj=rnames{i};

	if ~isempty(obj.(currobj));
		str=get(obj.(currobj),'R_str');
	else;
		str=[];
	end;

	index=0;
	for j=1:length(rndtypes)
		if isa(obj.(currobj),rndtypes{j})
			index=j;
			break;
		end;
	end;

	uicontrol(hf,'Style','Text','String',currobj,'Units','normalized','Position',[0.0 postop-space*pos 0.055 0.05],'HorizontalAlign','Left','TooltipString','The actual object','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.04 postop-space*pos 0.38 0.05],'Tag',[currobj,'_edit'],'callback',['danl_action(',num2str(hf),',''rnd_edit'',''',currobj,''')'],'TooltipString','A string representation of the object','Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','E','Units','normalized','Position',[0.425 postop-space*pos 0.03 0.05],'callback',['danl_action(',num2str(hf),',''rnd_objedit'',''',currobj,''')'],'Tag',[currobj,'_objedit'],'TooltipString','Edit the object','Fontname',myfont);
	uicontrol(hf,'Style','Popup','String',rndselect_str,'Value',index+1,'Units','normalized','Position',[0.51 postop-space*pos 0.49 0.05],'callback',['danl_action(',num2str(hf),',''rnd_select'',''',currobj,''')'],'Tag',[currobj,'_select'],'TooltipString','Object type','Fontname',myfont);

	pos=pos+1;
end;

T=obj.T;
xvars=obj.xvars;
x0=obj.x0;
states=length(x0);

% Description frame
uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.845-0.04 0.7 0.125+0.04]);
uicontrol(hf,'Style','Text','String','x(t+T) = f(x,t) + gu(x,t)*u(t) + gw(x,t)*w(t)','Units','normalized','Position',[0.01 0.9 0.65 0.04],'HorizontalAlign','Left','FontName','FixedWidth');
uicontrol(hf,'Style','Text','String','  y(t) = h(x,t) + hu(x,t)*u(t) + e(t)','Units','normalized','Position',[0.01 0.86 0.65 0.04],'HorizontalAlign','Left','FontName','FixedWidth');
uicontrol(hf,'Style','Text','String','  x(0) = x0 + p0','Units','normalized','Position',[0.01 0.82 0.65 0.04],'HorizontalAlign','Left','FontName','FixedWidth');
uicontrol(hf,'Style','Text','String','Discrete Additive Non-Linear Model','Units','normalized','Position',[0.01 0.95 0.5 0.04],'FontName',myfont);

% Main controls
uicontrol(hf,'Style','Text','String','Sample time (T):','Units','normalized','Position',[0.51 0.71 0.39 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',num2str(T),'Units','normalized','Position',[0.9 0.71 0.1 0.05],'callback',['danl_action(',num2str(hf),',''T_edit'')'],'Tag','T_edit','Fontname',myfont);
%uicontrol(hf,'Style','Text','String','seconds','Units','normalized','Position',[0.85 0.71 0.1 0.05],'Fontname',myfont);


o1x=-0.535;
o1y=-0.02-0.18-0.005;

%o1x=-0.0;
%o1y=-0.02-0.14;


uicontrol(hf,'Style','Frame','Units','normalized','Position',[o1x+0.535 o1y+0.735 0.455 0.255],'callback',['danl_action(',num2str(hf),',''states_edit'')']);

uicontrol(hf,'Style','Text','String','Names (xvars):','Units','normalized','Position',[o1x+0.54 o1y+0.855 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',cell2cellstr(xvars),'Units','normalized','Position',[o1x+0.75 o1y+0.855 0.2 0.05],'Tag','edit_xvars','callback',['danl_action(',num2str(hf),',''xvars_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Text','String','x0 vector:','Units','normalized','Position',[o1x+0.54 o1y+0.80 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',num2str2(x0),'Units','normalized','Position',[o1x+0.75 o1y+0.80 0.2 0.05],'Tag','edit_x0','callback',['danl_action(',num2str(hf),',''x0_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Text','String','Generate p0 noise','Units','normalized','Position',[o1x+0.54 o1y+0.74 0.3 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Checkbox','Units','normalized','Position',[o1x+0.93 o1y+0.74 0.03 0.05],'Tag','check_p0','Value',1,'Fontname',myfont);
uicontrol(hf,'Style','Text','String','States:','Units','normalized','Position',[o1x+0.54 o1y+0.91 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',num2str(states),'Units','normalized','Position',[o1x+0.75 o1y+0.91 0.095 0.05],'Tag','edit_states','callback',['danl_action(',num2str(hf),',''statenr_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Pushbutton','String','Edit','Units','normalized','Position',[o1x+0.85 o1y+0.91 0.1 0.05],'callback',['danl_action(',num2str(hf),',''states_edit'')'],'Fontname',myfont);



% Save / Cancel
blah=-0.03;
uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.95+blah,0.1,0.05],'Callback',['danl_action(',num2str(hf),',''save'')'],'Fontname',myfont);
uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.95+blah,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

% Importer
uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.51 0.1+0.48 0.49 0.09]);
uicontrol(hf,'Style','Popup','String','f|gu|gw|h|hu|w|e|p0','Value',1,'Units','normalized','Position',[0.52 0.1+0.5 0.1 0.05],'Tag','import_select','TooltipString','Object to import from the workspace','FontName',myfont);
uicontrol(hf,'Style','Edit','String','','Units','normalized','Position',[0.63 0.1+0.5 0.25 0.05],'callback',['danl_action(',num2str(hf),',''import_button'')'],'Tag','import_edit','Fontname',myfont);
uicontrol(hf,'Style','Pushbutton','String','Import','Units','normalized','Position',[0.89 0.1+0.5 0.1 0.05],'callback',['danl_action(',num2str(hf),',''import_button'')'],'Fontname',myfont);


handles=guihandles(hf);
handles.parentfig=hObject;
handles.caller=varargin{1};
handles.callback=varargin{2};
handles.datatypes=datatypes;
handles.rndtypes=rndtypes;
handles.dnames=dnames;
handles.obj=obj;
handles.f=obj.f;
handles.gu=obj.gu;
handles.gw=obj.gw;
handles.h=obj.h;
handles.hu=obj.hu;
handles.w=obj.w;
handles.e=obj.e;
handles.p0=obj.p0;
handles.x0=x0;
handles.xvars=xvars;
handles.T=T;
handles.states=states;
guidata(hf,handles);

for i=1:length(dnames)
	danl_update_checkbox(hf,i);
end;
