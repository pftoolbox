function sis_edit(action, varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';
%myfont='FixedWidth';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	caller=varargin{3};
	callback=varargin{4};

	resample_functions={@rs_regularized, @rs_simple, @rs_stratified, @rs_systematic};
	resample_names={'Residual resampling', 'Simple resampling', 'Stratified resampling', 'Systematic resampling'};
	resample_str=cell2str(resample_names);

	resampler_str=func2str(obj.resampler);

	resample_value=2;
	for i=1:length(resample_functions)
		if strcmp(resampler_str, func2str(resample_functions{i}))
			resample_value=i;
			break;
		end;
	end;

	N=obj.N;
	Nth=obj.Nth;

%	hf=figure('Name','sis object editor','NumberTitle','off','Units','normalized','Position',[0.3 0.5 0.5 0.4]);
	hf=figure('Name','sis object editor');

p=0.6;

	uicontrol(hf,'Style','Text','String','Number of particles (N):','Units','normalized','Position',[0 p+0.15 0.4 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',num2str(N),'Units','normalized','Position',[0.45 p+0.15 0.18 0.05],'Callback',['sis_edit(''N_edit'',',num2str(hf),')'],'Tag','N_edit','Fontname',myfont);

	uicontrol(hf,'Style','Text','String','Resampling function:','Units','normalized','Position',[0.0 p+0.07 0.4 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Popup','String',resample_str,'Value',resample_value,'Units','normalized','Position',[0.45 p+0.07 0.4 0.05],'Tag','resampler_select','Fontname',myfont);

%	uicontrol(hf,'Style','Frame','Units','Characters','Position',[55 0 0.1 5]);

	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.65,p+0.15,0.1,0.05],'Callback',['sis_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.75,p+0.15,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

	uicontrol(hf,'Style','Text','String','Resampling threshold (Nth):','Units','normalized','Position',[0 p+(-0.01) 0.4 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',num2str(Nth),'Units','normalized','Position',[0.45 p+(-0.01) 0.18 0.05],'Callback',['sis_edit(''Nth_edit'',',num2str(hf),')'],'Tag','Nth_edit','Fontname',myfont);
	uicontrol(hf,'Style','Text','String','0 <= Nth <= N','Units','normalized','Position',[0.65 p+(-0.01) 0.2 0.05],'HorizontalAlign','center','Fontname',myfont);


	handles=guihandles(hf);
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	handles.N=N;
	handles.Nth=Nth;
	handles.resample_functions=resample_functions;
	guidata(hf,handles);

elseif strcmp(action,'save');

	hf=varargin{1};
	handles=guidata(hf);

	caller=handles.caller;
	obj=handles.obj;

	resampler_index=get(handles.resampler_select,'value');

	set(obj,'N',handles.N);
	set(obj,'resampler',handles.resample_functions{resampler_index});
	set(obj,'Nth',handles.Nth);

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.(caller)=obj;
	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

elseif strcmp(action,'N_edit');
	hf=varargin{1};
	handles=guidata(hf);

	N_str=get(handles.N_edit,'String');
	if isempty(N_str)
		msgbox('Please enter a value');
	end;
	N=str2num(N_str);
	if N<1
		msgbox('Please enter a POSITIVE value');
		return
	elseif round(N)~=N
		msgbox('Please enter a positive INTEGER');
		return
	end	

	if N<handles.Nth
		handles.Nth=N;
		set(handles.Nth_edit,'String',num2str(N));
	end

	handles.N=N;
	guidata(hf,handles);

elseif strcmp(action,'Nth_edit');
	hf=varargin{1};
	handles=guidata(hf);

	Nth_str=get(handles.Nth_edit,'String');
	if isempty(Nth_str)
		msgbox('Please enter a value');
	end;
	Nth=str2num(Nth_str);
	if Nth<0
		msgbox('Please enter a POSITIVE value');
		return
	elseif Nth>handles.N
		msgbox('Nth must not be greater than N!');
		return
	end	
	handles.Nth=Nth;
	set(handles.Nth_edit,'String',num2str(Nth));	% Format!
	guidata(hf,handles);
end
