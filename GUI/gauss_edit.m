function gauss_edit(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	caller=varargin{3};
	callback=varargin{4};

	A=obj.R;
%	hf=figure('Name','gauss object editor','NumberTitle','off','Units','Characters');
	hf=figure('Name','gauss object editor');
	n=length(A);
	m=n;	%laziness
	uicontrol('Style','Text','String','Size:','Units','normalized','Position',[0,0.02,0.1,0.05],'Fontname',myfont);
	uicontrol('Style','Edit','String',num2str(n),'Units','normalized','Tag','matrixsize','Position',[0.12,0.02,0.1,0.05],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Resize','Units','normalized','Position',[0.3,0.02,0.2,0.05],'Callback',['gauss_edit(''resize'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['gauss_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Frame','Units','normalized','Position',[0,0.1,1,0.003]);

	% Generate the edit matrix
	for j=1:m
		for i=1:n
			uicontrol('Style','Edit','String',num2str(A(j,i)),'Units','normalized','Tag',['edit',num2str(j),'_',num2str(i)],'Position',[0.1*(i-1),0.1*(m-j)+0.13,0.08,0.05],'callback',['gauss_edit(''edit'',', num2str(hf), ',', num2str(j), ',', num2str(i), ')'],'Fontname',myfont);
		end
	end

	handles=guihandles(hf);
	handles.matrix=A;
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	guidata(hf,handles);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);


	caller=handles.caller;
	obj=handles.obj;
	set(obj,'R',handles.matrix);

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.(caller)=obj;
	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

elseif strcmp(action,'edit')

	hf=varargin{1};
	handles=guidata(hf);
	A=handles.matrix;

	j=varargin{2};
	i=varargin{3};

	source=['edit',num2str(j),'_',num2str(i)'];
	str=get(handles.(source),'String');

	num=str2num(str);		% WANTED: Error checking
	str=num2str(num);		% Format the data
	
	A(j,i)=num;

	set(handles.(source),'String',str);
	if i~=j
		dest=['edit',num2str(i),'_',num2str(j)'];
		set(handles.(dest),'String',str);
		A(i,j)=num;
	end;

	handles.matrix=A;
	guidata(hf,handles);

elseif strcmp(action,'resize')
	hf=varargin{1};
	handles=guidata(hf);

	A=handles.matrix;
	m=size(A,1);
	n=size(A,2);

	% Delete edit controls
	for j=1:m
		for i=1:n
			delete(handles.(['edit',num2str(j),'_',num2str(i)]));
		end
	end

	% Generate new values
	n=str2num(get(handles.matrixsize,'string'));
	m=n;	% l a z i n e s s

	A=join(A,eye(n));

	% Generate the edit matrix
	for j=1:m
		for i=1:n
			uicontrol('Style','Edit','String',num2str(A(j,i)),'Units','normalized','Tag',['edit',num2str(j),'_',num2str(i)],'Position',[0.1*(i-1),0.1*(m-j)+0.13,0.08,0.05],'callback',['gauss_edit(''edit'',', num2str(hf), ',', num2str(j), ',', num2str(i), ')'],'Fontname',myfont);
		end
	end

	% Save stuff from the old handles
	obj=handles.obj;
	parentfig=handles.parentfig;
	caller=handles.caller;
	callback=handles.callback;

	% Generate new handles structure
	handles=guihandles(hf);

	% Put back the saved data
	handles.obj=obj;
	handles.parentfig=parentfig;
	handles.caller=caller;
	handles.callback=callback;

	handles.matrix=A;
	guidata(hf,handles);
end;
