function dnl_action_noise_update(hObject,caller)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hObject);

if strcmp(caller,'w')
	% Generate wvars
	wvars=handles.wvars;
	states=get(handles.w,'n');
	states_old=length(wvars);
	sdiff=states-states_old;
	if sdiff>0
		for i=states_old+1:states
			wvars{i}=['w',num2str(i)];
		end;
	elseif sdiff<0
		wvars=wvars(1:states);
	end
	handles.wvars=wvars;
	set(handles.wvars_edit,'String',cell2cellstr(wvars));
	guidata(hObject,handles);
elseif strcmp(caller,'e')
	eval('evars=handles.evars;','evars=[];');
	if ~isa(evars,'double')
		% Generate evars
		evars=handles.evars;
		states=get(handles.e,'n');
		states_old=length(evars);
		sdiff=states-states_old;
		if sdiff>0
			for i=states_old+1:states
				evars{i}=['e',num2str(i)];
			end;
		elseif sdiff<0
			evars=evars(1:states);
		end
		handles.evars=evars;
		set(handles.evars_edit,'String',cell2cellstr(evars));
		guidata(hObject,handles);
	end
end
