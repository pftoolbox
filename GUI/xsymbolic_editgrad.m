function xsymbolic_editgrad(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='FixedWidth';
%myfont='default';

if strcmp(action,'init')

	parentfig=varargin{1};
	grad=varargin{2};
	caller=varargin{3};
	callback=varargin{4};

	hf=figure('Name','xsymbolic gradient editor (primitive version)');
	
	grad_str=char(grad);

	textstr1=sprintf('Here follows the INTERNAL gradient expression with respect to %s.',caller(5));
	textstr2='x(i,:) represents state i and w(i,:) represents noise element i (the same for both w(t) and e(t), since only one of them can exist in an expression at one time). Finally, u(i,:) represents u(t) element i.';
	uicontrol('Style','Text','String',textstr1,'Units','normalized','Position',[0 0.92 1 0.05],'Min',1,'Max',3,'HorizontalAlign','Left','Fontname',myfont);	
	uicontrol('Style','Text','String',textstr2,'Units','normalized','Position',[0 0.75 1 0.15],'Min',1,'Max',3,'HorizontalAlign','Left','Fontname',myfont);	
	
	uicontrol('Style','Edit','String',grad_str,'Units','normalized','Position',[0 0.22 1 0.5],'Tag','grad_edit','Min',1,'Max',3,'HorizontalAlign','Left','Fontname','FixedWidth');	

	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.15,0.1,0.05],'Callback',['xsymbolic_editgrad(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.15,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

	
	handles=guihandles(hf);
	handles.parentfig=parentfig;
	handles.caller=caller;
	handles.callback=callback;
	handles.grad=grad;
	guidata(hf,handles);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	grad_str=get(handles.grad_edit,'String');

	caller=handles.caller;

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.obj=set(parenthandles.obj,caller,grad_str);
	guidata(parentfig,parenthandles);	% Save data
	
%	eval(handles.callback);
	close(hf)

end