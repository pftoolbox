function dnl_action(hObject,action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hObject);

if strcmp(action,'check')
	caller=varargin{1};
	disp(caller);
end;

if strcmp(action,'edit_data')
	caller=varargin{1};
	str=get(handles.([caller, '_edit']),'String');

	if ~isempty(str)
		% Undefined object! We'll try to assign the expression to the correct class
		if isa(handles.(caller), 'double')
			% The object is undefined!
			tc=eval('str2num(str)',[]);
			if isempty(tc)
				handles.(caller)=xsymbolic;
			else
				handles.(caller)=xlinear;
				if strcmp(caller,'f')||strcmp(caller,'h')
					handles.(caller)=set(handles.(caller),'evalvar',1);
				end
			end
			guidata(hObject,handles);
			model_action_update_select(hObject,caller,'datatypes','str');
			handles=guidata(hObject);
		end
	end

	handles.(caller)=set(handles.(caller),'str',str);
	str=get(handles.(caller),'str');
	set(handles.([caller, '_edit']),'String',str);			% FORMAT DATA

end;

if strcmp(action,'save')

	% Check for empty objects
	needed_objects={'f','h','w','e','p0'};
	for i=1:length(needed_objects);	
		if isempty(handles.(needed_objects{i}))
			msgbox([needed_objects{i},' must not be undefined!']);
			return;
		end
	end

	% Assign the saved properties to local variables
	obj=handles.obj;
	props={'f','h','w','e','p0','xvars','uvars','wvars','x0','T'};
	for i=1:length(props)
		eval([props{i},'=handles.',props{i},';']);
	end

	% Insert xvars into the data objects
	data_objects={'f','h'};
	for i=1:length(data_objects)
		eval(['set(obj, ''', props{i}, ''', ', props{i}, ');']);
		eval(['set(', data_objects{i}, ', ''xvars'', xvars);']);
		eval(['set(', data_objects{i}, ', ''uvars'', uvars);']);
		eval(['set(', data_objects{i}, ', ''wvars'', wvars);']);
	end

	% Fix the correct varsize
% F�R PRESENTATION!!
%	varsize=[handles.states, 1, parenthandles.usize, get(handles.w,'n')];
	varsize=[handles.states, 1, 0, get(handles.w,'n')];

	eval(['set(f, ''varsize'', varsize);'], ';');	% Try / Catch
	eval(['set(h, ''varsize'', varsize);'], ';');	% Try / Catch

	% Insert the new objects into the model
	for i=1:length(props)
		eval(['set(obj, ''', props{i}, ''', ', props{i}, ');']);
	end

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);
	model_objects=parenthandles.model_objects;
	model_objects{handles.caller}=obj;
	parenthandles.model_objects=model_objects;
	guidata(parentfig,parenthandles);

	eval(handles.callback);

	close(hObject)
	return
end;


if strcmp(action,'rnd_edit')

	caller=varargin{1};
	rnd_edit_property=[caller, '_edit'];
	str=get(handles.(rnd_edit_property),'String');

	if ~isempty(str)
		% Undefined object! We'll try to assign the expression to the correct class
		if isa(handles.(caller), 'double')
			% The object is undefined!
			handles.(caller)=gauss;
			guidata(hObject,handles);
			model_action_update_select(hObject,caller,'rndtypes','R_str');
			handles=guidata(hObject);
		end
	end

	handles.(caller)=set(handles.(caller),'R_str',str);
	str=get(handles.(caller),'R_str');
	set(handles.(rnd_edit_property),'String',str);
	
	guidata(hObject,handles);
	dnl_action_noise_update(hObject,caller);
	handles=guidata(hObject);	% UGLY FIX LAST ROW BLAH BLAH

end;


if strcmp(action,'states_edit')
	handles.x0=str2num(get(handles.edit_x0,'String'));
	handles.states=str2num(get(handles.edit_states,'String'));
	handles.xvars=cellstr2cell(get(handles.xvars_edit,'String'));
	guidata(hObject,handles);

	model_edit_states('init', hObject, ['dnl_action(',num2str(hObject),',''states_refresh'')']);
end;

if strcmp(action,'states_refresh')

	set(handles.edit_states,'String',num2str(handles.states));
	set(handles.edit_x0,'String',num2str2(handles.x0));
	set(handles.xvars_edit,'String',cell2cellstr(handles.xvars));

	guidata(hObject,handles);
	model_generate_p0(hObject);
	handles=guidata(hObject);
end;

if strcmp(action,'uvars_edit_button')
	handles.uvars=cellstr2cell(get(handles.uvars_edit,'String'));
	guidata(hObject,handles);

	model_edit_uvars('init', hObject, ['dnl_action(',num2str(hObject),',''uvars_refresh'')']);
end;

if strcmp(action,'uvars_refresh')

	set(handles.uvars_edit,'String',cell2cellstr(handles.uvars));
	guidata(hObject,handles);
end;


if strcmp(action,'wvars_edit_button')
	handles.wvars=cellstr2cell(get(handles.wvars_edit,'String'));
	guidata(hObject,handles);

	model_edit_wvars('init', hObject, ['dnl_action(',num2str(hObject),',''wvars_refresh'')']);
end;

if strcmp(action,'wvars_refresh')

	set(handles.wvars_edit,'String',cell2cellstr(handles.wvars));
	guidata(hObject,handles);
end;


if strcmp(action,'T_edit')
	T=str2num(get(handles.T_edit,'String'));
	if T<=0
		msgbox('T must be positive!');
		return
	end
	handles.T=T;
	set(handles.T_edit,'String',num2str(T));	% Format data
end;

if strcmp(action,'statenr_edit')
	states=str2num(get(handles.edit_states,'String'));
	if states<0
		msgbox('The amount of states must be positive!');
		return
	end

%	x0=str2num(get(handles.x0,'String'));
%	xvars=cellstr2cell(get(handles.xvars,'String'));

	x0=handles.x0;
	xvars=handles.xvars;

	states_old=length(xvars);
	sdiff=states-states_old;

	if sdiff>0
		% Add more states
		x0(end+1:states)=0;
		for i=states_old+1:states
			xvars{i}=['x',num2str(i)];
		end;
	elseif sdiff<0
		% Delete some states
		x0=x0(1:states);
		xvars=xvars(1:states);
	else
		% Nothing has changed. Do nothing.
%		return;
	end

	if size(x0,2)>1
		% A row vector! Let's transpone it.
		x0=x0';
	end;

	set(handles.xvars_edit,'String',cell2cellstr(xvars));
	set(handles.edit_x0,'String',num2str2(x0));

	handles.states=states;
	handles.x0=x0;
	handles.xvars=xvars;

	guidata(hObject,handles);
	model_generate_p0(hObject);
	handles=guidata(hObject);
end;

if strcmp(action,'xvars_edit')
	xvars=cellstr2cell(get(handles.xvars_edit,'String'));
	if length(xvars)~=handles.states
		msgbox('Error! The amount of elements in ''xvars'' must be equal to the amount of states.');
		return;
	end;
	handles.xvars=xvars;

	set(handles.xvars_edit,'String',cell2cellstr(xvars));	% Format data
end;

if strcmp(action,'uvars_edit')
	uvars=cellstr2cell(get(handles.uvars_edit,'String'));
	handles.uvars=uvars;
	set(handles.uvars_edit,'String',cell2cellstr(uvars));	% Format data
end;

if strcmp(action,'wvars_edit')
	wvars=cellstr2cell(get(handles.wvars_edit,'String'));
	handles.wvars=wvars;
	set(handles.wvars_edit,'String',cell2cellstr(wvars));	% Format data
end;

if strcmp(action,'x0_edit')
	x0=str2num(get(handles.edit_x0,'String'));

	if min(size(x0))>1
		msgbox('''x0'' must be one-dimensional!');
		return;
	end;

	if length(x0)~=handles.states
		msgbox('Error! The amount of elements in ''x0'' must be equal to the amount of states.');
		return;
	end;

	if size(x0,2)>1
		% A row vector! Let's transpone it.
		x0=x0';
	end;

	handles.x0=x0;

	set(handles.edit_x0,'String',num2str2(x0));	% Format data
end;

if strcmp(action,'objedit')
	caller=varargin{1};
	dobj=handles.(caller);
	if isa(dobj,'double')
		msgbox('Error! Can not edit an undefined object.');
	else
		function_str=[class(dobj),'_edit'];	
		feval(function_str,'init',hObject,dobj,caller,['dnl_action(',num2str(hObject),',''obj_refresh'',''',caller,''')']);
	end
end;

if strcmp(action,'rnd_objedit')
	caller=varargin{1};
	robj=handles.(caller);
	if isa(robj,'double')
		msgbox('Error! Can not edit an undefined object.');
	else
		function_str=[class(robj),'_edit'];	
		feval(function_str,'init',hObject,robj,caller,['dnl_action(',num2str(hObject),',''rnd_refresh'',''',caller,''')']);
	end
end;


if strcmp(action,'obj_refresh')
	caller=varargin{1};
	str=get(handles.(caller),'str');
	rnd_edit_property=[caller,'_edit'];
	set(handles.(rnd_edit_property),'String',str);

	obj=handles.(caller);
end;

if strcmp(action,'rnd_refresh')
	caller=varargin{1};
	str=get(handles.(caller),'R_str');
	rnd_edit_property=[caller,'_edit'];
	set(handles.(rnd_edit_property),'String',str);
	guidata(hObject,handles);
	dnl_action_noise_update(hObject,caller);
	handles=guidata(hObject);	% UGLY FIX LAST ROW BLAH BLAH
end;


if strcmp(action,'select')

	caller=varargin{1};
	select_property=[caller, '_select'];
	edit_property=[caller, '_edit'];
	
	datatypes=handles.datatypes;
	index=get(handles.(select_property),'value')-1;
%	set(handles.(edit_property),'String','');

	if index>0
		handles.(caller)=eval(datatypes{index});
		% Default f and h xlinear objects have evalvar=1
		if isa(handles.(caller),'xlinear');
			if findcstr({'f','h'},caller)
				obj=handles.(caller);
				set(obj,'evalvar',1);
				handles.(caller)=obj;
			end
		end
	else
		handles.(caller)=[];
	end;
	set(handles.(edit_property),'String',get(handles.(caller),'str'));

	guidata(hObject,handles);
end;

if strcmp(action,'rnd_select')
	caller=varargin{1};
	select_property=[caller, '_select'];
	edit_property=[caller, '_edit'];
	
	rndtypes=handles.rndtypes;
	index=get(handles.(select_property),'value')-1;
%	set(handles.(edit_property),'String','');

	if index>0
		handles.(caller)=eval(rndtypes{index});
	else
		handles.(caller)=[];
	end;
	set(handles.(edit_property),'String',get(handles.(caller),'R_str'));

end;

if strcmp(action,'import_button')

	objects={'f','h','w','e','p0'};
	import_select=get(handles.import_select,'Value');
	caller=objects{import_select};
	import_edit=get(handles.import_edit,'String');
	
	if isempty(import_edit)
		% The edit control is blank
		msgbox('No object name was specified!');
	else
		data=evalin('base',import_edit,'[]');
		if isa(data,'logical')
			msgbox('Error! There was no object returned.');
		else
			% Object imported successfully
			if isa(data,'double')
				if import_select>2
					data=gauss(data);
				else
					data=xlinear(data);
				end
			elseif isa(data,'char')||isa(data,'inline')||isa(data,'sym')||isa(data,'cell')
				if import_select>2
					msgbox('Sorry, this datatype is not convertable to a noise object');
					return;
				else
					data=xsymbolic(data);
				end
			end
			handles.(caller)=data;
			guidata(hObject,handles);
			if import_select>2
				model_action_update_select(hObject,caller,'rndtypes','R_str');
			else
				model_action_update_select(hObject,caller,'datatypes','str');
			end
			handles=guidata(hObject);			% FIX: UGLY (last row...)
		end
	end
end

guidata(hObject,handles);