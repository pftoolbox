function danl_update_checkbox(hObject, i);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%myfont='FixedWidth';
myfont='default';

handles=guidata(hObject);
hf=hObject;
dnames=handles.dnames;

if isa(i,'double')
	caller=dnames{i};
elseif isa(i,'char')
	caller=i;
	i=findcstr(dnames,caller);
else
	error('''i'' must be a string or a string');
end

obj=handles.(caller);

if isfield(handles,[caller,'_checkbox'])
	delete(handles.([caller,'_checkbox']));
	delete(handles.([caller,'_checkbox_text']));
	handles=rmfield(handles,[caller,'_checkbox']);
	handles=rmfield(handles,[caller,'_checkbox_text']);
end

if isa(obj,'xlinear')

	evalvar=get(obj,'evalvar');

	if sum(find(evalvar==1))>0
		evalvar=1;
	else
		evalvar=0;
	end
%	set(handles.([caller,'_checkbox']),'value',evalvar);
	pos=i-1;
	postop=0.45;	% Put in the structure or something. Bl��h
	posx=-0.40;	%	% Put in the structure or something. Bl��h
	space=0.06;

	tooltip=['Two alternatives: ',dnames{i},'(x,t)=Ax or ',dnames{i},'(x,t)=A'];
	handles.([caller,'_checkbox'])=uicontrol(hf,'Style','Checkbox','Value',evalvar,'Units','normalized','Position',[posx+0.86 postop-space*pos 0.03 0.04],'Callback',['danl_action(',num2str(hf),',''check'',''',caller,''')'],'Tag',[caller,'_checkbox'],'TooltipString',tooltip,'Fontname',myfont);
	handles.([caller,'_checkbox_text'])=uicontrol(hf,'Style','Text','String','*','Units','normalized','Position',[posx+0.89 postop-space*pos 0.02 0.04],'Tag',[caller,'_checkbox_text'],'HorizontalAlign','Left','TooltipString',tooltip,'Fontname',myfont);

end

guidata(hf,handles);
