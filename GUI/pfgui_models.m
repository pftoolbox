function pfgui_models(action,varargin);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init');
	parentfig=varargin{1};
	callback=varargin{2};

	parenthandles=guidata(parentfig);
	model_menu=parenthandles.model_menu;

	model_objects=parenthandles.model_objects;
	model_names=parenthandles.model_names;

	model_types=pfgui_config('model_types');
	model_descriptions=cell(1,length(model_types)+1);
	model_descriptions{1}='Create a new model object';
	for i=1:length(model_types)
		model_descriptions{i+1}=eval(sprintf('get(%s,''description'')',model_types{i}));
	end

%	hf=figure('Name','Models','NumberTitle','off','Units','normalized');
%	hf=figure('Name','Models','NumberTitle','off','Units','Centimeters','Position',[3 2 15 10],'Units','Normalized','PaperUnits','Centimeters','PaperPosition',[0.25 3 18 12]);
	hf=figure('Name','Models');

	% For printing
	uicontrol(hf,'Style','Frame','Units','normalized','Position',[-0.1 -0.1 1.2 1.2]);

	p=0.6;
	% Frame + Model text
	model_tooltip='Create and edit the models';
	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.02 p-0.08*2-0.02 0.96 0.28+0.08*2+0.02],'TooltipString',model_tooltip);
	uicontrol(hf,'Style','Text','String','Models','Units','normalized','Position',[0.05 p+0.25 0.1 0.04],'TooltipString',model_tooltip,'Fontname',myfont);

	% Model selector
	model_select_tooltip='Select a model object to perform operations on';
	uicontrol(hf,'Style','PopUp','String',model_menu,'Value',1,'Units','normalized','Position',[0.05 p+0.18 0.9 0.05],'Tag','model_select','TooltipString',model_select_tooltip,'Fontname',myfont);

	% Edit, Copy, Delete, Rename
	edit_tooltip='Edit the model object';
	copy_tooltip='Copy the model object (Enter the new name in the name field)';
	delete_tooltip='Delete the model object';
	rename_tooltip='Rename the model object (Enter the new name in the name field)';
	uicontrol(hf,'Style','Pushbutton','String','Edit','Units','normalized','Position',[0.05 p+0.1 0.1 0.05],'Callback',['pfgui_models(''model_edit_button'',',num2str(hf),')'],'Tag','model_edit_button','TooltipString',edit_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','Copy','Units','normalized','Position',[0.15 p+0.1 0.1 0.05],'Callback',['pfgui_models(''model_copy_button'',',num2str(hf),')'],'Tag','model_copy_button','TooltipString',copy_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','Delete','Units','normalized','Position',[0.25 p+0.1 0.1 0.05],'Callback',['pfgui_models(''model_delete_button'',',num2str(hf),')'],'Tag','model_delete_button','TooltipString',delete_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','Rename','Units','normalized','Position',[0.35 p+0.1 0.1 0.05],'Callback',['pfgui_models(''model_rename_button'',',num2str(hf),')'],'Tag','model_rename_button','TooltipString',rename_tooltip,'Fontname',myfont);

	% Model importer
	model_import_edit_tooltip='Enter the name of the object to be imported here';
	model_import_button_tooltip='Import a model object from the workspace';
	uicontrol(hf,'Style','Edit','String','','Units','normalized','Position',[0.54 p+0.1 0.3 0.05],'Callback',['pfgui_models(''modelimport_button'',',num2str(hf),')'],'Tag','modelimport_edit','TooltipString',model_import_edit_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','Import','Units','normalized','Position',[0.85 p+0.1 0.1 0.05],'Callback',['pfgui_models(''modelimport_button'',',num2str(hf),')'],'Tag','modelimport_button','TooltipString',model_import_button_tooltip,'Fontname',myfont);

	% Model name
	model_name_tooltip='The name of any new model created or imported (this is the name field)';
	uicontrol(hf,'Style','Text','String','Name of new/imported model:','Units','normalized','Position',[0.05 p+0.02 0.5 0.05],'HorizontalAlign','Left','TooltipString',model_name_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Edit','String','New model object','Units','normalized','Position',[0.54 p+0.02 0.41 0.05],'Callback',['pfgui_models(''modelimport_name'',',num2str(hf),')'],'Tag','model_name','TooltipString',model_name_tooltip,'Fontname',myfont);

	% Model exporter
	model_export_edit_tooltip='Enter the name of the workspace variable which to the model will be assigned';
	model_export_button_tooltip='Export a model object to the workspace';
	uicontrol(hf,'Style','Text','String','Export model to workspace:','Units','normalized','Position',[0.05 p-0.06 0.5 0.05],'HorizontalAlign','Left','TooltipString',model_export_edit_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Edit','String','','Units','normalized','Position',[0.54 p-0.06 0.3 0.05],'Callback',['pfgui_models(''modelexport_button'',',num2str(hf),')'],'Tag','modelexport_edit','TooltipString',model_export_edit_tooltip,'Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','Export','Units','normalized','Position',[0.85 p-0.06 0.1 0.05],'Callback',['pfgui_models(''modelexport_button'',',num2str(hf),')'],'Tag','modelexport_button','TooltipString',model_export_button_tooltip,'Fontname',myfont);

	% Model type selector
	model_new_tooltip='Create a new model (Enter the new name in the name field)';
	uicontrol(hf,'Style','PopUp','String',cell2str(model_descriptions),'Value',1,'Units','normalized','Position',[0.05 p-0.06-0.08 0.9 0.05],'Callback',['pfgui_models(''new_model'',',num2str(hf),')'],'Tag','new_model','TooltipString',model_new_tooltip,'Fontname',myfont);

	% Exit button
	uicontrol(hf,'Style','Pushbutton','String','Exit','Units','normalized','Position',[0.05 0.06 0.1 0.05],'Callback',['close(',num2str(hf),')'],'Fontname',myfont);

	handles=guihandles(hf);
	handles.parentfig=parentfig;
	handles.callback=callback;
	handles.model_objects=model_objects;
	handles.model_names=model_names;
	handles.model_types=model_types;
	handles.usize=size(parenthandles.u,1);
	guidata(hf,handles);

elseif strcmp(action,'model_copy_button')
	hf=varargin{1};
	handles=guidata(hf);

	model_select_value=get(handles.model_select,'value')-1;
	if model_select_value<1
		msgbox('You must select a model to copy!');
		return;
	end

	model_name=get(handles.model_name,'string');
	if isempty(model_name)
		msgbox('Please enter a name for the new model.');
		return;
	end;

	model_objects=handles.model_objects;
	model_names=handles.model_names;

	index=length(model_objects)+1;
	model_objects{index}=model_objects{model_select_value};
	model_names{index}=model_name;

	handles.model_objects=model_objects;
	handles.model_names=model_names;
	guidata(hf,handles);

	pfgui_models_menu_select(hf,index+1);

elseif strcmp(action,'model_delete_button')
	hf=varargin{1};
	handles=guidata(hf);

	model_objects=handles.model_objects;
	model_names=handles.model_names;

	model_select_value=get(handles.model_select,'value')-1;
	if model_select_value<1
		msgbox('You must select a model to delete!');
		return;
	end
		
	model_objects(model_select_value:end-1)=model_objects(model_select_value+1:end);
	model_objects=model_objects(1:end-1);

	model_names(model_select_value:end-1)=model_names(model_select_value+1:end);
	model_names=model_names(1:end-1);

	handles.model_objects=model_objects;
	handles.model_names=model_names;
	guidata(hf,handles);
	pfgui_models_menu_select(hf,1);

elseif strcmp(action,'model_rename_button')
	hf=varargin{1};
	handles=guidata(hf);

	model_select_value=get(handles.model_select,'value')-1;
	if model_select_value<1
		msgbox('You must select a model to rename!');
		return;
	end
	model_name=get(handles.model_name,'string');
	if isempty(model_name)
		msgbox('Please enter a new name for the model.');
		return;
	end;

	model_names=handles.model_names;

	model_names{model_select_value}=model_name;
	handles.model_names=model_names;
	guidata(hf,handles);
	pfgui_models_menu_select(hf,0);

elseif strcmp(action,'model_edit_button')
	hf=varargin{1};
	handles=guidata(hf);

	model_select_value=get(handles.model_select,'value')-1;
	if model_select_value<1
		msgbox('You must select a model to edit!');
		return;
	end
	
	model=handles.model_objects{model_select_value};
	model_function=[class(model),'_edit'];
	
	feval(model_function,handles.model_objects{model_select_value},hf,model_select_value,['pfgui_models(''model_refresh'', ', num2str(hf), ', ', num2str(model_select_value), ');']);

elseif strcmp(action,'model_refresh')
	hf=varargin{1};
	handles=guidata(hf);

%	caller=varargin{1};
%	model_objects=handles.model_objects;
%%	model.objects{caller}=
	pfgui_models_menu_select(hf);

elseif strcmp(action,'new_model')
	hf=varargin{1};
	handles=guidata(hf);

%	caller=varargin{1};
%	model_objects=handles.model_objects;
%%	model.objects{caller}=

	model_objects=handles.model_objects;
	model_names=handles.model_names;
	
	model_value=get(handles.new_model,'Value')-1;
	if model_value==0
		% No object is selected
		return
	end;

	model_type=handles.model_types{model_value};

	model_name=get(handles.model_name,'string');
	if isempty(model_name)
		msgbox('Please enter a name for the new model.');
		return;
	end;

	index=length(model_objects)+1;

	model_names{index}=model_name;
	eval(sprintf('model_objects{index}=%s;',model_type));

	handles.model_names=model_names;
	handles.model_objects=model_objects;
	guidata(hf,handles);
	pfgui_models_menu_select(hf,index+1);

	edit_function=[model_type,'_edit'];
	feval(edit_function,model_objects{index},hf,index,['pfgui_models(''new_model_refresh'', ', num2str(hf), ', ', num2str(index), ');']);

elseif strcmp(action,'new_model_refresh')
	hf=varargin{1};
	handles=guidata(hf);
	
	pfgui_models_menu_select(hf);

elseif strcmp(action,'modelexport_button')
	hf=varargin{1};
	handles=guidata(hf);

	model_select_value=get(handles.model_select,'value')-1;
	if model_select_value<1
		msgbox('You must select a model to export!');
		return;
	end

	str=get(handles.modelexport_edit,'String');	
	if isempty(str)
		msgbox('The variable name must not be empty!');
		return;
	end

	assignin('base',str,handles.model_objects{model_select_value});
	msgbox('If no error messages occured in the workspace, the export was completed successfully');

elseif strcmp(action,'modelimport_button')
	hf=varargin{1};
	handles=guidata(hf);

	model_objects=handles.model_objects;
	model_names=handles.model_names;

	modelimport_edit=get(handles.modelimport_edit,'string');
	if isempty(modelimport_edit)
		% The edit control is blank
		msgbox('No model name was specified!');
	else
		model=evalin('base',modelimport_edit,'[]');
		if isa(model,'double')
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
		elseif isa(model,'char')||isa(model,'cell')||isa(model,'logical')
			msgbox('Error! There was no object returned.');
		else

			% Object imported successfully
			index=length(model_objects)+1;
			model_objects{index}=model;
			model_names{index}=get(handles.model_name,'string');

			handles.model_objects=model_objects;
			handles.model_names=model_names;
			guidata(hf,handles);		%Save

			pfgui_models_menu_select(hf,index+1);
		end
	end			
end
