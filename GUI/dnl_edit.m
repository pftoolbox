function dnl_edit(obj, hObject, varargin)
% NOTE: Model edit windows do not use the "action" input argument,
% as all other edit windows do.
% This will be fixed as soon as possible. In the meantime, just exclude the first input
% argument when programming own model edit windows.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

% Sorry, I didn't have enough time to write nice GUI code.

myfont='default';

hf=figure('Name','DNL Edit','NumberTitle','off','Units','Centimeters','Position',[1 3 15 10],'Units','Normalized','PaperUnits','Centimeters','PaperPosition',[0.25 3 18 12]);

datatypes=pfgui_config('expression_types');

object_str='Undefined';
for i=1:length(datatypes)
	object_str=[object_str,'|',eval(['get(',datatypes{i},',''description'')'])];
end;

dnames={'f','h'};
postop=0.37;
space=0.08;

pos=0;
for i=1:length(dnames);
	currobj=dnames{i};

	if ~isempty(obj.(currobj));
		str=get(obj.(currobj),'str');
	else;
		str=[];
	end;

	index=0;
	for j=1:length(datatypes)
		if isa(obj.(currobj),datatypes{j})
			index=j;
			break;
		end;
	end;

	uicontrol(hf,'Style','Text','String',currobj,'Units','normalized','Position',[0.0 postop-space*pos 0.055 0.05],'TooltipString','The actual object','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.065 postop-space*pos 0.38 0.05],'Tag',[currobj,'_edit'],'callback',['dnl_action(',num2str(hf),',''edit_data'',''',currobj,''')'],'TooltipString','A string representation of the object','Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','E','Units','normalized','Position',[0.46 postop-space*pos 0.03 0.05],'callback',['dnl_action(',num2str(hf),',''objedit'',''',currobj,''')'],'Tag',[currobj,'_objedit'],'TooltipString','Edit the object','Fontname',myfont);
	uicontrol(hf,'Style','Popup','String',object_str,'Value',index+1,'Units','normalized','Position',[0.5 postop-space*pos 0.49 0.05],'callback',['dnl_action(',num2str(hf),',''select'',''',currobj,''')'],'Tag',[currobj,'_select'],'TooltipString','Object type','Fontname',myfont);

	pos=pos+1;
end;

%-------------- rnd stuff

rndtypes=pfgui_config('noise_types');
rndselect_str='Undefined';
for i=1:length(rndtypes)
	rndselect_str=[rndselect_str,'|',eval(['get(',rndtypes{i},',''description'')'])];
end;

rnames={'w','e','p0'};

postop=0.18;
space=0.08;

pos=0;
for i=1:length(rnames);
	currobj=rnames{i};

	if ~isempty(obj.(currobj));
		str=get(obj.(currobj),'R_str');
	else;
		str=[];
	end;

	index=0;
	for j=1:length(rndtypes)
		if isa(obj.(currobj),rndtypes{j})
			index=j;
			break;
		end;
	end;

	uicontrol(hf,'Style','Text','String',currobj,'Units','normalized','Position',[0.0 postop-space*pos 0.055 0.05],'TooltipString','The actual object','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.065 postop-space*pos 0.38 0.05],'Tag',[currobj,'_edit'],'callback',['dnl_action(',num2str(hf),',''rnd_edit'',''',currobj,''')'],'TooltipString','A string representation of the object','Fontname',myfont);
	uicontrol(hf,'Style','Pushbutton','String','E','Units','normalized','Position',[0.46 postop-space*pos 0.03 0.05],'callback',['dnl_action(',num2str(hf),',''rnd_objedit'',''',currobj,''')'],'Tag',[currobj,'_objedit'],'TooltipString','Edit the object','Fontname',myfont);
	uicontrol(hf,'Style','Popup','String',rndselect_str,'Value',index+1,'Units','normalized','Position',[0.5 postop-space*pos 0.49 0.05],'callback',['dnl_action(',num2str(hf),',''rnd_select'',''',currobj,''')'],'Tag',[currobj,'_select'],'TooltipString','Object type','Fontname',myfont);

	pos=pos+1;
end;

T=obj.T;
xvars=obj.xvars;
x0=obj.x0;
states=length(x0);

% Description frame
uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.845-0.04 0.37 0.125+0.04]);
uicontrol(hf,'Style','Text','String','x(t+T) = f(x,t,u,w)','Units','normalized','Position',[0.01 0.9 0.34 0.04],'HorizontalAlign','Left','Fontname','FixedWidth');
uicontrol(hf,'Style','Text','String','  y(t) = h(x,t,u) + e(t)','Units','normalized','Position',[0.01 0.86 0.34 0.04],'HorizontalAlign','Left','Fontname','FixedWidth');
uicontrol(hf,'Style','Text','String','  x(0) = x0 + p0','Units','normalized','Position',[0.01 0.82 0.34 0.04],'HorizontalAlign','Left','Fontname','FixedWidth');
uicontrol(hf,'Style','Text','String','Discrete Non-Linear','Units','normalized','Position',[0.01 0.95 0.31 0.04],'Fontname',myfont);

% Main controls
uicontrol(hf,'Style','Text','String','Sample time (T):','Units','normalized','Position',[0.0 0.71 0.28 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',num2str(T),'Units','normalized','Position',[0.29 0.71 0.08 0.05],'callback',['dnl_action(',num2str(hf),',''T_edit'')'],'Tag','T_edit','Fontname',myfont);


o1x=0.0;
o1y=-0.02;

uicontrol(hf,'Style','Frame','Units','normalized','Position',[o1x+0.535 o1y+0.735 0.455 0.255],'callback',['dnl_action(',num2str(hf),',''states_edit'')']);

uicontrol(hf,'Style','Text','String','Names (xvars):','Units','normalized','Position',[o1x+0.54 o1y+0.855 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',cell2cellstr(xvars),'Units','normalized','Position',[o1x+0.75 o1y+0.855 0.2 0.05],'Tag','xvars_edit','callback',['dnl_action(',num2str(hf),',''xvars_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Text','String','x0 vector:','Units','normalized','Position',[o1x+0.54 o1y+0.80 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',num2str2(x0),'Units','normalized','Position',[o1x+0.75 o1y+0.80 0.2 0.05],'Tag','edit_x0','callback',['dnl_action(',num2str(hf),',''x0_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Text','String','Generate p0 noise:','Units','normalized','Position',[o1x+0.54 o1y+0.74 0.3 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Checkbox','Units','normalized','Position',[o1x+0.93 o1y+0.74 0.03 0.05],'Tag','check_p0','Value',1,'Fontname',myfont);
uicontrol(hf,'Style','Text','String','States:','Units','normalized','Position',[o1x+0.54 o1y+0.91 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',num2str(states),'Units','normalized','Position',[o1x+0.75 o1y+0.91 0.095 0.05],'Tag','edit_states','callback',['dnl_action(',num2str(hf),',''statenr_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Pushbutton','String','Edit','Units','normalized','Position',[o1x+0.85 o1y+0.91 0.1 0.05],'callback',['dnl_action(',num2str(hf),',''states_edit'')'],'Fontname',myfont);


% Save / Cancel
blah=-0.03;
uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.4,0.95+blah,0.1,0.05],'Callback',['dnl_action(',num2str(hf),',''save'')'],'Fontname',myfont);
uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.4,0.9+blah,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

% uvars + wvars frame
%uicontrol(hf,'Style','Frame','Units','normalized','Position',[0 0.58 1 0.09],'callback',['dnl_action(',num2str(hf),',''states_edit'')']);

% uvars
uvars=obj.uvars;
uicontrol(hf,'Style','Frame','Units','normalized','Position',[0 0.58 0.46 0.09]);
uicontrol(hf,'Style','Text','String','uvars:','Units','normalized','Position',[0.01 0.6 0.1 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',cell2cellstr(uvars),'Units','normalized','Position',[0.12 0.6 0.2 0.05],'Tag','uvars_edit','callback',['dnl_action(',num2str(hf),',''uvars_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Pushbutton','String','Edit','Units','normalized','Position',[0.33 0.6 0.1 0.05],'callback',['dnl_action(',num2str(hf),',''uvars_edit_button'')'],'Fontname',myfont);

% wvars
wvars=obj.wvars;
uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.53 0.58 0.46 0.09]);
uicontrol(hf,'Style','Text','String','wvars:','Units','normalized','Position',[0.54 0.6 0.1 0.05],'HorizontalAlign','Left','Fontname',myfont);
uicontrol(hf,'Style','Edit','String',cell2cellstr(wvars),'Units','normalized','Position',[0.65 0.6 0.2 0.05],'Tag','wvars_edit','callback',['dnl_action(',num2str(hf),',''wvars_edit'')'],'Fontname',myfont);
uicontrol(hf,'Style','Pushbutton','String','Edit','Units','normalized','Position',[0.86 0.6 0.1 0.05],'callback',['dnl_action(',num2str(hf),',''wvars_edit_button'')'],'Fontname',myfont);

% Importer
uicontrol(hf,'Style','Frame','Units','normalized','Position',[0 -0.01+0.48 0.46 0.09]);
uicontrol(hf,'Style','Popup','String','f|h|w|e|p0','Value',1,'Units','normalized','Position',[0.01 -0.01+0.5 0.1 0.05],'Tag','import_select','TooltipString','Object to import from the workspace','FontName',myfont);
uicontrol(hf,'Style','Edit','String','','Units','normalized','Position',[0.12 -0.01+0.5 0.2 0.05],'callback',['dnl_action(',num2str(hf),',''import_button'')'],'Tag','import_edit','Fontname',myfont);
uicontrol(hf,'Style','Pushbutton','String','Import','Units','normalized','Position',[0.33 -0.01+0.5 0.1 0.05],'callback',['dnl_action(',num2str(hf),',''import_button'')'],'Fontname',myfont);


handles=guihandles(hf);
handles.parentfig=hObject;
handles.caller=varargin{1};
handles.callback=varargin{2};
handles.datatypes=datatypes;
handles.rndtypes=rndtypes;
handles.dnames=dnames;
handles.obj=obj;
handles.f=obj.f;
handles.h=obj.h;
handles.w=obj.w;
handles.e=obj.e;
handles.p0=obj.p0;
handles.x0=x0;
handles.xvars=xvars;
handles.uvars=uvars;
handles.wvars=wvars;
handles.T=T;
handles.states=states;
guidata(hf,handles);
