function xlinear_edit(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	caller=varargin{3};
	callback=varargin{4};

	% Make the GUI show the correct information
	if strcmp(caller,'h')||strcmp(caller,'hu')
		if strcmp(obj.wchar,'w')
			set(obj,'wchar','e');
		end
	else
		if strcmp(obj.wchar,'e')
			set(obj,'wchar','w');
		end
	end	

	A=obj.expression;
	evalvar=obj.evalvar;

	evalvarvec=zeros(1,4);
	for i=1:4
		if sum(find(evalvar==i))
			evalvarvec(i)=1;
		else
			evalvarvec(i)=0;
		end;
	end;


%	hf=figure('Name','xlinear object editor','NumberTitle','off','Units','Characters');
	hf=figure('Name','xlinear object editor');

	m=size(A,1);
	n=size(A,2);
	uicontrol('Style','Edit','String',num2str(m),'Units','normalized','Tag','rows','Position',[0,0.02,0.04,0.05],'Fontname',myfont);
	uicontrol('Style','Text','String','x','Units','normalized','Position',[0.04,0.02,0.04,0.05],'Fontname',myfont);
	uicontrol('Style','Edit','String',num2str(n),'Units','normalized','Tag','columns','Position',[0.08,0.02,0.04,0.05],'Fontname',myfont);

	uicontrol('Style','Pushbutton','String','Resize','Units','normalized','Position',[0.15,0.02,0.15,0.05],'Callback',['xlinear_edit(''resize'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['xlinear_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Frame','Units','normalized','Position',[0,0.1,1,0.003]);

	uicontrol('Style','Text','String','Multiply by:','Units','normalized','Position',[0.32,0.04,0.2,0.04],'Fontname',myfont);
	uicontrol('Style','Text','String','','Units','normalized','Position',[0.32,0.00,0.2,0.04],'Tag','vars','Fontname',myfont);


	% Special case treatment for the user's convenience.
	% Future project: Make this general! Maybe an argument describing
	% the possible variables.
	checku_visible='on';
	checkw_visible='on';
	
	parenthandles=guidata(parentfig);
	model=parenthandles.obj;
	
	if isa(model,'danl');
		checku_visible='off';
		checkw_visible='off';
		evalvarvec(3:4)=[0 0];
	end

	if isa(model,'dnl')&&strcmp(caller,'h');
		checkw_visible='off';
		evalvarvec(4)=0;
	end
		
	uicontrol('Style','Checkbox','String','x','Units','normalized','Value',evalvarvec(1),'Position',[0.6,0.05,0.06,0.04],'Tag','check_1','callback',['xlinear_edit(''check'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Checkbox','String','t','Units','normalized','Value',evalvarvec(2),'Position',[0.7,0.05,0.06,0.04],'Tag','check_2','callback',['xlinear_edit(''check'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Checkbox','String','u','Units','normalized','Value',evalvarvec(3),'Position',[0.6,0.00,0.06,0.04],'Tag','check_3','callback',['xlinear_edit(''check'',',num2str(hf),')'],'Visible',checku_visible,'Fontname',myfont);
	uicontrol('Style','Checkbox','String',obj.wchar,'Units','normalized','Value',evalvarvec(4),'Position',[0.7,0.00,0.06,0.04],'Tag','check_4','callback',['xlinear_edit(''check'',',num2str(hf),')'],'Visible',checkw_visible,'Fontname',myfont);

	uicontrol('Style','Frame','Units','normalized','Position',[0.32,0,0.002,0.1]);
	uicontrol('Style','Frame','Units','normalized','Position',[0.78,0,0.002,0.1]);

	% Generate the edit matrix
	for j=1:m
		for i=1:n
			uicontrol('Style','Edit','String',num2str(A(j,i)),'Units','normalized','Tag',['edit',num2str(j),'_',num2str(i)],'Position',[0.1*(i-1),0.1*(m-j)+0.13,0.08,0.05],'callback',['xlinear_edit(''edit'',', num2str(hf), ',', num2str(j), ',', num2str(i), ')'],'Fontname',myfont);
		end
	end

	handles=guihandles(hf);
	handles.matrix=A;
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	handles.evalvar=evalvar;
	handles.wchar=obj.wchar;
	guidata(hf,handles);

	xlinear_edit('check',hf);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	caller=handles.caller;
	obj=handles.obj;

	set(obj,'expression',handles.matrix);
	set(obj,'evalvar',handles.evalvar);

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.(caller)=obj;
	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

elseif strcmp(action,'check')

	hf=varargin{1};
	handles=guidata(hf);

	str='';
	varnames={'x','t','u',handles.wchar};
	evalvar=[];
	for i=1:length(varnames)
		if get(handles.(['check_',num2str(i)]),'value')
			str=[str, varnames{i}, '; '];
			evalvar=[evalvar,i];
		end
	end
	if length(str)>1
		str=str(1:end-2);
		if length(str)>1
			str=['[', str, ']'];
		end
	end

	if isempty(str)
		str='Nothing';
	end

	set(handles.vars,'String',str);
	handles.evalvar=evalvar;
	guidata(hf,handles);

elseif strcmp(action,'edit')

	hf=varargin{1};
	handles=guidata(hf);
	A=handles.matrix;

	j=varargin{2};
	i=varargin{3};

	source=['edit',num2str(j),'_',num2str(i)'];
	str=get(handles.(source),'String');

	num=str2num(str);		% WANTED: Error checking
	str=num2str(num);		% Format the data
	
	A(j,i)=num;

	set(handles.(source),'String',str);

	handles.matrix=A;
	guidata(hf,handles);

elseif strcmp(action,'resize')
	hf=varargin{1};
	handles=guidata(hf);

	A=handles.matrix;
	m=size(A,1);
	n=size(A,2);

	% Delete edit controls
	for j=1:m
		for i=1:n
			delete(handles.(['edit',num2str(j),'_',num2str(i)]));
		end
	end

	% Generate new values
	m=str2num(get(handles.rows,'string'));
	n=str2num(get(handles.columns,'string'));

	A=join(A,zeros(m,n));

	% Generate the edit matrix
	for j=1:m
		for i=1:n
			uicontrol('Style','Edit','String',num2str(A(j,i)),'Units','normalized','Tag',['edit',num2str(j),'_',num2str(i)],'Position',[0.1*(i-1),0.1*(m-j)+0.13,0.08,0.05],'callback',['xlinear_edit(''edit'',', num2str(hf), ',', num2str(j), ',', num2str(i), ')'],'Fontname',myfont);
		end
	end

	% Save stuff from the old handles
	obj=handles.obj;
	parentfig=handles.parentfig;
	caller=handles.caller;
	callback=handles.callback;
	evalvar=handles.evalvar;

	% Generate new handles structure
	handles=guihandles(hf);

	% Put back the saved data
	handles.obj=obj;
	handles.parentfig=parentfig;
	handles.caller=caller;
	handles.callback=callback;
	handles.evalvar=evalvar;

	handles.matrix=A;
	guidata(hf,handles);
end;
