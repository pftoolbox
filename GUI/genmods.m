function varargout=genmods;
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

T=1;
f=[1 0 T 0; 0 1 0 T; 0 0 1 0; 0 0 0 1];
gw=[0.5*T^2 0; 0 0.5*T^2; T 0; 0 T];
%h={'sqrt(x1.^2+x2.^2)', 'atan(x2./x1)', 'x4'};
h={'x1','x2','x4'};

model=danl(f,gw,[],h,[],[0 0 0 0]',[1 1],[0.25 0.2 0.1], [], 1, {'x1','x2','x3','x4'});

f=[1 0 T 0 0.5*T^2 0; 0 1 0 T 0 0.5*T^2; 0 0 1 0 T 0; 0 0 0 1 0 T];
model4=dnl(xlinear(f,[1 4],[4 2]),h,[0 0 0 0]',[1 1],[0.25 0.2 0.1], [], 1, {'x1','x2','x3','x4'});

h={'x1+e1','x2+e2','x4+e3'};
model5=dgnl(xlinear(f,[1 4],[4 2]),h,@h_inv_1,[0 0 0 0]',[1 1],[0.25 0.2 0.1], [], 1, {'x1','x2','x3','x4'});

%%

T=1;
f='cos(x1.^2)+sin(x1)+x1';
h={'cos(x1)','sin(x1)'};

model2=danl(f, 1, [], h, [], 0, 1, [0.0025 0.002], 1);

%%

T=1;
A=[1 0 T 0; 0 1 0 T; 0 0 1 0; 0 0 0 1];
B=[0.5*T^2 0; 0 0.5*T^2; T 0; 0 T];
h={'sqrt(x1.^2+x2.^2)', 'atan2(x2,x1)'};

gradx={'1/(x1^2+x2^2+eps)^(1/2)*x1', '1/(x1^2+x2^2+eps)^(1/2)*x2', '0', '0'; '-x2/(x1^2+eps)/(1+x2^2/(x1^2+eps))', '1/(x1+eps)/((1+x2^2+eps)/(x1^2+eps))', '0', '0'};

model3=danl(A,B,[],h,[],[0 0 0 0],[1 1],[1 1], [1 1 1 1]');
model3=set(model3,'hgradx',gradx);

varargout={model,model2,model3,model4,model5};