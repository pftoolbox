function model_edit_states(action,varargin);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init')
	callback=varargin{2};
	parentfig=varargin{1};
	parenthandles=guidata(parentfig);
	
	xvars=parenthandles.xvars;
	x0=parenthandles.x0;
	states=parenthandles.states;

	if length(xvars)~=states
		if isempty(xvars);
			% Generate a default vector
			xvars=cell(1,states);
			for i=1:states
				xvars{i}=['x',num2str(i)];
			end		
		else
			msgbox('The number of elements in ''xvars'' does not match the number of states');
			return;
		end
	end

	if length(x0)~=states
		if isempty(x0);
			% Generate a default vector
			x0=zeros(states,1);
		else
			msgbox('The number of elements in ''x0'' does not match the number of states');
			return;
		end
	end

	hf=figure('Name','State editor');

	uicontrol('Style','Pushbutton','String','Save','Units','Normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['model_edit_states(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','Normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

	uicontrol(hf,'Style','Text','String','State','Units','normalized','Position',[0.0 0.9 0.1 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Text','String','Name','Units','normalized','Position',[0.15 0.9 0.3 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Text','String','Initial value','Units','normalized','Position',[0.8 0.9 0.2 0.05],'HorizontalAlign','Left','Fontname',myfont);

	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.88 1 0.003]);

%	xvars={'x1','x2','x3','x4'};
%	x0=[0 0 0 0];
%	states=4;

	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.1 1 0.003]);
	uicontrol(hf,'Style','Text','String','States:','Units','normalized','Position',[0.0 0.02 0.20 0.05],'HorizontalAlign','Left','Fontname',myfont);
	handles=guihandles(hf);
	handles.edit_statenumber=uicontrol(hf,'Style','Edit','String',num2str(states),'Units','normalized','Position',[0.2 0.02 0.1 0.05],'callback',['model_edit_states(''changenumber'',' ,num2str(hf), ')'],'Fontname',myfont);

	handles.xvars=xvars;
	handles.states=states;
	handles.x0=x0;
	handles.parentfig=parentfig;
	handles.callback=callback;
	guidata(hf,handles);	

	model_edit_states('updatestates',hf);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	states=handles.states;
	xvars=cell(1,states);
	x0=zeros(states,1);
	
	for i=1:handles.states
		name_str=get(handles.(['name_',num2str(i)]),'String');
		init_str=get(handles.(['init_',num2str(i)]),'String');
		
		if isempty(name_str)||isempty(init_str)
			msgbox('Error! A field is empty');
			return;
		end
		
		x0(i)=str2num(init_str);
		xvars{i}=name_str;
	end
	
	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.states=states;
	parenthandles.x0=x0;
	parenthandles.xvars=xvars;

	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

elseif strcmp(action,'changenumber')

	hf=varargin{1};
	handles=guidata(hf);	
	states_old=handles.states;
	states=str2num(get(handles.edit_statenumber,'String'));
	sdiff=states-states_old;

	xvars=handles.xvars;
	x0=handles.x0;

	if sdiff>0
		% Add more states
		x0(end+1:states)=0;
		for i=states_old+1:states
			xvars{i}=['x',num2str(i)];
		end;
	elseif sdiff<0
		% Delete some states
		x0=x0(1:states);
		xvars=xvars(1:states);
	else
		% Nothing has changed. Do nothing.
		return;
	end

	if size(x0,2)>1
		% A row vector! Let's transpone it.
		x0=x0';
	end;
	
	handles.xvars=xvars;
	handles.states=states;
	handles.x0=x0;
	guidata(hf,handles);

	model_edit_states('updatestates',hf);

elseif strcmp(action,'updatestates')

	hf=varargin{1};
	handles=guidata(hf);	

	states=handles.states;
	xvars=handles.xvars;
	x0=handles.x0;


	% Remove old controls
	i=1;
	while isfield(handles,['number_',num2str(i)])
		delete(handles.(['number_',num2str(i)]));
		delete(handles.(['name_',num2str(i)]));
		delete(handles.(['init_',num2str(i)]));
		handles=rmfield(handles,['number_',num2str(i)]);
		handles=rmfield(handles,['name_',num2str(i)]);
		handles=rmfield(handles,['init_',num2str(i)]);
		i=i+1;
	end;	

	postop=0.8;
	space=0.08;
	pos=0;

	% Add new controls
	for i=1:states
		str=xvars{i};
		x0_str=num2str(x0(i));

		handles.(['number_',num2str(i)])=uicontrol(hf,'Style','Text','String',num2str(i),'Units','normalized','Position',[0.0 postop-space*pos 0.1 0.05],'Fontname',myfont);
		handles.(['name_',num2str(i)])=uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.15 postop-space*pos 0.6 0.05],'Fontname',myfont);
		handles.(['init_',num2str(i)])=uicontrol(hf,'Style','Edit','String',x0_str,'Units','normalized','Position',[0.8 postop-space*pos 0.1 0.05],'Fontname',myfont);
		pos=pos+1;
	end

	guidata(hf,handles);
end
