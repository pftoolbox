function pfgui_menu_select(hf,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hf);

if nargin>1
	index=varargin{1};
else
	index=0;
end

model_objects=handles.model_objects;
model_names=handles.model_names;

% Generate the menu
model_number=length(model_objects);
model_descriptions=cell(model_number,1);
model_descriptions{1}='Select a model';

for i=1:model_number
	model_descriptions{i+1}=['[',class(model_objects{i}),'] ',model_names{i},' (',num2str(length(model_objects{i}.x0)),' states)'];
end

model_menu=cell2str(model_descriptions);

set(handles.filt_model,'string',model_menu);
set(handles.sim_model,'string',model_menu);

if index>1
	if get(handles.filt_model,'value')==1
		set(handles.filt_model,'value',index);
	end
	
	if get(handles.sim_model,'value')==1
		set(handles.sim_model,'value',index);
	end
end

handles.model_menu=model_menu;
guidata(hf,handles);
