% Just a testscript.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

hf=figure;
handles=guidata(hf);

handles.xhat=randn(3,100);
handles.xpred=rand(3,100);
handles.xtrue=ones(3,100);
%handles.xtrue=[];

clear filtobj;
filtobj.model.xvars={'x1','x2','x3'};
filtobj.Ts=0:size(handles.xhat,2)-1;
handles.filtobj=filtobj;

guidata(hf,handles);

pfgui_export('init',hf);
