function pfgui_rmse(action,hObject,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init')
	parenthandles=guidata(hObject);

	if isempty(parenthandles.xhat)&&isempty(parenthandles.xpred)
		msgbox('No filtered data available!');
		return;
	end

	if isempty(parenthandles.xtrue)
		msgbox('No true x(t) data exists.');
		return;
	end;

	if ~(size(parenthandles.xtrue,3)==1||size(parenthandles.xtrue,3)==max(size(parenthandles.xhat,3),size(parenthandles.xpred,3)))
		msgbox('Wrong dimensions of the true x(t) data.');
		return;
	end

	xhat_length=length(parenthandles.xhat);
	xpred_length=length(parenthandles.xpred);

%	xvars=parenthandles.filtobj.model.xvars;
	xvars=get(get(parenthandles.filtobj,'model'),'xvars');

	if xhat_length&&xpred_length
		lnames=cell(1,2*length(xvars));
		for i=1:length(xvars);
			lnames{i}=[xvars{i},' [xhat]'];
		end;
		for i=1:length(xvars);
			lnames{i+length(xvars)}=[xvars{i},' [xpred]'];
		end;
		lindex=[1:length(xvars), 1:length(xvars)];
		larray=[repmat({'xhat'},1,length(xvars)), repmat({'xpred'},1,length(xvars))];
	elseif xhat_length
		lnames=cell(1,length(xvars));
		lnames=xvars;
		lindex=[1:length(xvars)];
		larray=repmat({'xhat'},1,length(xvars));
	else
		lnames=cell(1,length(xvars));
		lnames=xvars;
		lindex=[1:length(xvars)];
		larray=repmat({'xpred'},1,length(xvars));
	end

	hf=figure('Name','RMSE command window');
	uicontrol(hf,'Style','Text','String','Select the state(s) to be included in the calculations:','Units','normalized','Position',[0.05 0.91 0.9 0.05],'HorizontalAlign','Left','Min',1,'Max',3,'FontName',myfont);

	uicontrol(hf,'Style','Listbox','String',cell2str(lnames),'Units','normalized','Position',[0.05 0.5 0.9 0.4],'Tag','state_select','HorizontalAlign','Left','Min',1,'Max',3,'Value',1:length(xvars),'FontName','FixedWidth');

	uicontrol(hf,'Style','Pushbutton','String','RMSE!','Units','normalized','Position',[0.05 0.1 0.9 0.05],'callback',sprintf('pfgui_rmse(''rmse'',%g,%g)',hObject,hf),'HorizontalAlign','Left','FontName',myfont);

	handles=guihandles(hf);
	handles.lnames=lnames;
	handles.lindex=lindex;
	handles.larray=larray;
	guidata(hf,handles);


elseif strcmp(action,'rmse');
	hf=varargin{1};
	parenthandles=guidata(hObject);
	handles=guidata(hf);
	
	states=get(handles.state_select,'value');

	if isempty(states)
		msgbox('Please select at least one state');
		return;
	end;

	Ts=parenthandles.filtobj.Ts;


	xhat_states=[];
	xpred_states=[];
	for i=1:length(states);
		if strcmp(handles.larray{states(i)},'xhat')
			xhat_states(end+1)=handles.lindex(i);
		else
			xpred_states(end+1)=handles.lindex(i);
		end
	end
	

	plotstr='plot(';
	simnames={};


	figure;

	% Hej Gustaf. Jag antar att du vill verifiera RMSE-ber�kningarna :)
	% Det �r inte s� snyggt, men jag ville inte sl�sa n�got krut p� n�got som kanske
	% �nd� �ndras snart... Du f�r fixa och trixa p� fyra st�llen... Du ser dem, f�rst�s
	
	legendcell={};
	runs=max(size(parenthandles.xhat,3),size(parenthandles.xpred,3));
	if xhat_states
		if size(parenthandles.xtrue,3)==runs
			rmse=sqrt(sum(sum((parenthandles.xhat(xhat_states,:,:)-parenthandles.xtrue(xhat_states,:,:)).^2,3),1)/runs);
		else				
			rmse=sqrt(sum(sum((parenthandles.xhat(xhat_states,:,:)-repmat(parenthandles.xtrue(xhat_states,:,1),[1 1 runs])).^2,3),1)/runs);
		end
		plot(Ts,rmse,getcolors(1));
		legendcell{end+1}='RMSE xhat';
		hold on
	else
		xhat_rmse=[];
	end;

	if xpred_states
		if size(parenthandles.xtrue,3)==runs
			xpred_rmse=sqrt(sum(sum((parenthandles.xpred(xpred_states,:,:)-parenthandles.xtrue(xpred_states,:,:)).^2,3),1)/runs);
		else
			xpred_rmse=sqrt(sum(sum((parenthandles.xpred(xpred_states,:,:)-repmat(parenthandles.xtrue(xpred_states,:,1),[1 1 runs])).^2,3),1)/runs);
		end
		plot(Ts,xpred_rmse,getcolors(2));
		legendcell{end+1}='RMSE xpred';
	else
		xpred_rmse=[];
	end;

	legend(legendcell{:});
	
end
