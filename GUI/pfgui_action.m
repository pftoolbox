function pfgui_action(action,hObject, varargin);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hObject);

if strcmp(action,'sim_radio_1')

	set(handles.sim_radio_1,'Value',1);
	set(handles.sim_radio_2,'Value',0);

	set(handles.sim_y_text,'Visible','off');
	set(handles.sim_y_edit,'Visible','off');
	set(handles.sim_y_size,'Visible','off');
	set(handles.sim_x_text,'Visible','off');
	set(handles.sim_x_edit,'Visible','off');
	set(handles.sim_x_size,'Visible','off');

	set(handles.sim_model,'Visible','on');
	set(handles.sim_steps_text,'Visible','on');
	set(handles.sim_steps,'Visible','on');

	handles.simulator_mode=true;
	guidata(hObject,handles);

end

if strcmp(action,'sim_radio_2')
	set(handles.sim_radio_2,'Value',1);
	set(handles.sim_radio_1,'Value',0);

	set(handles.sim_model,'Visible','off');
	set(handles.sim_steps_text,'Visible','off');
	set(handles.sim_steps,'Visible','off');

	set(handles.sim_y_text,'Visible','on');
	set(handles.sim_y_edit,'Visible','on');
	set(handles.sim_y_size,'Visible','on');
	set(handles.sim_x_text,'Visible','on');
	set(handles.sim_x_edit,'Visible','on');
	set(handles.sim_x_size,'Visible','on');
	
	handles.simulator_mode=false;
	guidata(hObject,handles);

end

if strcmp(action,'y_import')
	% Make this a shared function between x and y instead!
	y_edit_str=get(handles.sim_y_edit,'String');
	if isempty(y_edit_str)
		y=[];
		str='[No data]';
	else
		y=evalin('base',y_edit_str,'[]');
		if isempty(y)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(y,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		else
			% Object imported successfully
			str=['[',num2str(size(y,1)),' x ',num2str(size(y,2))];
			if size(y,3)>1
				str=[str, ' x ',num2str(size(y,3))];
			end			
			str=[str,']'];
		end
	end;
	handles.y=y;
	set(handles.sim_y_size,'String',str);
	guidata(hObject,handles);
end

if strcmp(action,'x_import')
	% Make this a shared function between x and y instead!
	x_edit_str=get(handles.sim_x_edit,'String');
	if isempty(x_edit_str)
		x=[];
		str='[No data]';
	else
		x=evalin('base',x_edit_str,'[]');
		if isempty(x)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(x,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		else
			% Object imported successfully
			str=['[',num2str(size(x,1)),' x ',num2str(size(x,2))];
			if size(x,3)>1
				str=[str, ' x ',num2str(size(x,3))];
			end			
			str=[str,']'];
		end
	end;
	handles.xtrue=x;
	set(handles.sim_x_size,'String',str);
	guidata(hObject,handles);
end

if strcmp(action,'filter')

	model_objects=handles.model_objects;

	simulator_mode=handles.simulator_mode;
	if simulator_mode
		% A simulator is chosen as input data
		sim_model_value=get(handles.sim_model,'value')-1;
		if sim_model_value<1
			msgbox('You must select a model for the simulation!');
			return;
		end
	else
		% A data matrix is chosen as input data
		if isempty(handles.y)
			msgbox('No y(t) data is available!');
			return;
		end
	end
	
	filt_model_value=get(handles.filt_model,'value')-1;
	if filt_model_value<1
		msgbox('You must select a model for the filter!');
		return;
	end

	xhat_mode=get(handles.xhat_check,'value');
	xpred_mode=get(handles.xpred_check,'value');
	if xhat_mode+xpred_mode==0
		msgbox('At least one of ''xhat'' or ''xpred'' must be checked');
		return;
	end

	% OK, we're ready to filter!
	t=handles.t;
	u=handles.u;

	filtobj=handles.filtobj;
	filt_model=model_objects{filt_model_value};

	% Set the u(t) varsize, if it exists. If not - do nothing!
	data={'f','gw','gu','h','hu'};
	for i=1:length(data)
		eval('varsize=get(filt_model.(data{i}),''varsize'');','varsize=[];'); 	% Try / Catch
		if ~isempty(varsize);
			varsize(3)=size(handles.u,1);
			filt_model.(data{i})=set(filt_model.(data{i}),'varsize',varsize);
		end
	end

	set(filtobj,'model',filt_model);

	states=length(get(filt_model,'x0'));
	runs=handles.runs;

	if simulator_mode
		model_objects=handles.model_objects;
		sim_model=model_objects{sim_model_value};
		simobj=simulator(sim_model);
		steps=handles.simulation_steps;
		xtrue=zeros(states,steps,runs);
	else
		xtrue=handles.xtrue;
		steps=size(handles.y,2);
		if size(handles.y,2)~=size(xtrue,2)&&~isempty(xtrue)
			msgbox('The number of colums of the y(t) and the true x(t) matrices must be identical!');
			return;
		end
	end;

	if xhat_mode
		xhat=zeros(states,steps,runs);
	else
		xhat=[];
	end;

	if xpred_mode
		xpred=zeros(states,steps,runs);
	else
		xpred=[];
	end

	wbar=waitbar(0,'Filtering...');
	for i=1:runs
		if simulator_mode
			simobj=reset(simobj,t);
			simulate(simobj,steps,u);
			y=get(simobj,'y');
			xtrue(:,:,i)=get(simobj,'x');
		else
			y=handles.y(:,:,rem(i-1,size(handles.y,3))+1);		
		end
		% Maybe we should change the output parameters of filter.m!
		filtobj=reset(filtobj,t);
		filter(filtobj,y,u,t);
		if xhat_mode
			xhat(:,:,i)=get(filtobj,'xhat');
		end
		if xpred_mode
			xpred(:,:,i)=get(filtobj,'xpred');
		end
		waitbar(i/runs,wbar);
	end

	handles.xhat=xhat;
	handles.xpred=xpred;
	handles.xtrue=xtrue;
	handles.steps=steps;
	handles.Ts=get(filtobj,'Ts');
	set(filtobj,'xhat',[]);		% Free some data
	set(filtobj,'xpred',[]);	% Free some data
	handles.filtobj=filtobj;
	
	guidata(hObject,handles);
	close(wbar);
end

if strcmp(action,'graph_button')
	pfgui_graph('init',hObject);
end

if strcmp(action,'rmse_button')
	pfgui_rmse('init',hObject);
end

if strcmp(action,'export_button')
	pfgui_export('init',hObject);
end

if strcmp(action,'t_edit')
	t_edit_str=get(handles.t_edit,'String');
	if isempty(t_edit_str)
		msgbox('Please enter a value, this field can not be blank!');
		return;
	end
	t_edit=str2num(t_edit_str);
	if t_edit<0
		msgbox('The time must not be negative!');
		return;
	end;
	handles.t=t_edit;
	set(handles.t_edit,'String',num2str(t_edit));	% Format data
	guidata(hObject,handles);
end

if strcmp(action,'u_edit')
	u_edit_str=get(handles.u_edit,'String');
	if isempty(u_edit_str)
		u=[];
		str='[No u(t)]';
	else
		u=evalin('base',u_edit_str,'[]');
		if isempty(u)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(u,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		else
			% Object imported successfully
			str=['[',num2str(size(u,1)),' x ',num2str(size(u,2)),']'];
		end
	end;
	handles.u=u;
	set(handles.u_text,'String',str);
	guidata(hObject,handles);

end



if strcmp(action,'filt_edit_button')
	filter_type_value=get(handles.filt_type_select,'value');
	filter_type=class(handles.filter_objects{filter_type_value});
	funcstr=[filter_type, '_edit'];
	feval(funcstr, 'init', hObject, handles.filtobj, 'filtobj', ['pfgui_action(''filter_refresh'', ', num2str(hObject), ', ''filtobj'',',num2str(filter_type_value),');']);
end

if strcmp(action,'filter_refresh')
	% Save the modified filter!
	filter_index=varargin{2};
	handles.filter_objects{filter_index}=handles.filtobj;
	guidata(hObject,handles);
end

if strcmp(action,'filt_type_select')
	
	filter_objects=handles.filter_objects;
	handles.filtobj=filter_objects{get(handles.filt_type_select,'value')};
	guidata(hObject,handles);

%	danl_menu_select(hObject);
end

if strcmp(action,'edit_runs')
	runs_str=get(handles.filt_runs_edit,'String');
	if isempty(runs_str)
		msgbox('Error! Blank field.');
		return;
	end;

	runs=str2num(runs_str);

	if runs<1
		msgbox('Error! Invalid value.');
		return;
	end;

	set(handles.filt_runs_edit,'String',num2str(runs));	% Format data

	handles.runs=runs;
	guidata(hObject,handles);
end

if strcmp(action,'simulation_steps')
	% MAKE A FUNCTION OF THIS ONE!!!
	steps_str=get(handles.sim_steps,'String');
	if isempty(steps_str)
		msgbox('Error! Blank field.');
		return;
	end;

	steps=str2num(steps_str);

	if steps<1
		msgbox('Error! Invalid value.');
		return;
	end;

	set(handles.sim_steps,'String',num2str(steps));	% Format data

	handles.simulation_steps=steps;
	guidata(hObject,handles);
end




if strcmp(action,'rmse_runs')
	rmse_runs_str=get(handles.rmse_runs_edit,'String');
	if isempty(rmse_runs_str)
		msgbox('Error! Blank field.');
		return;
	end;

	rmse_runs=str2num(rmse_runs_str);

	if rmse_runs<1
		msgbox('Error! Invalid value.');
		return;
	end;

	set(handles.rmse_runs_edit,'String',num2str(rmse_runs));	% Format data

	handles.rmse_runs=rmse_runs;
	guidata(hObject,handles);
end

if strcmp(action,'diskop_load')
	filename=get(handles.diskop_edit,'String');
	if isempty(filename)
		msgbox('Error! No filename is specified.');
		return;
	end;

	load(filename,'handles');
	guidata(hObject,handles);
	pfgui_refresh(hObject);
	msgbox('Guidata loaded');
end


if strcmp(action,'diskop_save')
	filename=get(handles.diskop_edit,'String');
	if isempty(filename)
		msgbox('Error! No filename is specified.');
		return;
	end;

	save(filename,'handles');

	msgbox('Guidata saved successfully');
end


if strcmp(action,'graph_states')
	graph_states_str=get(handles.graph_states_edit,'String');
	if isempty(graph_states_str)
		msgbox('Error! Blank field.');
		return;
	end;

	graph_states=str2num(graph_states_str);

	if graph_states<1
		msgbox('Error! Invalid value.');
		return;
	end;

	set(handles.graph_states_edit,'String',num2str(graph_states));	% Format data

	handles.graph_states=graph_states;
	guidata(hObject,handles);
end


if strcmp(action,'animate_axes')
	animate_axes_str=get(handles.animate_axes_edit,'String');
	if isempty(animate_axes_str)
		msgbox('Error! Blank field.');
		return;
	end;

	animate_axes=str2num(animate_axes_str);

	set(handles.animate_axes_edit,'String',num2str2(animate_axes));	% Format data

	handles.animate_axes=animate_axes;
	guidata(hObject,handles);
	pfgui_animate_text(hObject);
end

if strcmp(action,'animate_states')
	animate_states_str=get(handles.animate_states_edit,'String');
	if isempty(animate_states_str)
		msgbox('Error! Blank field.');
		return;
	end;

	animate_states=str2num(animate_states_str);

	set(handles.animate_states_edit,'String',num2str2(animate_states));	% Format data

	handles.animate_states=animate_states;
	guidata(hObject,handles);
	pfgui_animate_text(hObject);
end

if strcmp(action,'models_button')
	pfgui_models('init',hObject,['pfgui_action(''models_refresh'', ', num2str(hObject), ');']);
end

if strcmp(action,'models_refresh')
	msgbox('FRESH!');
end
