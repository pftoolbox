function editmatrix(A,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

if isa(A,'double');
	hf=figure('Name','Edit Matrix','NumberTitle','off','Units','Characters');
	m=size(A,1);
	n=size(A,2);
	uicontrol('Style','Edit','String',num2str(m),'Units','Characters','Tag','rows','Position',[0,0.5,5,1.5]);
	uicontrol('Style','Text','String','x','Units','Characters','Position',[5,0.5,5,1.5]);
	uicontrol('Style','Edit','String',num2str(n),'Units','Characters','Tag','columns','Position',[10,0.5,5,1.5]);
	uicontrol('Style','Pushbutton','String','Resize/Clear','Units','Characters','Position',[20,0.5,20,1.5],'Callback',['editmatrix(''resize'',',num2str(hf),')']);
	uicontrol('Style','Pushbutton','String','Save','Units','Characters','Position',[50,0.5,20,1.5],'Callback',['editmatrix(''save'',',num2str(hf),')']);
	uicontrol('Style','Pushbutton','String','Cancel','Units','Characters','Position',[70,0.5,20,1.5],'Callback',['delete(',num2str(hf),')']);
	uicontrol('Style','Frame','Units','Characters','Position',[0,3,90,0.1]);
elseif strcmp(A,'resize')
	hf=varargin{1};
	handles=guidata(hf);

	A=handles.matrix;
	m=size(A,1);
	n=size(A,2);

	% Delete edit controls
	for j=1:m
		for i=1:n
			delete(handles.(['edit',num2str(j),'_',num2str(i)]));
		end
	end

	% Generate new values
	m=str2num(get(handles.rows,'string'));
	n=str2num(get(handles.columns,'string'));
	A=zeros(m,n);
end;

% Generate the edit matrix
for j=1:m
	for i=1:n
		uicontrol('Style','Edit','String',num2str(A(j,i)),'Units','Characters','Tag',['edit',num2str(j),'_',num2str(i)],'Position',[10*(i-1),3*(m-j)+0.5+4,7,1.5]);
	end
end

% Save data
handles=guihandles(hf);
handles.matrix=A;
guidata(hf,handles);

