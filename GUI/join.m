function B=join(A,B);
% Merges two matrixes A and B. The contents of the A matrix is copied into the B matrix, which
% is returned with the same dimensions as before.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

am=size(A,1);
an=size(A,2);
bm=size(B,1);
bn=size(B,2);

if am>bm
	% A contains more rows than B
	if an>bn
		% A contains more columns than B
		B(1:bm,1:bn)=A(1:bm,1:bn);
	else
		% A contains less columns than B
		B(1:bm,1:an)=A(1:bm,1:an);
	end
else
	% A contains less rows than B
	if an>bn
		% A contains more columns than B
		B(1:am,1:bn)=A(1:am,1:bn);
	else
		% A contains less columns than B
		B(1:am,1:an)=A(1:am,1:an);
	end
end
