function pfgui_rmse(action,hObject,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init')
	parenthandles=guidata(hObject);

	if isempty(parenthandles.xhat)&&isempty(parenthandles.xpred)
		msgbox('No filtered data available!');
		return;
	end

	names={'xhat','xpred','xtrue','Ts'};
	descriptions={'filtered data','predicted data','true data','sample points'};

	hf=figure('Name','Export command window');

	pos=0;
	for i=1:length(names);
		if ~isempty(parenthandles.(names{i}))
			uicontrol(hf,'Style','Text','String','Name in workspace:','Units','normalized','Position',[0.01 0.91+pos 0.3 0.05],'HorizontalAlign','Left','FontName',myfont);
			uicontrol(hf,'Style','Edit','String','','Units','normalized','Position',[0.31 0.91+pos 0.3 0.05],'Tag',['edit_',names{i}],'HorizontalAlign','Left','FontName',myfont);
			uicontrol(hf,'Style','Pushbutton','String',['Export ',descriptions{i}],'Units','normalized','Position',[0.65 0.91+pos 0.34 0.05],'Callback',sprintf('pfgui_export(''export'',%g,%g,%d)',hObject,hf,i),'HorizontalAlign','Left','FontName',myfont);
			pos=pos-0.1;
		end
	end
	
%	uicontrol(hf,'Style','Listbox','String',cell2str(lnames),'Units','normalized','Position',[0.05 0.5 0.9 0.4],'Tag','state_select','HorizontalAlign','Left','Min',1,'Max',3,'Value',1:length(xvars),'FontName',myfont);

%	uicontrol(hf,'Style','Pushbutton','String','RMSE!','Units','normalized','Position',[0.05 0.1 0.9 0.05],'callback',sprintf('pfgui_rmse(''rmse'',%g,%g)',hObject,hf),'HorizontalAlign','Left','FontName',myfont);

	handles=guihandles(hf);
	guidata(hf,handles);


elseif strcmp(action,'export');
	hf=varargin{1};
	parenthandles=guidata(hObject);
	handles=guidata(hf);
	command=varargin{2};

	names={'xhat','xpred','xtrue','Ts'};

	str=get(handles.(['edit_',names{command}]),'String');	
	if isempty(str)
		msgbox('The name must not be empty!');
		return;
	end

	assignin('base',str,parenthandles.(names{command}));
	msgbox('If no error messages occured in the workspace, the export was completed successfully');
	
end
