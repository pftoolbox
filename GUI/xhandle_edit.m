function xsymbolic_edit(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%myfont='default';
myfont='FixedWidth';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	expression=obj.expression;
	caller=varargin{3};
	callback=varargin{4};

	gradx=obj.gradx;
	gradw=obj.gradw;

	hf=figure('Name','xhandle object editor');

	pos=0.1;
	space=0.04;

	uicontrol(hf,'Style','Text','String','Expression handle:','Units','normalized','Position',[0.0 0.8 0.4 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',obj.str,'Units','normalized','Position',[0.4 0.8 0.5 0.05],'Tag','expression_edit','Callback',sprintf('xhandle_edit(''expression_edit'',%g)',hf),'HorizontalAlign','Left','Fontname',myfont);

	uicontrol(hf,'Style','Text','String','gradx handle (for EKF):','Units','normalized','Position',[0.0 0.72 0.4 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',func2str2(obj.gradx),'Units','normalized','Position',[0.4 0.72 0.5 0.05],'Tag','gradx_edit','Callback',sprintf('xhandle_edit(''gradx_edit'',%g)',hf),'HorizontalAlign','Left','Fontname',myfont);

	uicontrol(hf,'Style','Text','String',sprintf('grad%s handle (for EKF):',obj.wchar),'Units','normalized','Position',[0.0 0.64 0.4 0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',func2str2(obj.gradw),'Units','normalized','Position',[0.4 0.64 0.5 0.05],'Tag','gradw_edit','Callback',sprintf('xhandle_edit(''gradw_edit'',%g)',hf),'HorizontalAlign','Left','Fontname',myfont);

	% Save/Cancel
	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.7,0.56,0.1,0.05],'Callback',['xhandle_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.8,0.56,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

	helptext=sprintf('The functions are called with the following arguments: x, t, u, %s',obj.wchar);
	uicontrol(hf,'Style','Text','String',helptext,'Units','normalized','Position',[0.0 0.4 1 0.05],'HorizontalAlign','Left','Min',1,'Max',3,'Fontname',myfont);

	handles=guihandles(hf);
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	handles.expression=expression;
	handles.gradx=gradx;
	handles.gradw=gradw;
	guidata(hf,handles);


elseif strcmp(action,'expression_edit')
	hf=varargin{1};
	handles=guidata(hf);

	expr_str=get(handles.expression_edit,'String');
	expr=str2func2(expr_str);
	set(handles.expression_edit,'String',func2str2(expr));	% Format
	
	handles.expression=expr;
	guidata(hf,handles);

elseif strcmp(action,'gradx_edit')
	hf=varargin{1};
	handles=guidata(hf);

	grad_str=get(handles.gradx_edit,'String');
	grad=str2func2(grad_str);
	set(handles.gradx_edit,'String',func2str2(grad));	% Format
	
	handles.gradx=grad;
	guidata(hf,handles);

elseif strcmp(action,'gradw_edit')
	hf=varargin{1};
	handles=guidata(hf);

	grad_str=get(handles.gradw_edit,'String');
	grad=str2func2(grad_str);
	set(handles.gradw_edit,'String',func2str2(grad));	% Format
	
	handles.gradw=grad;
	guidata(hf,handles);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	if isempty(handles.expression)
		msgbox('The expression field must not be empty!');
		return;
	end

	caller=handles.caller;
	obj=handles.obj;

	set(obj,'expression',handles.expression);
	set(obj,'gradx',handles.gradx);
	set(obj,'gradw',handles.gradw);

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.(caller)=obj;
	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

end

