function expr=str2expr(str);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

quotes=strfind(str, '''');
quotecount=length(quotes);
if rem(quotecount,2)
	% The number of quotes must be even
	error('Invalid expression!');
end

exprcount=quotecount/2;
expr=cell(1,exprcount);

j=1;
for i=1:exprcount
	expr{i}=str(quotes(j)+1:quotes(j+1)-1);
	j=j+2;
end
