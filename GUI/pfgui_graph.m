function pfgui_graph(action,hObject,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';

if strcmp(action,'init')
	parenthandles=guidata(hObject);

	if isempty(parenthandles.xhat)&&isempty(parenthandles.xpred)
		msgbox('No filtered data available!');
		return;
	end

	xhat_length=length(parenthandles.xhat);
	xpred_length=length(parenthandles.xpred);

%	xvars=parenthandles.filtobj.model.xvars;
	xvars=get(get(parenthandles.filtobj,'model'),'xvars');

	if xhat_length&&xpred_length
		lnames=cell(1,2*length(xvars));
		for i=1:length(xvars);
			lnames{i}=[xvars{i},' [xhat]'];
		end;
		for i=1:length(xvars);
			lnames{i+length(xvars)}=[xvars{i},' [xpred]'];
		end;
		lindex=[1:length(xvars), 1:length(xvars)];
		larray=[repmat({'xhat'},1,length(xvars)), repmat({'xpred'},1,length(xvars))];
		lsimnames=[xvars xvars];
	elseif xhat_length
		lnames=cell(1,length(xvars));
		lnames=xvars;
		lindex=[1:length(xvars)];
		larray=repmat({'xhat'},1,length(xvars));
		lsimnames=xvars;
	else
		lnames=cell(1,length(xvars));
		lnames=xvars;
		lindex=[1:length(xvars)];
		larray=repmat({'xpred'},1,length(xvars));
		lsimnames=xvars;
	end

	hf=figure('Name','Graph command window');
	uicontrol(hf,'Style','Text','String','Select the state(s) to plot:','Units','normalized','Position',[0.05 0.91 0.9 0.05],'HorizontalAlign','Left','Min',1,'Max',3,'FontName',myfont);

	uicontrol(hf,'Style','Listbox','String',cell2str(lnames),'Units','normalized','Position',[0.05 0.5 0.9 0.4],'Tag','state_select','HorizontalAlign','Left','Min',1,'Max',3,'FontName','FixedWidth');

	uicontrol(hf,'Style','Radiobutton','String','Plot a specific run:','Units','normalized','Position',[0.05 0.3 0.35 0.05],'Tag','runs_check_1','callback',sprintf('pfgui_graph(''runs_check_1'',%g,%g)',hObject,hf),'Value',1,'HorizontalAlign','Left','FontName',myfont);
	uicontrol(hf,'Style','Radiobutton','String','Plot the average','Units','normalized','Position',[0.05 0.22 0.35 0.05],'Tag','runs_check_2','callback',sprintf('pfgui_graph(''runs_check_2'',%g,%g)',hObject,hf),'Value',0,'HorizontalAlign','Left','FontName',myfont);

	uicontrol(hf,'Style','Edit','String','1','Units','normalized','Position',[0.5 0.3 0.2 0.05],'Tag','runs_edit','callback',sprintf('pfgui_graph(''run_edit'',%g,%g)',hObject,hf),'HorizontalAlign','Left','FontName',myfont,'Enable','on');
	uicontrol(hf,'Style','Text','String',sprintf('Max: %d',max(size(parenthandles.xhat,3),size(parenthandles.xpred,3))),'Units','normalized','Position',[0.75 0.3 0.2 0.05],'HorizontalAlign','Left','FontName',myfont);


	uicontrol(hf,'Style','Pushbutton','String','PLOT!','Units','normalized','Position',[0.05 0.1 0.26 0.05],'callback',sprintf('pfgui_graph(''graph'',%g,%g)',hObject,hf),'HorizontalAlign','Left','FontName',myfont);





	handles=guihandles(hf);
	handles.lnames=lnames;
	handles.lindex=lindex;
	handles.larray=larray;
	handles.lsimnames=lsimnames;
	handles.run=1;
	handles.average_mode=0;
	guidata(hf,handles);

elseif strcmp(action,'runs_check_1');
	hf=varargin{1};
	handles=guidata(hf);

	set(handles.runs_check_1,'Value',1);
	set(handles.runs_check_2,'Value',0);
	set(handles.runs_edit,'Enable','on');

	handles.average_mode=0;
	guidata(hf,handles);

elseif strcmp(action,'runs_check_2');
	hf=varargin{1};
	handles=guidata(hf);

	set(handles.runs_check_2,'Value',1);
	set(handles.runs_check_1,'Value',0);
	set(handles.runs_edit,'Enable','off');

	handles.average_mode=1;
	guidata(hf,handles);

elseif strcmp(action,'run_edit');
	hf=varargin{1};
	handles=guidata(hf);
	parenthandles=guidata(hObject);

	runs_str=get(handles.runs_edit,'String');
	if isempty(runs_str)
		msgbox('Error! Blank field.');
		return;
	end;

	runs=str2num(runs_str);

	if runs<1||runs>max(size(parenthandles.xhat,3),size(parenthandles.xpred,3))
		msgbox('Error! Invalid value.');
		return;
	end;

	set(handles.runs_edit,'String',num2str(runs));	% Format data

	handles.run=runs;
	guidata(hf,handles);

elseif strcmp(action,'graph');
	hf=varargin{1};
	parenthandles=guidata(hObject);
	handles=guidata(hf);
	
	states=get(handles.state_select,'value');
	if isempty(states)
		msgbox('Please select at least one state');
		return;
	end;

	Ts=parenthandles.filtobj.Ts;

	figure('Name','Graph data');


	plotstr='plot(';
	simnames={};

	if handles.average_mode
		xhat_avg=mean(parenthandles.xhat,3);
		xpred_avg=mean(parenthandles.xpred,3);
		xtrue_avg=mean(parenthandles.xtrue,3);
			
		if ~isempty(parenthandles.xtrue)
			simnames={};
			simmedindices=[];
			for i=1:length(states)
				if isempty(find(simmedindices==handles.lindex(states(i))));
					% Not yet plotted
					simnames{end+1}=[handles.lsimnames{states(i)},' [true]'];
					plotstr=[plotstr,sprintf('Ts,xtrue_avg(%d,:),',handles.lindex(states(i)))];
					simmedindices(end+1)=handles.lindex(states(i));
				end
			end
		end

		for i=1:length(states)
			if strcmp(handles.larray{states(i)},'xhat')
				plotstr=[plotstr,sprintf('Ts,xhat_avg(%d,:),',handles.lindex(states(i)))];
			else
				plotstr=[plotstr,sprintf('Ts,xpred_avg(%d,:),',handles.lindex(states(i)))];
			end
		end
	else
		if ~isempty(parenthandles.xtrue)
			simnames={};
			simmedindices=[];
			for i=1:length(states)
				if isempty(find(simmedindices==handles.lindex(states(i))));
					% Not yet plotted
					simnames{end+1}=[handles.lsimnames{states(i)},' [true]'];
					plotstr=[plotstr,sprintf('Ts,parenthandles.xtrue(%d,:,%d),',handles.lindex(states(i)),rem(handles.run-1,size(parenthandles.xtrue,3))+1)];
					simmedindices(end+1)=handles.lindex(states(i));
				end
			end
		end

		for i=1:length(states)
			if strcmp(handles.larray{states(i)},'xhat')
				plotstr=[plotstr,sprintf('Ts,parenthandles.xhat(%d,:,%d),',handles.lindex(states(i)),handles.run)];
			else
				plotstr=[plotstr,sprintf('Ts,parenthandles.xpred(%d,:,%d),',handles.lindex(states(i)),handles.run)];
			end
		end

	end


	plotstr=[plotstr(1:end-1),')'];
	eval(plotstr);		% PLOT!!!

	legend(simnames{:},handles.lnames{states})
	
end
