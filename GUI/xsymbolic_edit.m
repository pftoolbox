function xsymbolic_edit(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

myfont='default';
%myfont='FixedWidth';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	expression=obj.expression;
	caller=varargin{3};
	callback=varargin{4};

	% Make the GUI show the correct information
	if strcmp(caller,'h')||strcmp(caller,'hu')
		if strcmp(obj.wchar,'w')
			set(obj,'wchar','e');
		end
	else
		if strcmp(obj.wchar,'e')
			set(obj,'wchar','w');
		end
	end	
	
%	hf=figure('Name','xsymbolic object editor','NumberTitle','off','Units','Characters');
	hf=figure('Name','xsymbolic object editor');

	pos=0.1;
	space=0.04;

	parenthandles=guidata(parentfig);
	eval(sprintf('wvars=parenthandles.%svars;',obj.wchar),'wvars=[];');
	if isa(wvars,'double')
		eval('wvars=parenthandles.wvars;','wvars=[];');
	end

	eval('uvars=parenthandles.uvars;','uvars=[];');
	xvars=parenthandles.xvars;

%	wvars=obj.wvars;
%	uvars=obj.uvars;
%	xvars=obj.xvars;
	
	if ~isempty(wvars)
		str=['  '];
		for i=1:length(wvars)
			str=[str, wvars{i}, ', '];
		end;
		str=str(1:end-2);
		uicontrol(hf,'Style','Text','String',sprintf('%s(t) names [%svars]: ',obj.wchar,obj.wchar),'Units','normalized','Position',[0.0 pos 0.31 0.04],'HorizontalAlignment','Right','Fontname',myfont);
		uicontrol(hf,'Style','Text','String',str,'Units','normalized','Position',[0.31 pos 0.68 0.04],'HorizontalAlignment','Left','Fontname',myfont);
		pos=pos+space;
	end
	
	if ~isempty(uvars)
		str=['  '];
		for i=1:length(uvars)
			str=[str, uvars{i}, ', '];
		end;
		str=str(1:end-2);
		uicontrol(hf,'Style','Text','String','u(t) names [uvars]: ','Units','normalized','Position',[0.0 pos 0.31 0.04],'HorizontalAlignment','Right','Fontname',myfont);
		uicontrol(hf,'Style','Text','String',str,'Units','normalized','Position',[0.31 pos 0.68 0.04],'HorizontalAlignment','Left','Fontname',myfont);
		pos=pos+space;
	end

	if ~isempty(xvars)
		str=['  '];
		for i=1:length(xvars)
			str=[str, xvars{i}, ', '];
		end;
		str=str(1:end-2);
		uicontrol(hf,'Style','Text','String','State names [xvars]: ','Units','normalized','Position',[0.0 pos 0.31 0.04],'HorizontalAlignment','Right','Fontname',myfont);
		uicontrol(hf,'Style','Text','String',str,'Units','normalized','Position',[0.31 pos 0.68 0.04],'HorizontalAlignment','Left','Fontname',myfont);
		pos=pos+space;
	end


	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.0 0.1 1 0.003]);
	uicontrol(hf,'Style','Text','String','Rows/subexpressions','Units','normalized','Position',[0.0 0.02 0.28 0.04],'Fontname',myfont);
	uicontrol(hf,'Style','Edit','String',num2str(length(expression)),'Units','normalized','Position',[0.29 0.02 0.1 0.05],'callback',['xsymbolic_edit(''changenumber'',' ,num2str(hf), ')'],'Tag','edit_number','Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['xsymbolic_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);

	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.41 0.0 0.002 0.1]);
	uicontrol(hf,'Style','Frame','Units','normalized','Position',[0.78 0.0 0.002 0.1]);

	uicontrol(hf,'Style','Text','String','Edit:','Units','normalized','Position',[0.43 0.02 0.12 0.04],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String',sprintf('grad%s',obj.wchar),'Units','normalized','Position',[0.66,0.02,0.1,0.05],'Callback',['xsymbolic_edit(''gradw'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','gradx','Units','normalized','Position',[0.56,0.02,0.1,0.05],'Callback',['xsymbolic_edit(''gradx'',',num2str(hf),')'],'Fontname',myfont);


	handles=guihandles(hf);
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	handles.expression=expression;
	guidata(hf,handles);

	xsymbolic_edit('updatestates',hf);

elseif strcmp(action,'gradx')
	hf=varargin{1};
	if xsymbolic_expr2obj(hf)
		handles=guidata(hf);
		obj=handles.obj;
		if isempty(obj.xvars)
			msgbox('There are no named x(t) elements!');
			return
		end;
		obj=initgradx(obj);
%		get(obj,'gradx')
		handles.obj=obj;
		guidata(hf,handles);
%		msgbox('Not yet implemented! Check the Matlab command window for the automatically generated expression.');
		xsymbolic_editgrad('init',hf,get(obj,'gradx'),'gradx',sprintf('xsymbolic_edit(''grad_refresh'',%g)',hf));
	end
elseif strcmp(action,'gradw')
	hf=varargin{1};
	if xsymbolic_expr2obj(hf)
		handles=guidata(hf);
		obj=handles.obj;
		if isempty(obj.wvars)
			msgbox(sprintf('There are no named %s(t) elements!',obj.wchar));
			return
		end;
		obj=initgradw(obj);
%		get(obj,'gradw')
		handles.obj=obj;
		guidata(hf,handles);
%		msgbox('Not yet implemented! Check the Matlab command window for the automatically generated expression.');
		xsymbolic_editgrad('init',hf,get(obj,'gradw'),'gradw',sprintf('xsymbolic_edit(''grad_refresh'',%g)',hf));
	end
elseif strcmp(action,'save')

	hf=varargin{1};
	if xsymbolic_expr2obj(hf)

		handles=guidata(hf);

		caller=handles.caller;
		obj=handles.obj;

		parentfig=handles.parentfig;
		parenthandles=guidata(parentfig);

		parenthandles.(caller)=obj;
		guidata(parentfig,parenthandles);	% Save data
	
		eval(handles.callback);
		close(hf)
	end

elseif strcmp(action,'edit_expression')
	hf=varargin{1};
	caller=varargin{2};
	handles=guidata(hf);
	expression=handles.expression;
	expr=get(handles.(['name_',num2str(caller)]),'String');
	if isempty(expr)
		msgbox('No empty subexpressions are allowed');
		return;
	end;
	expression{caller}=expr;
	handles.expression=expression;
	guidata(hf,handles);
elseif strcmp(action,'updatestates')

	hf=varargin{1};
	handles=guidata(hf);	

	expression=handles.expression;

	exprnr=length(expression);
	xvars=expression;
%	x0=[1 2 3];


	% Remove old controls
	i=1;
	while isfield(handles,['number_',num2str(i)])
		delete(handles.(['number_',num2str(i)]));
		delete(handles.(['name_',num2str(i)]));
%		delete(handles.(['init_',num2str(i)]));
		handles=rmfield(handles,['number_',num2str(i)]);
		handles=rmfield(handles,['name_',num2str(i)]);
%		handles=rmfield(handles,['init_',num2str(i)]);
		i=i+1;
	end;	

	postop=0.92;
	space=0.07;
	pos=0;

	% Add new controls
	for i=1:exprnr
		str=xvars{i};
		handles.(['number_',num2str(i)])=uicontrol(hf,'Style','Text','String',num2str(i),'Units','normalized','Position',[0.0 postop-space*pos 0.05 0.05],'Fontname',myfont);
		handles.(['name_',num2str(i)])=uicontrol(hf,'Style','Edit','String',str,'Units','normalized','Position',[0.1 postop-space*pos 0.8 0.05],'Callback',['xsymbolic_edit(''edit_expression'',',num2str(hf),' ,',num2str(i),')'],'Fontname',myfont);
		pos=pos+1;
	end

	guidata(hf,handles);

elseif strcmp(action,'changenumber')

	hf=varargin{1};
	handles=guidata(hf);	
	expression=handles.expression;
	exprnr_old=length(expression);
	exprnr=str2num(get(handles.edit_number,'String'));
	sdiff=exprnr-exprnr_old;


	if sdiff>0
		% Add more subexpressions
		for i=exprnr_old+1:exprnr
			expression{i}='';
		end;
	elseif sdiff<0
		% Delete some subexpressions
		expression=expression(1:exprnr);
	else
		% Nothing has changed. Do nothing.
		return;
	end
	
	handles.expression=expression;	
	guidata(hf,handles);

	xsymbolic_edit('updatestates',hf);

end

