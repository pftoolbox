function danl_action_update_select(hf, caller, types, str_name)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hf);

select_property=[caller,'_select'];
edit_property=[caller,'_edit'];

datatypes=handles.(types);
data=handles.(caller);

index=0;
for i=1:length(datatypes)
	if isa(data,datatypes{i})
		index=i;
		break;
	end;
end;

set(handles.(select_property),'Value',index+1);
if ~isempty(data); str=get(data,str_name); else; str=[]; end;
set(handles.(edit_property),'String',str);

guidata(hf,handles);
