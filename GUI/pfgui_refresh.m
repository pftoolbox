function pfgui_refresh(hObject);
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hObject);
handles

if handles.simulator_mode
	pfgui_action('sim_radio_1',hObject);
else
	pfgui_action('sim_radio_2',hObject);
end	

%	set(handles.u_edit,handles.u
	pfgui_action('u_edit

%	hmhm... min id� kommer ej att funka!!


%handles.filter_objects=filter_objects;
%handles.filtobj=filter_objects{1};
%handles.t=0; 
%handles.u=[];
%handles.y=[];
%handles.xtrue=[];
%handles.xhat=[];
%handles.xpred=[];
%handles.Ts=[];
%handles.runs=runs;
%handles.steps=[];
%handles.simulation_steps=simulation_steps;
