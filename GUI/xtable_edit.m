function xtable_edit(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%myfont='FixedWidth';
myfont='default';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	caller=varargin{3};
	callback=varargin{4};

	% Make the GUI show the correct information
	if strcmp(caller,'h')||strcmp(caller,'hu')
		if strcmp(obj.wchar,'w')
			set(obj,'wchar','e');
		end
	else
		if strcmp(obj.wchar,'e')
			set(obj,'wchar','w');
		end
	end	

	array=obj.array;

	interpolate=obj.interpolate;
	Ts=obj.Ts;
	gradx=obj.gradx;
	gradw=obj.gradw;
	
	if isempty(Ts)
		t_first='None';
		t_last='None';
	else
		t_first=num2str(Ts(1));
		t_last=num2str(Ts(end));
	end;

	hf=figure('Name','xtable object editor');

	uicontrol('Style','Text','String','3d expression matrix:','Units','normalized','Position',[0.0,0.90,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Edit','String','','Units','normalized','Position',[0.4,0.90,0.4,0.05],'Tag','expression_edit','Callback',['xtable_edit(''expression_import'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Import','Units','normalized','Position',[0.85,0.90,0.15,0.05],'Callback',['xtable_edit(''expression_import'',',num2str(hf),')'],'Fontname',myfont);

	uicontrol('Style','Text','String','Sample point vector:','Units','normalized','Position',[0.0,0.82,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Edit','String','','Units','normalized','Position',[0.4,0.82,0.4,0.05],'Tag','Ts_edit','Callback',['xtable_edit(''Ts_import'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Import','Units','normalized','Position',[0.85,0.82,0.15,0.05],'Callback',['xtable_edit(''Ts_import'',',num2str(hf),')'],'Fontname',myfont);

	uicontrol('Style','Text','String','gradx (for EKF):','Units','normalized','Position',[0.0,0.74,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Edit','String','','Units','normalized','Position',[0.4,0.74,0.4,0.05],'Tag','gradx_edit','Callback',['xtable_edit(''gradx_import'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Import','Units','normalized','Position',[0.85,0.74,0.15,0.05],'Callback',['xtable_edit(''gradx_import'',',num2str(hf),')'],'Fontname',myfont);

	uicontrol('Style','Text','String',sprintf('grad%s (for EKF):',obj.wchar),'Units','normalized','Position',[0.0,0.66,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Edit','String','','Units','normalized','Position',[0.4,0.66,0.4,0.05],'Tag','gradw_edit','Callback',['xtable_edit(''gradw_import'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Import','Units','normalized','Position',[0.85,0.66,0.15,0.05],'Callback',['xtable_edit(''gradw_import'',',num2str(hf),')'],'Fontname',myfont);


	if isempty(array)
		n=0;
	else
		n=size(array,3);
	end

	if isempty(gradx)
		gxn='None';
	else
		gxn=sprintf('%d x %d x %d',size(gradx,1),size(gradx,2),size(gradx,3));
	end

	if isempty(gradw)
		gwn='None';
	else
		gwn=sprintf('%d x %d x %d',size(gradw,1),size(gradw,2),size(gradw,3));
	end


	p=-0.1;
	uicontrol('Style','Text','String','Expression size:','Units','normalized','Position',[0.0,p+0.60,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',sprintf('%d x %d',size(array,1),size(array,2)),'Units','normalized','Position',[0.4,p+0.60,0.4,0.05],'Tag','expression_size','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','Number of expressions:','Units','normalized','Position',[0.0,p+0.55,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',num2str(n),'Units','normalized','Position',[0.4,p+0.55,0.4,0.05],'Tag','expression_number','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','Number of sample points:','Units','normalized','Position',[0.0,p+0.5,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',num2str(length(Ts)),'Units','normalized','Position',[0.4,p+0.5,0.4,0.05],'Tag','Ts_number','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','First sample point:','Units','normalized','Position',[0.0,p+0.45,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',t_first,'Units','normalized','Position',[0.4,p+0.45,0.4,0.05],'Tag','Ts_first','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','Last sample point:','Units','normalized','Position',[0.0,p+0.4,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',t_last,'Units','normalized','Position',[0.4,p+0.4,0.4,0.05],'Tag','Ts_last','HorizontalAlign','Left','Fontname',myfont);

	uicontrol('Style','Text','String','gradx size:','Units','normalized','Position',[0.0,0.2,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',gxn,'Units','normalized','Position',[0.4,0.2,0.4,0.05],'Tag','gradx_size','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',sprintf('grad%s size:',obj.wchar),'Units','normalized','Position',[0.0,0.15,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',gwn,'Units','normalized','Position',[0.4,0.15,0.4,0.05],'Tag','gradw_size','HorizontalAlign','Left','Fontname',myfont);

	uicontrol('Style','Text','String','Interpolation:','Units','normalized','Position',[0.0,0.02,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Radiobutton','String','None','Value',~interpolate,'Units','normalized','Position',[0.4,0.02,0.2,0.05],'Tag','radio1','Callback',['xtable_edit(''radio1'',',num2str(hf),')'],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Radiobutton','String','Linear','Value',interpolate,'Units','normalized','Position',[0.6,0.02,0.2,0.05],'Tag','radio2','Callback',['xtable_edit(''radio2'',',num2str(hf),')'],'HorizontalAlign','Left','Fontname',myfont);

	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['xtable_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);


	handles=guihandles(hf);
	handles.array=array;
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	handles.interpolate=interpolate;
	handles.Ts=Ts;
	handles.gradx=gradx;
	handles.gradw=gradw;
	handles.wchar=obj.wchar;
	guidata(hf,handles);

	xtable_edit('check',hf);


elseif strcmp(action,'radio1')
	hf=varargin{1};
	handles=guidata(hf);
	set(handles.radio1,'value',1);
	set(handles.radio2,'value',0);
	handles.interpolate=false;
	guidata(hf,handles);
	
elseif strcmp(action,'radio2')
	hf=varargin{1};
	handles=guidata(hf);
	set(handles.radio2,'value',1);
	set(handles.radio1,'value',0);
	handles.interpolate=true;
	guidata(hf,handles);
	
elseif strcmp(action,'expression_import')
	hf=varargin{1};
	handles=guidata(hf);
	expr_str=get(handles.expression_edit,'String');
	if isempty(expr_str)
		msgbox('Error! Empty field');
		return;
	else
		array=evalin('base',expr_str,'[]');
		if isempty(array)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(array,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		elseif ndims(array)~=3
			msgbox('The data array must be three-dimensional!');
			return;
		else
			set(handles.expression_size,'String',sprintf('%d x %d',size(array,1),size(array,2)));
			set(handles.expression_number,'String',num2str(size(array,3)));
		end
	end;
	handles.array=array;
	guidata(hf,handles);

elseif strcmp(action,'Ts_import')
	hf=varargin{1};
	handles=guidata(hf);
	expr_str=get(handles.Ts_edit,'String');
	if isempty(expr_str)
		msgbox('Error! Empty field');
		return;
	else
		Ts=evalin('base',expr_str,'[]');
		if isempty(Ts)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(Ts,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		elseif min(size(Ts,1),size(Ts,2))~=1||ndims(Ts)>2
			msgbox('The data matrix must be one-dimensional!');
			return;
		else
			set(handles.Ts_number,'String',num2str(length(Ts)));
			set(handles.Ts_first,'String',num2str(Ts(1)));
			set(handles.Ts_last,'String',num2str(Ts(end)));
		end
	end;
	handles.Ts=Ts;
	guidata(hf,handles);

elseif strcmp(action,'gradx_import')
	hf=varargin{1};
	handles=guidata(hf);
	expr_str=get(handles.gradx_edit,'String');
	if isempty(expr_str)
		grad=[];
	else
		grad=evalin('base',expr_str,'[]');
		if isempty(grad)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(grad,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		elseif ndims(grad)~=3
			msgbox('The gradient array must be three-dimensional!');
			return;
		end
	end;

	if isempty(grad)
		gxn='None';
	else
		gxn=sprintf('%d x %d x %d',size(grad,1),size(grad,2),size(grad,3));
	end

	set(handles.gradx_size,'String',gxn);
	handles.gradx=grad;
	guidata(hf,handles);

elseif strcmp(action,'gradw_import')
	hf=varargin{1};
	handles=guidata(hf);
	expr_str=get(handles.gradw_edit,'String');
	if isempty(expr_str)
		grad=[];
	else
		grad=evalin('base',expr_str,'[]');
		if isempty(grad)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(grad,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		elseif ndims(grad)~=3
			msgbox('The gradient array must be three-dimensional!');
			return;
		end
	end;

	if isempty(grad)
		gwn='None';
	else
		gwn=sprintf('%d x %d x %d',size(grad,1),size(grad,2),size(grad,3));
	end

	set(handles.gradw_size,'String',gwn);
	handles.gradw=grad;
	guidata(hf,handles);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	caller=handles.caller;
	obj=handles.obj;

	if isempty(handles.array)
		msgbox('The expression array must not be empty!');
		return;
	end;

	if isempty(handles.Ts)
		msgbox('The sample points vector must not be empty!');
		return;
	end;

	if length(handles.Ts)~=size(handles.array,3)
		msgbox('Error! The number of expressions and the number of sample points must be equal!');
		return;
	end

	if ~isempty(handles.gradx)
		if sum(size(handles.gradx)~=size(handles.array))
			msgbox('Error! The gradients need to have the same size as the data array.');
			return;
		end
	end

	if ~isempty(handles.gradw)
		if sum(size(handles.gradw)~=size(handles.array))
			msgbox('Error! The gradients need to have the same size as the data array.');
			return;
		end
	end


%	if handles.interpolation&&size(array,3)<2
%		msgbox('The number of expressions must be at least 2 in interpolation mode.');
%	end

	set(obj,'array',handles.array);
	set(obj,'Ts',handles.Ts);
	set(obj,'interpolate',handles.interpolate);
	set(obj,'gradx',handles.gradx);
	set(obj,'gradw',handles.gradw);

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.(caller)=obj;
	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

end;
