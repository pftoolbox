function dgnl_action(hObject,action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

handles=guidata(hObject);



if strcmp(action,'save')

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	if isempty(handles.h_inv)
		msgbox('h_inv must be defined!');
		return
	end;

	% Check for empty objects
	needed_objects={'f','h','w','e','p0'};
	for i=1:length(needed_objects);	
		if isempty(handles.(needed_objects{i}))
			msgbox([needed_objects{i},' must not be undefined!']);
			return;
		end
	end

	% Assign the saved properties to local variables
	obj=handles.obj;
	props={'f','h','w','e','p0','xvars','uvars','wvars','evars','x0','T'};
	for i=1:length(props)
		eval([props{i},'=handles.',props{i},';']);
	end

	% Insert xvars into the data objects
	set(f,'xvars',xvars);
	set(f,'uvars',uvars);
	set(f,'wvars',wvars);
	set(h,'xvars',xvars);
	set(h,'uvars',uvars);
	set(h,'wvars',evars);

	% Fix the correct varsize
	varsize=[handles.states, 1, parenthandles.usize, get(handles.w,'n')];
	eval(['set(f, ''varsize'', varsize);'], ';');	% Try / Catch
	varsize=[handles.states, 1, parenthandles.usize, get(handles.e,'n')];
	eval(['set(h, ''varsize'', varsize);'], ';');	% Try / Catch

	% Insert the new objects into the model
	for i=1:length(props)
		eval(['set(obj, ''', props{i}, ''', ', props{i}, ');']);
	end

	model_objects=parenthandles.model_objects;
	model_objects{handles.caller}=obj;
	parenthandles.model_objects=model_objects;
	guidata(parentfig,parenthandles);

	eval(handles.callback);

	close(hObject)
	return
end;

if strcmp(action,'evars_edit_button')
	handles.evars=cellstr2cell(get(handles.evars_edit,'String'));
	guidata(hObject,handles);

	model_edit_evars('init', hObject, ['dgnl_action(',num2str(hObject),',''evars_refresh'')']);
end;

if strcmp(action,'evars_refresh')
	set(handles.evars_edit,'String',cell2cellstr(handles.evars));
%	handles.h=set(handles.h,'wvars',handles.evars);
	guidata(hObject,handles);
end;

if strcmp(action,'evars_edit')
	evars=cellstr2cell(get(handles.evars_edit,'String'));
	handles.evars=evars;
	set(handles.evars_edit,'String',cell2cellstr(evars));	% Format data
%	handles.h=set(handles.h,'wvars',evars);
end;

if strcmp(action,'h_inv_edit')
	h_inv=get(handles.h_inv_edit,'String');
	if isempty(h_inv)
		msgbox('The h_inv field must not be empty!');
		return
	end
	
	if h_inv(1)=='@'
		h_inv=h_inv(2:end);
	end

	handles.h_inv=str2func(h_inv);
	set(handles.h_inv_edit,'String',['@',h_inv]);	% Format data
end;


guidata(hObject,handles);
