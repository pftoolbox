function xltv_edit(action,varargin)
% Sorry, I didn't have enough time to write nice and documented GUI code.

% Toolbox for nonlinear filtering.
% Copyright (C) 2005  Jakob Ros�n <jakob.rosen@gmail.com>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%myfont='FixedWidth';
myfont='default';

if strcmp(action,'init');
	parentfig=varargin{1};
	obj=varargin{2};
	caller=varargin{3};
	callback=varargin{4};

	% Make the GUI show the correct information
	if strcmp(caller,'h')||strcmp(caller,'hu')
		if strcmp(obj.wchar,'w')
			set(obj,'wchar','e');
		end
	else
		if strcmp(obj.wchar,'e')
			set(obj,'wchar','w');
		end
	end	

	array=obj.array;
	evalvar=obj.evalvar;

	evalvarvec=zeros(1,4);
	for i=1:4
		if sum(find(evalvar==i))
			evalvarvec(i)=1;
		else
			evalvarvec(i)=0;
		end;
	end;

	interpolate=obj.interpolate;
	Ts=obj.Ts;
	
	if isempty(Ts)
		t_first='None';
		t_last='None';
	else
		t_first=num2str(Ts(1));
		t_last=num2str(Ts(end));
	end;

	hf=figure('Name','xltv object editor');

	uicontrol('Style','Text','String','3d expression matrix:','Units','normalized','Position',[0.0,0.8,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Edit','String','','Units','normalized','Position',[0.4,0.8,0.4,0.05],'Tag','expression_edit','Callback',['xltv_edit(''expression_import'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Import','Units','normalized','Position',[0.85,0.8,0.15,0.05],'Callback',['xltv_edit(''expression_import'',',num2str(hf),')'],'Fontname',myfont);

	uicontrol('Style','Text','String','Sample point vector:','Units','normalized','Position',[0.0,0.72,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Edit','String','','Units','normalized','Position',[0.4,0.72,0.4,0.05],'Tag','Ts_edit','Callback',['xltv_edit(''Ts_import'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Import','Units','normalized','Position',[0.85,0.72,0.15,0.05],'Callback',['xltv_edit(''Ts_import'',',num2str(hf),')'],'Fontname',myfont);

	if isempty(array)
		n=0;
	else
		n=size(array,3);
	end

	uicontrol('Style','Text','String','Expression size:','Units','normalized','Position',[0.0,0.60,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',sprintf('%d x %d',size(array,1),size(array,2)),'Units','normalized','Position',[0.4,0.60,0.4,0.05],'Tag','expression_size','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','Number of expressions:','Units','normalized','Position',[0.0,0.55,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',num2str(n),'Units','normalized','Position',[0.4,0.55,0.4,0.05],'Tag','expression_number','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','Number of sample points:','Units','normalized','Position',[0.0,0.5,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',num2str(length(Ts)),'Units','normalized','Position',[0.4,0.5,0.4,0.05],'Tag','Ts_number','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','First sample point:','Units','normalized','Position',[0.0,0.45,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',t_first,'Units','normalized','Position',[0.4,0.45,0.4,0.05],'Tag','Ts_first','HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String','Last sample point:','Units','normalized','Position',[0.0,0.4,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Text','String',t_last,'Units','normalized','Position',[0.4,0.4,0.4,0.05],'Tag','Ts_last','HorizontalAlign','Left','Fontname',myfont);

	uicontrol('Style','Text','String','Interpolation:','Units','normalized','Position',[0.0,0.3,0.4,0.05],'HorizontalAlign','Left','Fontname',myfont);
	

	uicontrol('Style','Radiobutton','String','None','Value',~interpolate,'Units','normalized','Position',[0.4,0.3,0.2,0.05],'Tag','radio1','Callback',['xltv_edit(''radio1'',',num2str(hf),')'],'HorizontalAlign','Left','Fontname',myfont);
	uicontrol('Style','Radiobutton','String','Linear','Value',interpolate,'Units','normalized','Position',[0.6,0.3,0.2,0.05],'Tag','radio2','Callback',['xltv_edit(''radio2'',',num2str(hf),')'],'HorizontalAlign','Left','Fontname',myfont);

	uicontrol('Style','Text','String','Multiply by:','Units','normalized','Position',[0.0,0.2,0.2,0.05],'Fontname',myfont);
%	uicontrol('Style','Text','String','','Units','normalized','Position',[0.8,0.2,0.2,0.05],'Tag','vars','Fontname',myfont);
	uicontrol('Style','Text','String','','Units','normalized','Position',[0.0,0.15,0.2,0.05],'Tag','vars','Fontname',myfont);

	uicontrol('Style','Checkbox','String','x','Units','normalized','Value',evalvarvec(1),'Position',[0.4,0.2,0.06,0.05],'Tag','check_1','callback',['xltv_edit(''check'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Checkbox','String','t','Units','normalized','Value',evalvarvec(2),'Position',[0.5,0.2,0.06,0.05],'Tag','check_2','callback',['xltv_edit(''check'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Checkbox','String','u','Units','normalized','Value',evalvarvec(3),'Position',[0.6,0.2,0.06,0.05],'Tag','check_3','callback',['xltv_edit(''check'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Checkbox','String',obj.wchar,'Units','normalized','Value',evalvarvec(4),'Position',[0.7,0.2,0.06,0.05],'Tag','check_4','callback',['xltv_edit(''check'',',num2str(hf),')'],'Fontname',myfont);

	uicontrol('Style','Pushbutton','String','Save','Units','normalized','Position',[0.8,0.02,0.1,0.05],'Callback',['xltv_edit(''save'',',num2str(hf),')'],'Fontname',myfont);
	uicontrol('Style','Pushbutton','String','Cancel','Units','normalized','Position',[0.9,0.02,0.1,0.05],'Callback',['delete(',num2str(hf),')'],'Fontname',myfont);


	handles=guihandles(hf);
	handles.array=array;
	handles.obj=obj;
	handles.caller=caller;
	handles.callback=callback;
	handles.parentfig=parentfig;
	handles.evalvar=evalvar;
	handles.interpolate=interpolate;
	handles.Ts=Ts;
	handles.wchar=obj.wchar;
	guidata(hf,handles);

	xltv_edit('check',hf);


elseif strcmp(action,'radio1')
	hf=varargin{1};
	handles=guidata(hf);
	set(handles.radio1,'value',1);
	set(handles.radio2,'value',0);
	handles.interpolate=false;
	guidata(hf,handles);
	
elseif strcmp(action,'radio2')
	hf=varargin{1};
	handles=guidata(hf);
	set(handles.radio2,'value',1);
	set(handles.radio1,'value',0);
	handles.interpolate=true;
	guidata(hf,handles);
	
elseif strcmp(action,'expression_import')
	hf=varargin{1};
	handles=guidata(hf);
	expr_str=get(handles.expression_edit,'String');
	if isempty(expr_str)
		msgbox('Error! Empty field');
		return;
	else
		array=evalin('base',expr_str,'[]');
		if isempty(array)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(array,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		elseif ndims(array)~=3
			msgbox('The data matrix must be three-dimensional!');
			return;
		else
			set(handles.expression_size,'String',sprintf('%d x %d',size(array,1),size(array,2)));
			set(handles.expression_number,'String',num2str(size(array,3)));
		end
	end;
	handles.array=array;
	guidata(hf,handles);

elseif strcmp(action,'Ts_import')
	hf=varargin{1};
	handles=guidata(hf);
	expr_str=get(handles.Ts_edit,'String');
	if isempty(expr_str)
		msgbox('Error! Empty field');
		return;
	else
		Ts=evalin('base',expr_str,'[]');
		if isempty(Ts)
			% An error occured while importing
			msgbox('Error! Did you type the correct name of the object?');
			return;
		elseif ~isa(Ts,'double')
			msgbox('Error! The object must be a matrix consisting of doubles!');
			return;
		elseif min(size(Ts,1),size(Ts,2))~=1||ndims(Ts)>2
			msgbox('The data matrix must be one-dimensional!');
			return;
		else
			set(handles.Ts_number,'String',num2str(length(Ts)));
			set(handles.Ts_first,'String',num2str(Ts(1)));
			set(handles.Ts_last,'String',num2str(Ts(end)));
		end
	end;
	handles.Ts=Ts;
	guidata(hf,handles);

elseif strcmp(action,'save')

	hf=varargin{1};
	handles=guidata(hf);

	caller=handles.caller;
	obj=handles.obj;

	if isempty(handles.array)
		msgbox('The expression array must not be empty!');
		return;
	end;

	if isempty(handles.Ts)
		msgbox('The sample points vector must not be empty!');
		return;
	end;

	if length(handles.Ts)~=size(handles.array,3)
		msgbox('Error! The number of expressions and the number of sample points must be equal!');
		return;
	end

	if isempty(handles.evalvar)
		msgbox('An evaluation variable must be selected. Use ''xtable'' if no variable multiplication is desired.');
		return;
	end

%	if handles.interpolation&&size(array,3)<2
%		msgbox('The number of expressions must be at least 2 in interpolation mode.');
%	end

	set(obj,'array',handles.array);
	set(obj,'Ts',handles.Ts);
	set(obj,'evalvar',handles.evalvar);
	set(obj,'interpolate',handles.interpolate);

	parentfig=handles.parentfig;
	parenthandles=guidata(parentfig);

	parenthandles.(caller)=obj;
	guidata(parentfig,parenthandles);	% Save data
	
	eval(handles.callback);
	close(hf)

elseif strcmp(action,'check')

	hf=varargin{1};
	handles=guidata(hf);

	str='';
	varnames={'x','t','u',handles.wchar};
	evalvar=[];
	for i=1:length(varnames)
		if get(handles.(['check_',num2str(i)]),'value')
			str=[str, varnames{i}, '; '];
			evalvar=[evalvar,i];
		end
	end
	if length(str)>1
		str=str(1:end-2);
		if length(str)>1
			str=['[', str, ']'];
		end
	end

	if isempty(str)
		str='Nothing';
	end

	set(handles.vars,'String',str);
	handles.evalvar=evalvar;
	guidata(hf,handles);
end;
